PATH = '/Users/Markus/workspace/isr14/generated';

X = linspace(-8,10,500);

Q = 0.8*normpdf(X/1.5);
P = 0.6*normpdf(X) + 0.25*normpdf(X-3) + 0.15*normpdf(X+3);

plot(X, Q, 'k-', 'LineWidth', 2);
hold on;
plot(X, P, 'k--', 'LineWidth', 1.2);
legend({'Laplace Approximation q(f^c|C)', 'True posterior p(f^c|C)'});
line([0 0], [0 0.4], 'LineStyle', ':', 'Color', 'k');
text(-4.5, 0.38, 'Posterior mode $$\hat{f^c}$$','interpreter','latex');
pbaspect([3, 1, 1]);

xlabel('$$f^c(t)$$','interpreter','latex');
ylabel('$$p(f^c(t) | C)$$','interpreter','latex');
set(gca,'XTickLabel',{});

hold off;

print(gcf, '-depsc', sprintf('%s/laplace_approx.eps', PATH), '-r 600');    