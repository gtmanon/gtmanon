PATH = '/Users/Markus/workspace/isr14/generated';

prepare_data;

BCD = DATASETS{1};
BCD = BCD.selectUsers(1);
CLD = BCD.toClassificationDataset([0 0 0 0], true);
CLD.discretize([-1 -1 25 25], 'equalspace');
[ GDS, rowGridIndices ] = CLD.toGrid(false, []);
AX = GDS.getAxes()';
Ytrain = GDS.YBinary([], true);
Xdata = GDS.X();

lengthScale = 1 * ones(1, GDS.DX());
sigma = 0.5;

K = KroneckerSEXPCov(AX);
K.updateParameters(log([sigma lengthScale]));

gpcfkron = gpcf_kronsexp('lengthScale', lengthScale, 'magnSigma2', sigma);      
gpcfkron = gpcf_kronsexp(gpcfkron, 'lengthScale_prior', prior_logunif(), ...
                                   'magnSigma2_prior', prior_sqrtunif());

                               
for eigennum = [10 100 1000]                               
    gp = gp_set('lik', lik_probit_idnoise(), 'cf', gpcfkron, 'jitterSigma2', 1e-9, ...
        'EXMAX', 100, 'KroneckerCov', K, 'FORCEAPPROX', true, 'DEBUG', false, 'SKIPIMPLICIT', false, ...
        'EIGENLIMIT', 1E-10, 'EIGENCOUNTLIMIT', eigennum, 'EIGENSAFE', 999.0, 'CGACCURACY', 1E-6);


    [ E, Var ] = gp_pred(gp, AX, { Ytrain }, AX);

    [coeff,score] = pca(Xdata(Ytrain.indices,:));

    Xcoord = score(:,1);
    Ycoord = score(:,2);

    pos = find(Ytrain.values > 0);
    neg = find(Ytrain.values < 0);

    xlin=linspace(min(Xcoord),max(Xcoord),100);
    ylin=linspace(min(Ycoord),max(Ycoord),100);
    [X,Y]=meshgrid(xlin,ylin);

    % Invert colormap
    colormap jet
%     cmap = colormap;
%     cmap = flipud(cmap);
%     colormap(cmap);

    Ze=griddata(Xcoord, Ycoord, E(Ytrain.indices,:), X, Y, 'cubic');
    [Ce,he] = contourf(X,Y,Ze,200);  
    xlim([-4,4]);
    ylim([-3.2,3.2]);
    shading flat;
    hold on;
    scatter(Xcoord(pos), Ycoord(pos), 210, '+', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
    scatter(Xcoord(neg), Ycoord(neg), 150, 'o', 'MarkerEdgeColor', 'w');
    print(gcf, '-depsc', sprintf('%s/posterior_mean_sushi_eigennum%d.eps', PATH, eigennum), '-r 600');    
    set(gca,'XTickLabel',{});
    set(gca,'YTickLabel',{});
    hold off;

    Zvar=griddata(Xcoord, Ycoord, Var, X, Y, 'cubic');
    [Cvar,hvar] = contourf(X,Y,Zvar,200);  
    xlim([-4,4]);
    ylim([-3.2,3.2]);
    shading flat;
    hold on;
        
    scatter(Xcoord(pos), Ycoord(pos), 210, '+', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
    scatter(Xcoord(neg), Ycoord(neg), 150, 'o', 'MarkerEdgeColor', 'w');
    set(gca,'XTickLabel',{});
    set(gca,'YTickLabel',{});
    
    if eigennum == 10
        rectangle('Position',[-4 -3.2 1.8 6.4], 'FaceColor', 'w', 'EdgeColor', 'w')
        h = colorbar('West', 'YTick', [-0.05 0.15 0.35], 'YTickLabel', {'Certain', 'Indifferent', 'Uncertain'});
        set(h, 'FontSize', 15);
        set(h, 'FontWeight', 'normal');
        set(h, 'YColor', [0 0 0]);
    end
    
    print(gcf, '-depsc', sprintf('%s/posterior_var_sushi_eigennum%d.eps', PATH, eigennum), '-r 600');  

    hold off;

    Zl=griddata(Xcoord, Ycoord, normcdf(E(Ytrain.indices,:) ./ sqrt(1+Var)), X, Y, 'cubic');
    [Cl,hl] = contourf(X,Y,Zl,200);  
    xlim([-4,4]);
    ylim([-3.2,3.2]);
    shading flat;
    hold on;
    scatter(Xcoord(pos), Ycoord(pos), 210, '+', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
    scatter(Xcoord(neg), Ycoord(neg), 150, 'o', 'MarkerEdgeColor', 'w');
    set(gca,'XTickLabel',{});
    set(gca,'YTickLabel',{});
    print(gcf, '-depsc', sprintf('%s/posterior_lik_sushi_eigennum%d.eps', PATH, eigennum), '-r 600');      
    hold off;
end
