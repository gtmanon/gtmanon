PATH = '/Users/Markus/workspace/isr14/generated';

Ngrid = 100;

xlin=linspace(-1, 1, Ngrid);
ylin=linspace(-1, 1, Ngrid);
[X,Y] = meshgrid(xlin, ylin);

prior = mvnpdf([X(:) Y(:)], [0 0], [1 0.4; 0.4 1]);
prior = reshape(prior, Ngrid, Ngrid);

% Invert colormap
colormap gray
cmap = colormap;
cmap = flipud(cmap);
colormap(cmap);

[C, h] = contourf(xlin, ylin, prior, 200);
shading flat;
hold on;
set(gca,'FontSize', 15);
xlabel('f(t_1)', 'Color', 'k', 'FontSize', 30);
set(gca,'XTick',[-0.5 0 0.5]);
%set(gca,'XTickLabel',{});
ylabel('f(t_2)', 'Color', 'k', 'FontSize', 30);
set(gca,'YTick',[-0.5 0 0.5]);
%set(gca,'YTickLabel',{});

print(gcf, '-depsc', sprintf('%s/example_prior.eps', PATH), '-r 600');     
hold off;

lik = normcdf(Y)*normcdf(-X);
[C, h] = contourf(xlin, ylin, lik, 200);
shading flat;
hold on;
set(gca,'FontSize', 15);
xlabel('f(t_1)', 'Color', 'k', 'FontSize', 30);
set(gca,'XTick',[-0.5 0 0.5]);
%set(gca,'XTickLabel',{});
ylabel('f(t_2)', 'Color', 'k', 'FontSize', 30);
set(gca,'YTick',[-0.5 0 0.5]);
%set(gca,'YTickLabel',{});

print(gcf, '-depsc', sprintf('%s/example_lik.eps', PATH), '-r 600');      
hold off;

posterior = prior.*lik;
[C, h] = contourf(xlin, ylin, posterior, 200);
shading flat;
hold on;
set(gca,'FontSize', 15);
xlabel('f(t_1)', 'Color', 'k', 'FontSize', 30);
set(gca,'XTick',[-0.5 0 0.5]);
%set(gca,'XTickLabel',{});
ylabel('f(t_2)', 'Color', 'k', 'FontSize', 30);
set(gca,'YTick',[-0.5 0 0.5]);
%set(gca,'YTickLabel',{});

print(gcf, '-depsc', sprintf('%s/example_posterior.eps', PATH), '-r 600');      
hold off;