PATH = '/Users/Markus/workspace/isr14/generated';

t = linspace(-5,5,100);
ft = sin((t+1.6)/3)*1.5;
pft = normcdf(ft);

[haxes,hline1,hline2] = plotyy(t,ft,t,pft);
legend({'Trade-off Evaluation (f(t), left axis)', 'Probit Likelihood (\Phi[f(t)], right axis)'}, 'Location', 'SouthEast');
hold all;

set(hline1, 'LineStyle', '--', 'LineWidth', 1, 'Color', 'k');
set(hline2, 'LineWidth', 2, 'Color', 'k');


%%%%% Axis 1 %%%%%

pbaspect([3, 1, 1]);

ylim([-2 2]);
set(haxes(1), 'YTick', -2:.5:2);
set(haxes(1), 'YColor', 'k');

ylabel('Trade-Off Evaluation: f(t)', 'Color', 'k', 'FontSize', 10)

%%%%% Axis 2 %%%%%
axes(haxes(2));

pbaspect([3, 1, 1]);

ylim([-0.5 1.5]);
set(haxes(2), 'YTick', [-1 0 1]);
set(haxes(2), 'YColor', 'k');

ylabel('Likelihood: p(y=+1|f) = \Phi[ f(t) ]', 'Color', 'k', 'FontSize', 10)
xlabel('Trade-Off: t', 'Color', 'k', 'FontSize', 10)

line([-5 5], [0 0], 'LineStyle', ':', 'Color', 'k');
line([-5 5], [1 1], 'LineStyle', ':', 'Color', 'k');

hold off;
print(gcf, '-depsc', sprintf('%s/probit.eps', PATH), '-r 600');  

