function [x, y] = zerocontours(c, offset)
% Adapted from http://stackoverflow.com/questions/15301626/choosing-isolines-from-matlab-contour-function

    sz = size(c,2);                 % Size of the contour matrix c
    ii = 1;                         % Index to keep track of current location
    jj = 1;                         % Counter to keep track of # of contour lines

    while ii < sz                   % While we haven't exhausted the array
        n = c(2,ii);                % How many points in this contour?
        s(jj).v = c(1,ii);          % Value of the contour
        s(jj).x = c(1,ii+1:ii+n);   % X coordinates
        s(jj).y = c(2,ii+1:ii+n);   % Y coordinates
        ii = ii + n + 1;            % Skip ahead to next contour line
        jj = jj + 1;                % Increment number of contours
    end

    if(nargin < 2)
        offset = 0;
    end
    
    minIdx = -1;
    minVal = Inf;
    for idx = 1:numel(s)    
        if(abs(s(idx).v - offset) < minVal)
            minIdx = idx;
            minVal = abs(s(idx).v - offset);
        end
    end
    
    x = s(minIdx).x;
    y = s(minIdx).y;
    