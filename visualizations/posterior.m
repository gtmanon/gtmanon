PATH = '/Users/Markus/workspace/isr14/generated';

prepare_data;

BCD = DATASETS{1};
BCD = BCD.selectUsers(1);
CLD = BCD.toClassificationDataset([0 0 0 0], true);
CLD.discretize([-1 -1 25 25], 'equalspace');
[ GDS, rowGridIndices ] = CLD.toGrid(false, []);
AX = GDS.getAxes()';
Ytrain = GDS.YBinary([], true);
Xdata = GDS.X();

lengthScale = 1 * ones(1, GDS.DX());
sigma = 0.5;

K = KroneckerSEXPCov(AX);
K.updateParameters(log([sigma lengthScale]));

gpcfkron = gpcf_kronsexp('lengthScale', lengthScale, 'magnSigma2', sigma);      
gpcfkron = gpcf_kronsexp(gpcfkron, 'lengthScale_prior', prior_logunif(), ...
                                   'magnSigma2_prior', prior_sqrtunif());

gp = gp_set('lik', lik_probit_idnoise(), 'cf', gpcfkron, 'jitterSigma2', 1e-9, ...
    'EXMAX', 100, 'KroneckerCov', K, 'FORCEAPPROX', false, 'DEBUG', false, 'SKIPIMPLICIT', false, ...
    'EIGENLIMIT', 0.01, 'EIGENCOUNTLIMIT', 100, 'EIGENSAFE', 20.0, 'CGACCURACY', 1E-6);


opt=optimset('TolFun', 1e-3, 'TolX', 1e-3, 'DerivativeCheck', 'off', 'Display', 'iter');
opt.lambdalim = 1E6;

[ E, Var ] = gp_pred(gp, AX, { Ytrain }, AX);
      
[coeff,score] = pca(Xdata(Ytrain.indices,:));

Xcoord = score(:,1);
Ycoord = score(:,2);

pos = find(Ytrain.values > 0);
neg = find(Ytrain.values < 0);

xlin=linspace(min(Xcoord),max(Xcoord),100);
ylin=linspace(min(Ycoord),max(Ycoord),100);
[X,Y]=meshgrid(xlin,ylin);

% Invert colormap
% colormap gray
colormap jet
% cmap = colormap;
% cmap = flipud(cmap);
% colormap(cmap);

Ze=griddata(Xcoord, Ycoord, E(Ytrain.indices,:), X, Y, 'cubic');
[Ce,he] = contourf(X,Y,Ze,200);  
shading flat;
xlim([-4,4]);
ylim([-3.2,3.2]);
hold on;

[Zex, Zey] = zerocontours(Ce);
% This is a hack to limit the zero contour line to just the part that
% separates the positive and negative areas.
Zex = Zex(83:221);
Zey = Zey(83:221);
scatter(Xcoord(pos), Ycoord(pos), 210, '+', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
scatter(Xcoord(neg), Ycoord(neg), 150, 'o', 'MarkerEdgeColor', 'w');
plot(Zex, Zey, 'k', 'LineWidth', 2);
set(gca,'XTickLabel',{});
set(gca,'YTickLabel',{});
print(gcf, '-depsc', sprintf('%s/posterior_mean_sushi.eps', PATH), '-r 600');      
hold off;

Zvar=griddata(Xcoord, Ycoord, Var, X, Y, 'cubic');
[Cvar,hvar] = contourf(X,Y,Zvar,200);  
shading flat;
xlim([-4,4]);
ylim([-3.2,3.2]);
hold on;

scatter(Xcoord(pos), Ycoord(pos), 210, '+', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
scatter(Xcoord(neg), Ycoord(neg), 150, 'o', 'MarkerEdgeColor', 'w');
plot(Zex, Zey, 'k', 'LineWidth', 2);
set(gca,'XTickLabel',{});
set(gca,'YTickLabel',{});
print(gcf, '-depsc', sprintf('%s/posterior_var_sushi.eps', PATH), '-r 600');      
hold off;

Zl=griddata(Xcoord, Ycoord, normcdf(E(Ytrain.indices,:) ./ sqrt(1+Var)), X, Y, 'cubic');
[Cl,hl] = contourf(X,Y,Zl,200);  
[Zlx, Zly] = zerocontours(Cl,0.5);
Zlx = Zlx(88:220);
Zly = Zly(88:220);
shading flat;
xlim([-4,4]);
ylim([-3.2,3.2]);

hold on;
scatter(Xcoord(pos), Ycoord(pos), 210, '+', 'MarkerEdgeColor', 'k', 'MarkerFaceColor', 'k');
scatter(Xcoord(neg), Ycoord(neg), 150, 'o', 'MarkerEdgeColor', 'w');
plot(Zlx, Zly, 'k', 'LineWidth', 2);
set(gca,'XTickLabel',{});
set(gca,'YTickLabel',{});
print(gcf, '-depsc', sprintf('%s/posterior_lik_sushi.eps', PATH), '-r 600');      
hold off;
