prepare_data;

d = 1;
for d = 1:numel(DATASETS)

    fprintf('Characteristics for -%s-\n', DATASET_NAMES{d});
    fprintf('===============================\n');
    
    fprintf('# Instances: %d\n', DATASETS{d}.NX());
    fprintf('# Instance Dims: %d\n', DATASETS{d}.DX());
    
    fprintf('# Users: %d\n', DATASETS{d}.getUsers().NX());
    
    fprintf('# Tradeoffs: %d\n', numel(DATASETS{d}.getChoices()));
    
    fprintf('Grid Size: %d\n', prod(DATASETS_CLD{d}.uniques()));
    
    fprintf('\n');
end
