function [ nlZ, dnlZ ] = laplace_wrapper(log_param, X, u, P, kernelname, likname, ...
                                         log_kernelparam, log_likparam, offset)
                                     
    persistent bestAlpha;
    persistent bestAlphaU;
 
    nx = size(X,1);
    if any(size(bestAlpha) ~= [nx,1]) || bestAlphaU ~= u
        bestAlpha = zeros(nx,1);  
        bestAlphaU = u;
    end
    
    if ~exist('offset', 'var')
        offset = zeros(size(X,1),1);
    end

    Nkernelparam = ec_cov_matrix_params(kernelname);
    Nlikparam = ec_likelihood_params(likname);
    
    hasKernelParams = (nargin >= 7 && ~isempty(log_kernelparam));
    hasLikParams = (nargin >= 8 && ~isempty(log_likparam));
    
    if(hasKernelParams)
        kernelparam = exp(log_kernelparam);        
        startlik = 1;
    else
        kernelparam = exp(log_param(1:Nkernelparam));
        startlik = Nkernelparam + 1;
    end
    
    if(hasLikParams)
        likparam = exp(log_likparam);
    else
        likparam = exp(log_param(startlik:(startlik + Nlikparam - 1)));
    end
    
    [bestnlZ,dnlZcov,dnlZlik,bestAlpha,~] = ec_laplace_approximation( ...
        double(X), double(P), kernelname, kernelparam, likname, likparam, offset, bestAlpha);
  
    nlZ = bestnlZ;
    dnlZ = [];
    
    if(~hasKernelParams)
       dnlZ = [ dnlZ; dnlZcov ]; 
    end
    
    if(~hasLikParams)
       dnlZ = [ dnlZ; dnlZlik ];
    end
    % fprintf('MP: EVAL cov=%f, lik=%f: nlZ=%f, dnlZ=%f,%f\n', exp(loghyper(1)), exp(loghyper(2)), nlZ, dnlZcov, dnlZlik);
end

