classdef COLGPExperiment < Experiment
% Perform preference learning as described in Houlsby et al. "Collaborative
% Gaussian Processes for Preference Learning" (NIPS 2012). This is a thin
% wrapper around their R-based implementation.
%
% Accepted optional parameters (set through setInput):
%
% METHOD    Which algorithm to use. Valid values are 'Birlutiu', 'Bonilla', 
%           'GPC', 'MPLFITC', and 'MPLFITC_NU' (default) See their paper 
%           for details.
% 
    
    properties(Constant)
        
        COLGP_DIR = 'benchmarks/colgp';    
        
    end

    properties(Access = 'private')
        
        % Unique name of the serialized dataset
        name;
                 
    end
    
    methods
        
        function obj = COLGPExperiment(binaryChoiceDataset)   
            
            if(nargin == 0)
                binaryChoiceDataset = [];
            end
            
            obj@Experiment(binaryChoiceDataset);
            
        end
                
        function type = getDatasetType(obj)
            type = 'BinaryChoiceDataset';
        end
        
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using GP preference learning as described in Houlsby et al. (NIPS, 2012) including their benchmarks.';
            
        end
        
        function prepare(obj)      
           
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            % Pseudo-unique filename
            obj.name = datestr(now,'ddmmyyyy_HHMMSS_FFF');
            
            x = obj.D.X();                
            u = obj.D.getUsers().X();
    
            p = [ obj.D.getAlternatives(), obj.D.getChoiceUsers(), obj.D.getChoices() ];
            % Houlsby et al. use -1 do indicate that the second alternative
            % was preferred
            p(p(:,4) == 2, 4) = -1;
                        
            [trainingIndices, testIndices] = obj.D.splitChoiceIndices(obj.trainingFraction, true, true);
                
            mkdir(strcat(obj.COLGP_DIR, '/data'), obj.name);
            
            dlmwrite(strcat(obj.COLGP_DIR, '/data/', obj.name, '/itemFeatures.txt'), x, 'delimiter', ' ');
            dlmwrite(strcat(obj.COLGP_DIR, '/data/', obj.name, '/userFeatures.txt'), u, 'delimiter', ' ');
            dlmwrite(strcat(obj.COLGP_DIR, '/data/', obj.name, '/train.txt'), p(trainingIndices, :), 'delimiter', ' ');
            dlmwrite(strcat(obj.COLGP_DIR, '/data/', obj.name, '/test.txt'), p(testIndices, :), 'delimiter', ' ');            
            
        end
        
        function [ trainingTime, testTime ] = train(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            oldFolder = cd(obj.COLGP_DIR);
            
            if(obj.hasInput('METHOD'))
                METHOD = obj.getInput('METHOD');
            else
                METHOD = 'MPLFITC_NU';
            end            
                        
            COMMAND = sprintf('Rscript runExperiment.R %s data/%s %d RES_%s.txt', METHOD, obj.name, obj.randomSeed, obj.name);
            Experiment.exlog(sprintf('Command line: --%s--\n', COMMAND));
            Experiment.exlog('===== START R =====\n');
            
            status = system(COMMAND);
            
            Experiment.exlog('===== END R =====\n');
            Experiment.exlog(sprintf('R command returned with exit status %d\n', status));
            
            
            if(status == 0)
                output = csvread(strcat('RES_', obj.name, '.txt'));                        
                obj.accuracy = 1 - output(1);            
                trainingTime = output(2);
                testTime = output(4);
                cd(oldFolder);
            else
                cd(oldFolder);
                error('The R execution reported an error, see log output.');                
            end
            
        end
        
        function test(obj)            
            % See also hasSeparateTesting  
        end
        
        function cleanup(obj)
            oldFolder = cd(obj.COLGP_DIR);
            delete(strcat('RES_', obj.name, '.txt'));
            rmdir(strcat('data/', obj.name), 's');  
            cd(oldFolder);
        end
    
    end
    
    methods(Access = 'protected')
        
        function value = hasSeparateTesting(obj)
            value = 0;
        end
        
    end          
    
end

