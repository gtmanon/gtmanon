classdef CGPCExperiment < GPGridExperiment
    
    properties(Access = 'protected')
        % Number of traits
        NT;
        
        % Class membership probabilities
        Gamma;
        
        lengthScale;
        
        noise;
        
        annealingFactor;
        
        skipVariance;
        
        axes;
    end
    
    methods
        
        function obj = CGPCExperiment(classificationDataset)
            
            if(nargin == 0)
                classificationDataset = [];
            end
            
            obj = obj@GPGridExperiment(classificationDataset);
            
        end
        
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using collaborative GPs with gridded feature differences of alternatives and Kronecker structured covariances.';
            
        end
        
        function prepare(obj)
            
            prepare@GPGridExperiment(obj);
            
            obj.NT = min(ceil(sqrt(obj.NU())), 25);
            
            if(obj.hasInput('NT'))
                obj.NT = obj.getInput('NT');
            end
          
            obj.lengthScale =  mean(obj.taskDispersion, 1);
            
            if(obj.hasInput('LENGTHSCALE'))
                obj.lengthScale = obj.getInput('LENGTHSCALE');
                obj.lengthScale = repmat(obj.lengthScale, 1, obj.D.DX());
            end
            
            obj.noise = mean(obj.labelDispersion, 1);
            
            if(obj.hasInput('NOISE'))
                obj.noise = obj.getInput('NOISE');
            end
            
            obj.annealingFactor = 0;
            
            if(obj.hasInput('ANNEALINGFACTOR'))
                obj.annealingFactor = obj.getInput('ANNEALINGFACTOR');
            end            
            
            obj.skipVariance = 0;
            
            if(obj.hasInput('SKIPVARIANCE'))
                obj.skipVariance = obj.getInput('SKIPVARIANCE');
            end            
            
            
            % Convenience copy of the grid's axes
            obj.axes = obj.GDSTest.getAxes()';

            % Tasks, Posterior means and variances of the tasks
            %obj.Ef = cell(1,obj.CVfolds);
            %obj.Varf = cell(1,obj.CVfolds);
                        
            CF = gpcf_kronsexp('lengthScale', obj.lengthScale, ...
                    'magnSigma2', obj.noise, ...
                    'lengthScale_prior', prior_logunif(), ...
                    'magnSigma2_prior', prior_sqrtunif());
                
                            
            if(obj.NT > 1)
                LIK = lik_probit_weighted();
                
                 % Random assignment, normalize to sum to one
            
                % exp(randi(...)) gives more concentrated draw than, e.g. rand(),
                % i.e., users have some influence from every expert, but major
                % influence from only few
                % obj.Gamma = rand(obj.NU, obj.NT);
                obj.Gamma = exp(randi(5, obj.NU, obj.NT));
                obj.Gamma = bsxfun(@rdivide, obj.Gamma, sum(obj.Gamma, 2));
            else
                LIK = lik_probit_idnoise();
            end
            
            obj.GP = gp_set('lik', LIK, ...
                'cf', CF, ...
                'jitterSigma2', 1e-8, ...
                'KroneckerCov', KroneckerSEXPCov(obj.axes), ...
                'EXMAX', obj.EXMAX, ...
                'FORCEAPPROX', obj.forceApproximation, ...
                'DEBUG', false, ...               
                'SKIPIMPLICIT', obj.skipImplicit, ...
                'MEANONLY', obj.skipVariance, ...
                'EIGENLIMIT', obj.eigenLimit, ...
                'EIGENCOUNTLIMIT', obj.eigenCountLimit, ...
                'EIGENSAFE', obj.eigenSafe, ...
                'CGACCURACY', 1E-5, ...
                'GAMMA', obj.Gamma);
        end
        
        function train(obj)
            
            train@GPGridExperiment(obj);
            
            if(obj.NT > 1)                
                iteration = 0;
                minimumIteration = 2;
                accuracy = zeros(1, obj.CVfolds);
                stopIteration = false;
                
                while ~stopIteration
                    iteration = iteration + 1;
                    accuracyOld = accuracy;
                    
                    obj.GP.GAMMA = obj.Gamma;
                    
                    if(obj.skipVariance)
                        obj.Ef = gp_pred(obj.GP, obj.axes, obj.YTrain(1), obj.axes);
                    else
                        [ obj.Ef, obj.Varf ] = gp_pred(obj.GP, obj.axes, obj.YTrain(1), obj.axes);
                    end
                    
                    E = sum(obj.Ef(obj.YCV{1}.indices,:) .* obj.Gamma(obj.YCV{1}.tasks, :), 2);
                    accuracy = mean(sign(E) == sign(obj.YCV{1}.values));
                    
                    if(obj.DEBUG)                        
                        fprintf('Iteration %d: accuracy (CV) = %f', iteration, accuracy);
                    end
                    
                    if(obj.skipVariance)
                        logpData = log(normcdf(bsxfun(@times, obj.Ef(obj.YCV{1}.indices,:), obj.YCV{1}.values)));
                    else                        
                        logpData = log(normcdf(bsxfun(@times, obj.Ef(obj.YCV{1}.indices,:), obj.YCV{1}.values) ./ ...
                            (sqrt(1 + obj.Varf))));
                    end
                    
                    logpUser = zeros(obj.NU, obj.NT);
                    for it = 1:obj.NT
                        logpUser(:,it) = accumarray(obj.YCV{1}.tasks, logpData(:,it));
                    end
                    
                    GammaNew = exp(logpUser);
                    GammaNew = bsxfun(@rdivide, GammaNew, sum(GammaNew, 2));
                    
                    GammaNewFilter = (GammaNew > 1/obj.NT);
                    GammaNew = GammaNew .* GammaNewFilter; % Sparsify
                    GammaNew = bsxfun(@rdivide, GammaNew, sum(GammaNew, 2));
                    
                    if(obj.annealingFactor > 0)
                        GammaNew = GammaNew + rand(size(GammaNew)) * obj.annealingFactor;
                        GammaNew = bsxfun(@rdivide, GammaNew, sum(GammaNew, 2));
                    end
                    
                    deltaAccuracy = mean(accuracy - accuracyOld);
                    deltaGamma = norm(obj.Gamma - GammaNew(:,1:size(obj.Gamma,2))) / obj.NT;
                    
                    stopIteration = (deltaAccuracy < 0.01 && (iteration >= minimumIteration)) || (iteration > 4);
                    
                    if(~stopIteration)
                        obj.Gamma = GammaNew;
                    end
                    
                    if(obj.DEBUG)
                        fprintf(' -> delta: %f (accuracy) / %f (Gamma)\n', deltaAccuracy, deltaGamma);
                    end
                end
                
            else
                obj.Ef = gp_pred(obj.GP, obj.axes, obj.YTrain(1), obj.axes);                
            end
            
        end
        
        function test(obj)
            
            test@GPGridExperiment(obj);
           
            if(obj.skipVariance)
                obj.GP.MEANONLY = false;
                
                % If we do the Gamma calculation without variance, we need
                % to calculate the variances a test time to be fair.
                [ obj.Ef, obj.Varf ] = gp_pred(obj.GP, obj.axes, obj.YTrain(1), obj.axes);
            end
            
            if(obj.NT > 1)                
                Eactual = sum(obj.Ef(obj.YTest.indices,:) .* obj.Gamma(obj.YTest.tasks,:), 2);
            else
                Eactual = obj.Ef;
            end

            obj.accuracy = mean(sign(Eactual) == sign(obj.YTest.values));            
        end
        
    end
end





