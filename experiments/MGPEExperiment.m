classdef MGPEExperiment < GPGridExperiment
       
    properties(Access = 'protected')                   
        % Number of experts
        NE;
                
        % Class membership probabilities
        Gamma;
    end
    
    methods
        
        function obj = MGPEExperiment(classificationDataset)   
                        
            if(nargin == 0)
                classificationDataset = [];
            end
                                        
            obj = obj@GPGridExperiment(classificationDataset);

        end
                
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using GP mixture of experts with gridded feature differences of alternatives and Kronecker structured covariances.';
            
        end
        
        function prepare(obj)
            
            prepare@GPGridExperiment(obj);
  
            obj.NE = min(ceil(sqrt(obj.NU())), 25);
            
            if(obj.hasInput('NE'))
                obj.NE = obj.getInput('NE');
            end
            
            Ntrain = numel(obj.YTrainAll.indices);
                        
            % Experts, Posterior means and variances of the experts
            obj.GP = cell(1, obj.NE);
            obj.Ef = zeros(Ntrain, obj.NE);
            obj.Varf = zeros(Ntrain, obj.NE);
                        
            for e = 1:obj.NE
                        
                CF = gpcf_kronsexp('lengthScale', obj.taskDispersion, ...
                                   'magnSigma2', mean(obj.labelDispersion, 1), ...
                                   'lengthScale_prior', prior_logunif(), ...
                                   'magnSigma2_prior', prior_sqrtunif());    
                
                      
                obj.GP{e} = gp_set('lik', lik_probit_idnoise(), ...
                                   'cf', CF, ...
                                   'jitterSigma2', 1e-8, ...
                                   'KroneckerCov', KroneckerSEXPCov(obj.GDSTrainAll.getAxes()'), ...
                                   'EXMAX', obj.EXMAX, ...
                                   'FORCEAPPROX', obj.forceApproximation, ...
                                   'DEBUG', false, ...
                                   'SKIPIMPLICIT', obj.skipImplicit, ...
                                   'EIGENLIMIT', obj.eigenLimit, ...
                                   'EIGENCOUNTLIMIT', obj.eigenCountLimit, ...
                                   'EIGENSAFE', obj.eigenSafe, ...
                                   'CGACCURACY', 1E-6);                 
            end
                           
            % Random assignment, normalize to sum to one
            
            % exp(randi(5)) gives more concentrated draw than, e.g. rand(), 
            % i.e., users have some influence from every expert, but major
            % influence from only few
            obj.Gamma = exp(randi(5, obj.NU, obj.NE));             
            obj.Gamma = bsxfun(@rdivide, obj.Gamma, sum(obj.Gamma, 2));
            
        end
        
        function train(obj)       
            
            train@GPGridExperiment(obj);
            
%             opt = optimset('TolFun', 1e-2, 'TolX', 1e-2, 'DerivativeCheck', 'off');    
%             opt.lambdalim = 1E6;
%             obj.gp=gp_optim(obj.gp, obj.GDSTrain.getAxes()', { YTrain }, 'opt', opt);    
            
            stopIteration = false;
            iteration = 0;   
            
%             hyperLearning = false;  
            hypersLearned = true; 
            
            accuracyTrain = 0;            
                       
            while ~stopIteration
                
                iteration = iteration + 1;                                     
                SigmaT = repmat(obj.labelDispersion, 1, obj.NE) ./ (obj.Gamma + 1E-7);
                                                                
                for e = 1:obj.NE                    
                    obj.GP{e}.SIGMAT = SigmaT(:,e);
                    
%                     if(hyperLearning)
%                         obj.GP{e} = gp_optim(obj.GP{e}, obj.GDSTrain.getAxes()', { obj.YTrain }, 'opt', opt);       
%                         hypersLearned = true;
%                     end
                    
                    [E, obj.Varf(:,e)] = gp_pred(obj.GP{e}, ...
                        obj.GDSTrainAll.getAxes()', { obj.YTrainAll }, obj.GDSTrainAll.getAxes()');   
                    
                    obj.Ef(:,e) = E(obj.YTrainAll.indices,:);
                end
                
                accuracyTrainOld = accuracyTrain;                
                Etrain = sum(obj.Ef.* obj.Gamma(obj.YTrainAll.tasks, :), 2);       
                accuracyTrain = mean(sign(Etrain) == sign(obj.YTrainAll.values));
                
                if(obj.DEBUG)                    
                    fprintf('Iteration %d: accuracy (train) = %f', iteration, accuracyTrain); 
                end
                
                logpData = log(normcdf(obj.Ef .* repmat(obj.YTrainAll.values, 1, obj.NE) ./ ...
                    (SigmaT(obj.YTrainAll.tasks,:) .* sqrt(1 + obj.Varf ./ SigmaT(obj.YTrainAll.tasks,:).^2))));
                
                logpMember = zeros(obj.NU, obj.NE);                
                for ie = 1:obj.NE
                    logpMember(:,ie) = accumarray(obj.YTrainAll.tasks, logpData(:,ie));
                end
                                
                obj.Gamma = exp(logpMember);
                obj.Gamma = obj.Gamma ./ (repmat(sum(obj.Gamma, 2), 1, obj.NE));
                
                deltaO = accuracyTrain - accuracyTrainOld;
                
%                 if(deltaO < 1E-3 && ~hypersLearned)
%                     hyperLearning = true;
%                 else
%                     hyperLearning = false;
%                 end
                                
                stopIteration = (deltaO < 0.01) && hypersLearned || (iteration > 10);
                
                if(obj.DEBUG)                         
                    fprintf(' -> delta (objective) = %f\n', deltaO);                    
                end                            
            end
            
        end
        
        function test(obj)
            
            test@GPGridExperiment(obj);           
            
            Ntest = numel(obj.YTest.indices);
            
            obj.Ef = zeros(Ntest, obj.NE);
            obj.Varf = [];
            
            for e = 1:obj.NE
                E = gp_pred(obj.GP{e}, obj.GDSTest.getAxes()', { obj.YTest }, obj.GDSTest.getAxes()');  
                
                obj.Ef(:, e) = E(obj.YTest.indices,:);
            end
            
            Etest = sum(obj.Ef .* obj.Gamma(obj.YTest.tasks, :), 2);       
            obj.accuracy = mean(sign(Etest) == sign(obj.YTest.values));
            
        end
        
    end    
end





