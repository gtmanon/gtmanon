classdef GPPDiffLaplaceExperiment < Experiment
% Perform preference learning as binary classification task on feature
% differences
%
% Accepted optional parameters (set through setInput):
%
% GP_TYPE       Type of GP specification, e.g., FULL, PIC, FIC, see GPstuff
%               documentation for more details
%
% X_IND         Inducing inputs, required by some sparse GP specifications.
%
    
    properties(Access = 'private')
        
        idxTrain;
        
        idxTest;
        
        gp;
        
    end
    
    methods
        
        function obj = GPPDiffLaplaceExperiment(classificationDataset)   
                        
            if(nargin == 0)
                classificationDataset = [];
            end
                                        
            obj = obj@Experiment(classificationDataset);
            
        end
        
        function type = getDatasetType(obj)
            
            type = 'ClassificationDataset';                
            
        end
        
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using a binary GP classifier on feature differences of alternatives.';
            
        end
        
        function prepare(obj)           

            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            Ntrain = round(obj.D.NX() * obj.trainingFraction);
            [obj.idxTrain, obj.idxTest] = obj.D.splitIndices(Ntrain, true);
            
            lik = lik_probit();
            gpcf = gpcf_sexp('lengthScale', ones(1, obj.D.DX()), ...
                             'lengthScale_prior', prior_t(), ...
                             'magnSigma2', 0.2^2, ...
                             'magnSigma2_prior', prior_sqrtunif());
             
            obj.gp = gp_set('lik', lik, 'cf', gpcf, 'jitterSigma2', 1e-9);
            
            if(obj.hasInput('GP_TYPE') && obj.hasInput('X_IND'))
                obj.gp = gp_set(obj.gp, 'type', obj.getInput('GP_TYPE'), 'X_u', double(obj.getInput('X_IND')));
            elseif(obj.hasInput('GP_TYPE'))
                obj.gp = gp_set(obj.gp, 'type', obj.getInput('GP_TYPE'));
            end
            
            obj.gp = gp_set(obj.gp, 'latent_method', 'Laplace');
            
        end
        
        function train(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            opt=optimset('TolFun', 1e-3, 'TolX', 1e-3);
            obj.gp=gp_optim(obj.gp, double(obj.D.X(obj.idxTrain)), ...
                double(obj.D.YBinary(obj.idxTrain)), 'opt', opt);
           
        end
        
        function test(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            Eft_la = gp_pred(obj.gp, double(obj.D.X(obj.idxTrain)), ...
                double(obj.D.YBinary(obj.idxTrain)), double(obj.D.X(obj.idxTest)));

            obj.accuracy = sum(sign(Eft_la) == sign(double(obj.D.YBinary(obj.idxTest)))) ./ ...
                numel(obj.idxTest);           
            
        end
        
        function cleanup(obj)
            
            % NOOP
            
        end
        
    end    
end

