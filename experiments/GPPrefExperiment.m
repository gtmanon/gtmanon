classdef GPPrefExperiment < Experiment

    properties(Access = 'private')
        
        P;
        
        idxTrain;
        
        idxTest;
        
        %Name of the kernel function to use
        KERNELNAME = 'GAUSSIAN';
        
        % Kernel parameters
        pKernel;
    
        %Name of the likelihood function to use
        LIKNAME = 'GPPREF';
        
        % Likelihood parameters
        pLik;
        
    end
    
    methods
        
        function obj = GPPrefExperiment(binaryChoiceDataset)   
            
            if(nargin == 0)
                binaryChoiceDataset = [];
            end
            
            obj@Experiment(binaryChoiceDataset);
            
        end
                
        function type = getDatasetType(obj)
            type = 'BinaryChoiceDataset';
        end
        
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using GP preference learning as described in Chu&Ghahramani (2005).';
            
        end
        
        function prepare(obj)      
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            Nkernelparam = ec_cov_matrix_params(obj.KERNELNAME);
            Nlikparam = ec_likelihood_params(obj.LIKNAME);

            [ preferred, dispreferred ] = obj.D.getChoiceIndices(); 
            obj.P = double([ preferred, dispreferred ]);
            obj.P = obj.P - ones(size(obj.P));
            
            [obj.idxTrain, obj.idxTest] = obj.D.splitChoiceIndices(obj.trainingFraction, true, true);
            
            obj.pKernel = 0.5 * ones(1, Nkernelparam);
            obj.pLik = 0.5 * ones(1, Nlikparam);            
                       
        end
        
        function success = train(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            Nkernelparam = ec_cov_matrix_params(obj.KERNELNAME);
                                    
            x0 = log([obj.pKernel, obj.pLik])';
            options = optimset('Display', 'off', 'GradObj', 'on');
            
            f = @(x) laplace_wrapper(x, obj.D.X(), 1, obj.P(obj.idxTrain,:), obj.KERNELNAME, obj.LIKNAME);
            loghyper = fminunc(f, x0, options);
                
            obj.pKernel = exp(loghyper(1:Nkernelparam));
            obj.pLik = exp(loghyper((Nkernelparam+1):end));           
                      
        end
        
        function test(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            [~,mu,s2,~,~] = ec_gp_prediction(double(obj.D.X()), double(obj.P(obj.idxTrain)), ...
                                obj.KERNELNAME, obj.pKernel, ... 
                                obj.LIKNAME, obj.pKernel, double(obj.D.X()));        

            [ preferred, dispreferred ] = obj.D.getChoiceIndices(); 
            
            obj.accuracy = sum(mu(preferred(obj.idxTest)) > mu(dispreferred(obj.idxTest))) ./ numel((obj.idxTest));
                        
        end
        
        function cleanup(obj)
            
            % NOOP
            
        end
        
    end    
end

