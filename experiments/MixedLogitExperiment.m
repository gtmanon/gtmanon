classdef MixedLogitExperiment < Experiment
% Perform preference learning using mixed logit as described in 
% Kenneth Train, Discrete Choice Methods with Simulation, Chapter 12
%
% This class is a thin wrapper around the model file 'mxlhb.m' of the
% original author and most code and comments in here are copied almost 
% verbatim from there.
%
% Accepted optional parameters (set through setInput):
%

% From the original author's documentation:
% Matlab code to estimate a mixed logit model with hierachical Bayes 
% procedures
% Written by Kenneth Train, first version July 13, 2006, 
%   latest edits July 27, 2006
%
% MODEL NOTATION
% Utility of person n from alternative j is: U_jn=F'Z_jn+C_n'X_jn+e_jn
% where F is a vector of fixed coefficients (the same for all people) of 
% variables Z_jn. and C_n is a vector of random coefficients (different 
% for each person) of variables X_jn. 
% The random coefficients for each person are transformations of randomly 
% distributed latent terms. That is, C_n=T(B_n) for transformation T and 
% B_n~N(A,D). 
% The transformations available in this code are:
%     normally distributed coefficient: C_n=B_n, 
%     lognormal: C_n=exp(B_n), 
%     truncated normal: C_n=max(0,B_n).
% The latent terms are distributed normally with mean vector A and covariance 
% matrix D. Correlation among the latent terms induces correlation among 
% the coefficients.
% The procedure takes draws of A, D, and B_n for all n sequentially using 
% MCMC methods. 
% Draws of C_n are created from the draws of B_n. That is, a draw of B_n 
% implies a draw of C_n.
% Matrix B={B_1,...,B_N} is a NVxNP matrix of latent terms for all people. 
% Matrix C is the corresponding matrix of coefficients for all people.

    properties(Access = 'private')
        
        idxTrain;
        
        idxTest;
        
    end
    
    methods
        
        function obj = MixedLogitExperiment(binaryChoiceDataset)   
            
            if(nargin == 0)
                binaryChoiceDataset = [];
            end
            
            obj@Experiment(binaryChoiceDataset);
            
        end
                
        function type = getDatasetType(obj)
            type = 'BinaryChoiceDataset';
        end
        
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using the mixed logit model.';
            
        end
        
        function prepare(obj)           

            global NP NCS NROWS
            global IDV NV NAMES FULLCV
            global A B D % C  
            global RHO NCREP NEREP NSKIP NRFOR NRLL INFOSKIP
            global SEED1 SEED2
            global KEEPMN WANTINDC % PUTA PUTD  PUTC 
            % global NALTMAX NCSMAX
            % global X S A B C D SQRTNP 
            global F NF IDF NAMESF RHOF % XF
            global XMAT            
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            [obj.idxTrain, obj.idxTest] = obj.D.splitChoiceIndices(obj.trainingFraction, true, true);
                        
            obj.idxTrain = sort(obj.idxTrain);
            obj.idxTest = sort(obj.idxTest);
            
            % Number of people (decision-makers) in dataset 
            NP=obj.D.NU();

            % Number of choice situations in dataset. This is the number 
            % faced by all the people combined.
            NCS=numel(obj.idxTrain);

            % Total number of alternatives faced by all people in all choice 
            % situations combined.
            % This is the number of rows of data in XMAT below.
            NROWS=numel(obj.idxTrain)*2;
            
            % Load and/or create XMAT, a matrix that contains the data.
            % XMAT must contain one row of data for each alternative in 
            % each choice situation for each person.
            % The rows are grouped by person, and by choice situations 
            % faced by each person.
            % The number of rows in XMAT must be NROWS, specified above.
            % The columns in XMAT are variable that describe the alternative.
            % 
            % The *first* column of XMAT identifies the person who faced 
            % this alternative. 
            % The people must be numbered sequentially from 1 to NP, in 
            % ascending order.
            % All alternatives for a given person must be grouped together.
            % The *second* column of XMAT identifies the choice situation. 
            % The choice situations must be numbered sequentially from 1 to 
            % NCS.
            % All alternatives for a given choice situation must be grouped 
            % together.
            % The *third* column of XMAT identifies the chosen alternatives 
            % (1 for chosen, 0 for not). One and only one alternative must 
            % be chosen for each choice situation.
            % The remaining columns of XMAT can be any variables.

            XMAT = zeros(NROWS, 3 + obj.D.DX());
            
            Dmak = obj.D.getChoiceUsers();
            Dmak = repmat(Dmak(obj.idxTrain)', 2, 1);
            XMAT(:,1) = Dmak(:);
            
            Csit = repmat(1:NCS, 2, 1);
            XMAT(:,2) = Csit(:);
            
            Chosen = repmat(obj.D.getChoices()', 2, 1);
            Chosen = Chosen(:,obj.idxTrain);
            Chosen(1,Chosen(1,:) == 2) = 0;
            Chosen(2,Chosen(2,:) == 1) = 0;
            Chosen(2,Chosen(2,:) == 2) = 1;
            XMAT(:,3) = double(Chosen(:));
            
            Alt = obj.D.getAlternatives();
            Alt = Alt(obj.idxTrain,:);
            XMAT(1:2:NROWS-1, 4:end) = obj.D.X(Alt(:,1));
            XMAT(2:2:NROWS, 4:end) = obj.D.X(Alt(:,2));
                        
            % RANDOM COEFFICIENTS
            % List the variables in XMAT that enter the model with random
            % coefficients and give the distribution for the coefficient of 
            % each variable.
            % IDV contains one row for each random coefficient and two columns.
            % The *first* column gives the number of a variable in XMAT 
            % that has a random coefficient, and the *second* column 
            % specifies the distribution of the coefficient for that variable.
            % The distributions can be: 
            %   1. normal
            %   2. lognormal
            %   3. truncated normal (with the share below zero massed at zero), 
            %   4. S_B
            %   5. normal with zero mean(for error components.)
            % If no random coefficients, put IDV=[];
            %
            % Notes:
            % The lognormal, truncated normal, and S_B distributions give 
            % positive coefficients only. If you want a variable to have 
            % only negative coefficients, create the negative of the 
            % variable (in the specification of XMAT above).
            % The S_B distribution gives coefficients between 0 and 1. 
            % If you want coefficients to be between 0 and k, then multiply 
            % the variable by k (in the specification of XMAT above), 
            % since b*k*x for b~(0-1) is the same as b*x for b~(0-k).
 
            if(obj.hasInput('IDV'))                
                IDV=obj.getInput('IDV');
            else
                IDV=zeros(obj.D.DX(),2);
                IDV(:,1) = 4:3+obj.D.DX();
                IDV(:,2) = 1;
            end

            NV=size(IDV,1); %Number of random coefficients. Do not change this line.

            % Give a name to each of the explanatory variables in IDV. They 
            % can have up to ten characters including spaces. Put the names 
            % in single quotes and separate the quotes with semicolons.
            if(obj.hasInput('NAMES'))                
                IDV=obj.getInput('NAMES');
            else
                NAMES = arrayfun(@(idx) sprintf('VAR%d', idx), 1:NV, 'UniformOutput', false)';
            end            

            % Correlation
            % Set FULLCV=1 to estimate covariance among all random 
            % coefficients, FULLCV=0 for no covariances
            FULLCV=1;

            % Starting values
            % Specify the starting values for A,B, and D.
            % A contains the means of the distribution of the underlying 
            % normal for each random coefficient.  It is a column vector 
            % with the same length as IDV. For distribution 5 (normal with 
            % zero mean), put 0 for the starting value for the mean. The 
            % code will keep it at 0. B contains the random coefficients 
            % for each person. It is a matrix with IDV rows and NP columns.
            % D contains the covariance matrix of the distribution of 
            % underlying normals for the random coefficients. It is a 
            % symmetric matrix with IDV rows and columns.
            % If FULLCV=0, then D is diagonal.

            A=ones(NV, 1);
            B=repmat(A,1,NP);    %Start each person at the starting means
            D=NV.*eye(NV);       %Starting variances equal to number of random coefficients

            % Set the initial proportionality fraction for the jumping 
            % distribution for the random coefficients for each person.
            % This fraction is adjusted by the program in each iteration to 
            % attain an acceptance rate of about .3 in the M-H algorithm 
            % for the B's 
            RHO = 0.1;

            % FIXED COEFFICIENTS
            % List the variables in XMAT that enter with fixed coefficients.
            % Put semicolons between the numbers. If no fixed coefficients, 
            % put IDF=[];
            if(obj.hasInput('IDF'))                
                IDV=obj.getInput('IDF');
            else
                IDF=[];
            end

            NF=size(IDF,1); %Number of fixed coefficients. Do not change this line.
            
            if(obj.hasInput('NAMESF'))                
                IDF=obj.getInput('NAMESF');
            else
                NAMESF = arrayfun(@(idx) sprintf('FIX%d', idx), 1:NF, 'UniformOutput', false)';
            end   
            
            % Starting values.
            % Specify the starting values for the fixed coefficients F.
            % F must have the same length as IDF and have one column.
            F=zeros(NF, 1);

            % Set the proportionality fraction for the jumping distribution 
            % for the fixed coefficients.
            % This fraction is not adjusted by the program. Ideally, RHOF 
            % should be set such the acceptance rate is about .3 in the M-H 
            % algorithm for the draws of F 
            RHOF = 0.01;

            % BURN-IN, RETAINING DRAWS, PRINTING ITERATION INFORMATION

            % Number of iterations to make prior to retaining draws 
            % (i.e., length of burn-in)
            NCREP = 10000;

            % Number of draws to retain after burn-in 
            NEREP = 1000;

            % Number of iterations to make between retained draws
            % NOTE: The number of iterations after burn-in is NEREP*NSKIP, 
            % of which NEREP are retained The total number of iterations is 
            % NCREP+(NEREP*NSKIP)
            NSKIP = 10;

            % Number of draws from N(A-hat,D-hat) to use in simulating the 
            % estimated dist of random coefficients.
            NRFOR=2000;

            % Number of draws per person to use in calculating the simulated 
            % log-likelihood value at A-hat, D-hat, and F-hat.
            NRLL=2000;

            % How frequently to print information about the iteration process 
            % eg 100 == print out info every 100-th iteration 
            INFOSKIP = 500;

            % Set seeds for the random number generators.
            % Seed for random draws in iteration process. Must be a positive 
            % integer.
            SEED1 = obj.randomSeed;
            
            % Seed for random draws fron N(A-hat,D-hat) in simulated 
            % distribution of coefficients and log-likelihood value. Must be 
            % a positive integer.
            SEED2 = obj.randomSeed * 13;   

            % Do you want to save the draws of A, D, and F to a file? 
            % If so, set KEEPMN=1. If no, set KEEPMN=0.
            KEEPMN=0;

            % Do you want to calculate and save the means of the NEREP draws 
            % of the individual-level random coefficients? If so, set 
            % WANTINDC=1. If no, set WANTINDC=0.
            WANTINDC=0;

            % If WANTINDC=1, specify filename (and full path if not the 
            % working directory) to which to save the mean of the draws
            % of the individual-level random coefficients. The output in 
            % FOUTPUTC will be a matlab matrix of dimension NPxNIV, 
            % containing one row for each person and one column for each 
            % coefficient.             
        end
        
        function train(obj)
            
            global NP NCS NROWS
            global IDV NV NAMES FULLCV
            global A B D C  
            global RHO NCREP NEREP NSKIP NRFOR NRLL INFOSKIP
            global SEED1 SEED2
            global KEEPMN WANTINDC PUTA PUTD  PUTC 
            global NALTMAX NCSMAX
            global X S SQRTNP 
            global F NF IDF NAMESF RHOF XF
            global XMAT  
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end

            doit;
            
        end
        
        function test(obj)
            
            global B
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
            
            % Indices into the instances of each choice situation used for
            % testing
            Alt = obj.D.getAlternatives();
            Alt = Alt(obj.idxTest,:);
            
            % Decision
            Dmak = obj.D.getChoiceUsers();
            
            AltMat = zeros(size(Alt,1), 4);            
            AltMat(:,1) = Dmak(obj.idxTest); % The decision maker
            AltMat(:,2) = sum(double(obj.D.X(Alt(:, 1))) .* B(:,AltMat(:,1))', 2); % Utility of first alternative
            AltMat(:,3) = sum(double(obj.D.X(Alt(:, 2))) .* B(:,AltMat(:,1))', 2); % Utility of second alternative
            
            AltMat(:,4) = sign(AltMat(:,2) - AltMat(:,3));
            AltMat(AltMat(:,4) == -1,4) = 2;
            
            Choices = obj.D.getChoices();
            obj.accuracy = sum(AltMat(:,4) == Choices(obj.idxTest)) / size(AltMat, 1);
            
        end
        
        function cleanup(obj)
            
            clear NP NCS NROWS
            clear IDV NV NAMES FULLCV
            clear A B C D
            clear RHO NCREP NEREP NSKIP NRFOR NRLL INFOSKIP
            clear SEED1 SEED2
            clear KEEPMN WANTINDC % PUTA PUTD  PUTC 
            clear NALTMAX NCSMAX
            clear X S A B C D SQRTNP 
            clear F NF IDF NAMESF RHOF
            clear XMAT  
            
        end
        
    end    
end






