PATH = '/Users/Markus/workspace/isr14/generated';

LINECOLOR = { 'k', 'k', 'k', 'k', 'k', 'k', 'k' };
LINESTYLE = { '-', '-', '-', ':', '-.', '-', '-' };
LINEMARKER = { '.', '^', 'x', 'o', '.', 'o', '^' };
LINEWIDTH = { 2.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0 };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Part 1:
%
% Benchmark results
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('results_scaling.mat');

SELECTION = [3 4 5 2 1];

prepTimeAll = prepTime(:,SELECTION,:,:);
trainTimeAll = trainTime(:,SELECTION,:,:);
testTimeAll = testTime(:,SELECTION,:,:);
accuracyAll = accuracy(:,SELECTION,:,:);
certaintyAll = certainty(:,SELECTION,:,:);

experimentAll = experiment(SELECTION);
experimentOptionsAll = experimentOptions(SELECTION);

errorMarkerAll = errorMarker(:,SELECTION,:,:);

DIMENSIONS_ALL = DIMENSIONS;

EXPERIMENT_NAMES_ALL = EXPERIMENT_NAMES(SELECTION);
EXPERIMENT_NAMES_ALL = cellfun(@(n) strrep(n, 'COLGPExperiment METHOD=MPLFITC_NU', 'Collaborative GP'), EXPERIMENT_NAMES_ALL, 'Un', 0);
EXPERIMENT_NAMES_ALL = cellfun(@(n) strrep(n, 'COLGPExperiment METHOD=GPC', 'GP Classification'), EXPERIMENT_NAMES_ALL, 'Un', 0);
EXPERIMENT_NAMES_ALL = cellfun(@(n) ifthenelse(strfind(n, 'CGPCExperiment') == 1, 'GTM', n), EXPERIMENT_NAMES_ALL, 'Un', 0);

EXPERIMENT_NAMES_ALL{1} = [ EXPERIMENT_NAMES_ALL{1} ', 3 Levels' ];
EXPERIMENT_NAMES_ALL{2} = [ EXPERIMENT_NAMES_ALL{2} ', 5 Levels' ];
EXPERIMENT_NAMES_ALL{3} = [ EXPERIMENT_NAMES_ALL{3} ', 7 Levels' ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Part 2:
%
% Additional benchmark results for 10 Dimensions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('results_scaling9.mat');

prepTime = prepTime(:,SELECTION,:,:);
trainTime = trainTime(:,SELECTION,:,:);
testTime = testTime(:,SELECTION,:,:);
accuracy = accuracy(:,SELECTION,:,:);
certainty = certainty(:,SELECTION,:,:);

prepTime = (prepTime(1:10,:,:,:) + prepTime(11:20,:,:,:)) ./ 2;
trainTime = (trainTime(1:10,:,:,:) + trainTime(11:20,:,:,:)) ./ 2;
testTime = (testTime(1:10,:,:,:) + testTime(11:20,:,:,:)) ./ 2;
accuracy = (accuracy(1:10,:,:,:) + accuracy(11:20,:,:,:)) ./ 2;
certainty = (certainty(1:10,:,:,:) + certainty(11:20,:,:,:)) ./ 2;

prepTimeAll = cat(3, prepTimeAll, prepTime);
trainTimeAll = cat(3, trainTimeAll, trainTime);
testTimeAll = cat(3, testTimeAll, testTime);
accuracyAll = cat(3, accuracyAll, accuracy);
certaintyAll = cat(3, certaintyAll, certainty);

DIMENSIONS_ALL = [ DIMENSIONS_ALL DIMENSIONS ];

experimentAll = [ experimentAll, experiment ];
experimentOptionsAll = [ experimentOptionsAll, experimentOptions ];

errorMarker = errorMarker(:,SELECTION,:,:);
errorMarker = (errorMarker(1:10,:,:,:) + errorMarker(11:20,:,:,:));

errorMarkerAll = cat(3, errorMarkerAll, errorMarker);

EXPERIMENT_NAMES_ALL = [ EXPERIMENT_NAMES_ALL EXPERIMENT_NAMES ];
EXPERIMENT_NAMES_ALL = cellfun(@(n) ifthenelse(strfind(n, 'COLGPExperiment METHOD=MPLFITC_NU') == 1, 'Collaborative GP', n), EXPERIMENT_NAMES_ALL, 'Un', 0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Analysis
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prepdata = @(M) (1 - errorMarkerAll) .* ~isnan(M) .* M;

samples = squeeze(sum(errorMarkerAll == 0,1));

accuracyAll(accuracyAll == 0) = NaN;
accuracy_mean = squeeze(nanmean(prepdata(accuracyAll),1));
accuracy_stddev = squeeze(nanstd(prepdata(accuracyAll),0,1));
accuracy_stderr = 1.96 * (accuracy_stddev ./ sqrt(samples));

trainTimeAll(trainTimeAll == 0)= NaN;
trainTime_mean = squeeze(nanmean(prepdata(trainTimeAll),1));
trainTime_stddev = squeeze(nanstd(prepdata(trainTimeAll),0,1));
trainTime_stderr = 1.96 * (trainTime_stddev ./ sqrt(samples));

testTimeAll(testTimeAll == 0) = NaN;
testTime_mean = squeeze(nanmean(prepdata(testTimeAll),1));
testTime_stddev = squeeze(nanstd(prepdata(testTimeAll),0,1));
testTime_stderr = 1.96 * (testTime_stddev ./ sqrt(samples));

LABELSELECTION = USERS([2 4:end]);

XLabels = arrayfun(@(p) sprintf('%d', p), 105*LABELSELECTION*.80, 'Un', 0);
set(gca,'FontSize', 15);

Ndimensions = size(accuracy_mean, 2);

for d = 1:Ndimensions
   
    accuracy_mean_D = squeeze(accuracy_mean(:,d,:));
    accuracy_stderr_D = squeeze(accuracy_stderr(:,d,:));
    trainTime_mean_D = squeeze(trainTime_mean(:,d,:));
    trainTime_stderr_D = squeeze(trainTime_stderr(:,d,:));
    testTime_mean_D = squeeze(testTime_mean(:,d,:));
    testTime_stderr_D = squeeze(testTime_stderr(:,d,:));
   
        
    hFig = figure();
    set(hFig, 'Visible', 'off');
    set(gca,'FontSize', 15);
    for alg = 1:size(accuracy_mean_D, 1)
        h = errorbar(USERS, trainTime_mean_D(alg,:), trainTime_stderr_D(alg,:), ...
            'Color', LINECOLOR{alg}, 'LineStyle', LINESTYLE{alg}, ...
            'LineWidth', LINEWIDTH{alg}, 'Marker', LINEMARKER{alg});
        
        hold on;
    end
    
    set(gca,'XTick', LABELSELECTION)
    set(gca,'XTickLabel', XLabels);
    xlabel('Number of Trade-Offs');
    ylabel('Training Time');
    xlim([40 1010]);
    ylim([0 350]);
    legend(EXPERIMENT_NAMES_ALL, 'Location', 'NorthWest');
    print(gcf, '-depsc', sprintf('%s/synth_trainingtime_%d_dim.eps', PATH, DIMENSIONS_ALL(d)), '-r 600');      
    hold off;
     
end