% Number of repetitions per combination of
% Experiment / Option / Dataset / training fraction
REPETITION = 1:20;

% Range of percentages of data used for training 
TRAININGFRACTION = [0.20, 0.30, 0.40, 0.50, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90]; % linspace(0.20, 0.90, 8);
% TRAININGFRACTION = linspace(0.9, 0.9, 1);

%%%%%%%%%%%%%%%%%%% Nothing to configure below this line %%%%%%%%%%%%%%%%%%%

% Load and preprocess the datasets
prepare_data;

EXP_NAME = [ 'pref_' datestr(now,'ddmmyyyy_HHMMSS_FFF') ];

EXPERIMENTS = { 'COLGPExperiment' };
% , ...
%                 'MixedLogitExperiment', ...
%                 'CGPCExperiment'};
            
% For each experiment, indicate whether the experiment requires gridding.
% In this case the discretized dataset CLD_* (see prepare_data) is passed 
% to the experiment instead of the full classification dataset CL_*
EXPERIMENT_REQUIRES_GRIDDING = [ 0, 0, 1 ];

% For each experiment, there is an option set in EXPERIMENT_OPTIONS. This
% can either be empty ({}) or it can be a cell array (one instantiation of 
% the experiment) containing multiple cell arrays (one for each option)
% with two elements each (key/value pair).
EXPERIMENT_OPTIONS = {{{{'METHOD', 'MPLFITC_NU'}}}};
                   
% EXPERIMENT_OPTIONS = {{{{'METHOD', 'Birlutiu'}}
%                        {{'METHOD', 'GPC'}}                       
%                        {{'METHOD', 'MPLFITC_NU'}}}
%                       {}
%                      {{{'EIGENSAFE', 999}
%                        {'EIGENLIMIT', 0.1}
%                        {'EIGENCOUNTLIMIT', 100}
%                        {'NT', 10 }
%                        {'SKIPVARIANCE', 1 }
%                        {'NOISE', 0.5 }
%                        {'DEBUG', 0 }}}};                   
 
           
% Experiment names are computed from a combination of the experiment and its
% options
EXPERIMENT_NAMES = cell(0);

% %%%%%%%%%%%%%%%%%%%% Nothing to configure below this line %%%%%%%%%%%%%%%%%%%%

BASEDIR = pwd();
Ninst = 0;

% Determine how many instantiations of experiments and option settings there
% are
for expIdx = 1:numel(EXPERIMENTS)
    
    if isempty(EXPERIMENT_OPTIONS{expIdx})
        % Just the plain experiment without options
        Ninst = Ninst + 1;
        EXPERIMENT_NAMES{Ninst} = EXPERIMENTS{expIdx};
    else
        baseName = EXPERIMENTS{expIdx};
        
        for optsetIdx = 1:numel(EXPERIMENT_OPTIONS{expIdx})
            Ninst = Ninst + 1;            
            
            currentName = baseName;
            for optIdx = 1:numel(EXPERIMENT_OPTIONS{expIdx}{optsetIdx})
                currentName = sprintf('%s %s=%s', currentName, ...
                    EXPERIMENT_OPTIONS{expIdx}{optsetIdx}{optIdx}{1}, ...
                    EXPERIMENT_OPTIONS{expIdx}{optsetIdx}{optIdx}{2});
                
            end
                        
            EXPERIMENT_NAMES{Ninst} = currentName;
        end
    end
    
end

prepTime = zeros(numel(REPETITION), Ninst, numel(DATASETS), numel(TRAININGFRACTION));
trainTime = zeros(numel(REPETITION), Ninst, numel(DATASETS), numel(TRAININGFRACTION));
testTime = zeros(numel(REPETITION), Ninst, numel(DATASETS), numel(TRAININGFRACTION));

accuracy = zeros(numel(REPETITION), Ninst, numel(DATASETS), numel(TRAININGFRACTION));
certainty = zeros(numel(REPETITION), Ninst, numel(DATASETS), numel(TRAININGFRACTION));

experiment = cell(1,Ninst);
experimentOptions = cell(1,Ninst);

errorMarker = zeros(numel(REPETITION), Ninst, numel(DATASETS), numel(TRAININGFRACTION));

for repIdx = REPETITION
    
    instanceIdx = 1;    
    
    for expIdx = 1:numel(EXPERIMENTS)
        
        cd(BASEDIR);
        E = eval(EXPERIMENTS{expIdx});
                        
        if isempty(EXPERIMENT_OPTIONS{expIdx})
        % Just the plain experiment without options        
            OPTION_SET = { { { 'NO_OPTIONS', 'TRUE' } } };       
           
        else
            OPTION_SET = EXPERIMENT_OPTIONS{expIdx};     
        end
        
        for optSetIdx = 1:numel(OPTION_SET)
            
            E.clearInput();
            E.setRandomSeed(repIdx);
            
            OPTIONS = OPTION_SET{optSetIdx};            
            % Set all option pairs
            for optIdx = 1:numel(OPTIONS)            
                OPTION = OPTIONS{optIdx};
                E.setInput(OPTION{1}, OPTION{2});
            end                
                
            experiment{instanceIdx} = EXPERIMENTS{expIdx};
            experimentOptions{instanceIdx} = OPTION_SET;
            
            for dsIdx = 1:numel(DATASETS)  
                
                E.setInput('DATASET', DATASET_NAMES{dsIdx});
                % E.setInput('LENGTHSCALE', DATASETS_MEDIANDIST(dsIdx));
                
                if(strcmp(E.getDatasetType(), 'BinaryChoiceDataset'))
                        E.setDataset(DATASETS{dsIdx});
                    elseif(strcmp(E.getDatasetType(), 'ClassificationDataset'))
                        
                        if(EXPERIMENT_REQUIRES_GRIDDING(expIdx))
                            E.setDataset(DATASETS_CLD{dsIdx});
                        else
                            E.setDataset(DATASETS_CL{dsIdx});
                        end
                        
                        E.setInput('X_IND', unique(DATASETS_CLD{dsIdx}.X(), 'rows'));            
                    else
                        warning('Unknown dataset type required: %s -- skipping.', E.getDatasetType());
                        continue;
                end
                    
                for trFracIdx = 1:numel(TRAININGFRACTION)
                
                    try                                
                        E.trainingFraction = TRAININGFRACTION(trFracIdx);
                        E.run();

                        prepTime(repIdx, instanceIdx, dsIdx, trFracIdx) = E.prepT;
                        trainTime(repIdx, instanceIdx, dsIdx, trFracIdx) = E.trainT;
                        testTime(repIdx, instanceIdx, dsIdx, trFracIdx) = E.testT;

                        accuracy(repIdx, instanceIdx, dsIdx, trFracIdx) = E.accuracy;
                        certainty(repIdx, instanceIdx, dsIdx, trFracIdx) = 0;

                    catch err
                        fprintf('Skipping iteration due to error %s\n', err.message);
                        errorMarker(repIdx, instanceIdx, dsIdx, trFracIdx) = 1;

                        for e=1:length(err.stack)
                            fprintf('  - %s at %i\n',err.stack(e).name,err.stack(e).line);
                        end
                    end

                    save([EXP_NAME '.mat'], 'experiment', 'experimentOptions', ...
                        'prepTime', 'trainTime', 'testTime', ...
                        'accuracy', 'certainty', 'errorMarker', ...
                        'DATASET_NAMES', 'TRAININGFRACTION', 'EXPERIMENT_NAMES' );
                
                end
                
            end
        
            instanceIdx = instanceIdx + 1;
                    
        end    
    end
end







