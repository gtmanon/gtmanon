function val = ifthenelse(cond, thenval, elseval)

if(cond)
    val = thenval;
else
    val = elseval;
end

end

