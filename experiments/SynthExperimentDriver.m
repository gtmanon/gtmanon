% Number of repetitions per combination of
% Experiment / Option / Dataset / training fraction
REPETITION = 1:10;

% Range of percentages of data used for training 
TRAININGFRACTION = 0.80;

DIMENSIONS = [5];
USERS = [50,75,100,250,500,750,1000];

%%%%%%%%%%%%%%%%%%% Nothing to configure below this line %%%%%%%%%%%%%%%%%%%

EXP_NAME = [ 'synth_' datestr(now,'ddmmyyyy_HHMMSS_FFF') ];

EXPERIMENTS = { 'COLGPExperiment', ...
                'CGPCExperiment'};
            
% For each experiment, indicate whether the experiment requires gridding.
% In this case the discretized dataset CLD_* (see prepare_data) is passed 
% to the experiment instead of the full classification dataset CL_*
EXPERIMENT_REQUIRES_GRIDDING = [ 0, 1 ];

% For each experiment, there is an option set in EXPERIMENT_OPTIONS. This
% can either be empty ({}) or it can be a cell array (one instantiation of 
% the experiment) containing multiple cell arrays (one for each option)
% with two elements each (key/value pair).
EXPERIMENT_OPTIONS ={{{{'METHOD', 'MPLFITC_NU'}}
                      {{'METHOD', 'GPC'}}}
                     {{{'EIGENSAFE', 999999}
                       {'EIGENLIMIT', 0.1}
                       {'EIGENCOUNTLIMIT', 10}
                       {'NT', 10 }
                       {'SKIPVARIANCE', 1 }
                       {'DISCRETIZATION', 3 }}
                      {{'EIGENSAFE', 999999}
                       {'EIGENLIMIT', 0.1}
                       {'EIGENCOUNTLIMIT', 10}
                       {'NT', 10 }
                       {'SKIPVARIANCE', 1 }
                       {'DISCRETIZATION', 5 }}                   
                      {{'EIGENSAFE', 999999}
                       {'EIGENLIMIT', 0.1}
                       {'EIGENCOUNTLIMIT', 10}
                       {'NT', 10 }
                       {'SKIPVARIANCE', 1 }
                       {'DISCRETIZATION', 7 }}}};
            
% Experiment names are computed from a combination of the experiment and its
% options
EXPERIMENT_NAMES = cell(0);

% %%%%%%%%%%%%%%%%%%%% Nothing to configure below this line %%%%%%%%%%%%%%%%%%%%

BASEDIR = pwd();
Ninst = 0;

% Determine how many instantiations of experiments and option settings there
% are
for expIdx = 1:numel(EXPERIMENTS)
    
    if isempty(EXPERIMENT_OPTIONS{expIdx})
        % Just the plain experiment without options
        Ninst = Ninst + 1;
        EXPERIMENT_NAMES{Ninst} = EXPERIMENTS{expIdx};
    else
        baseName = EXPERIMENTS{expIdx};
        
        for optsetIdx = 1:numel(EXPERIMENT_OPTIONS{expIdx})
            Ninst = Ninst + 1;            
            
            currentName = baseName;
            for optIdx = 1:numel(EXPERIMENT_OPTIONS{expIdx}{optsetIdx})
                currentName = sprintf('%s %s=%s', currentName, ...
                    EXPERIMENT_OPTIONS{expIdx}{optsetIdx}{optIdx}{1}, ...
                    EXPERIMENT_OPTIONS{expIdx}{optsetIdx}{optIdx}{2});
                
            end
                        
            EXPERIMENT_NAMES{Ninst} = currentName;
        end
    end
    
end

prepTime = zeros(numel(REPETITION), Ninst, numel(DIMENSIONS), numel(USERS));
trainTime = zeros(numel(REPETITION), Ninst, numel(DIMENSIONS), numel(USERS));
testTime = zeros(numel(REPETITION), Ninst, numel(DIMENSIONS), numel(USERS));

accuracy = zeros(numel(REPETITION), Ninst, numel(DIMENSIONS), numel(USERS));
certainty = zeros(numel(REPETITION), Ninst, numel(DIMENSIONS), numel(USERS));

experiment = cell(1,Ninst);
experimentOptions = cell(1,Ninst);

errorMarker = zeros(numel(REPETITION), Ninst, numel(DIMENSIONS), numel(USERS));

for repIdx = REPETITION
    
    for dIdx = 1:numel(DIMENSIONS)
        
        for uIdx = 1:numel(USERS)
            
            clear D;
            
            D = SyntheticBinaryChoiceDataset('InstancesPerRelation', 15, ...
                'InstanceDimensionality', DIMENSIONS(dIdx), ...
                'NumberOfClusters', 1, ...
                'RelationsPerCluster', USERS(uIdx), ...
                'SigmaRelation', 0.01, ...
                'SigmaPreference', 0.01);
            
            D.load();
                        
            instanceIdx = 1;
            
            for expIdx = 1:numel(EXPERIMENTS)
                
                cd(BASEDIR);
                E = eval(EXPERIMENTS{expIdx});
                
                if isempty(EXPERIMENT_OPTIONS{expIdx})
                    % Just the plain experiment without options
                    OPTION_SET = { { { 'NO_OPTIONS', 'TRUE' } } };
                    
                else
                    OPTION_SET = EXPERIMENT_OPTIONS{expIdx};
                end
                
                for optSetIdx = 1:numel(OPTION_SET)
                    
                    E.clearInput();
                    E.setRandomSeed(repIdx);
                    
                    discretizationLevel = 3;
                    
                    OPTIONS = OPTION_SET{optSetIdx};
                    % Set all option pairs
                    for optIdx = 1:numel(OPTIONS)
                        OPTION = OPTIONS{optIdx};
                        E.setInput(OPTION{1}, OPTION{2});
                        
                        if(strcmp(OPTION{1}, 'DISCRETIZATION'))
                            discretizationLevel = OPTION{2};
                        end
                    end
                    
                    experiment{instanceIdx} = EXPERIMENTS{expIdx};
                    experimentOptions{instanceIdx} = OPTION_SET;
                    
                    
                    try
                        Dinst = BinaryChoiceDataset;
                        Dinst.copy(D);
                        
                        if(strcmp(E.getDatasetType(), 'BinaryChoiceDataset'))
                            E.setDataset(Dinst);
                        elseif(strcmp(E.getDatasetType(), 'ClassificationDataset'))
                            Dinst = Dinst.toClassificationDataset();
                            
                            binDims = ceil(DIMENSIONS(dIdx) / 2);
                            nonbinDims = DIMENSIONS(dIdx) - binDims;
                            
                            Dinst.discretize([repmat(3, 1, binDims) repmat(discretizationLevel, 1, nonbinDims)], 'equalcount');
                            E.setDataset(Dinst);
                        end
                        
                        
                        E.trainingFraction = TRAININGFRACTION;
                        E.run();
                        
                        prepTime(repIdx, instanceIdx, dIdx, uIdx) = E.prepT;
                        trainTime(repIdx, instanceIdx, dIdx, uIdx) = E.trainT;
                        testTime(repIdx, instanceIdx, dIdx, uIdx) = E.testT;
                        
                        accuracy(repIdx, instanceIdx, dIdx, uIdx) = E.accuracy;
                        certainty(repIdx, instanceIdx, dIdx, uIdx) = 0;
                        
                        
                    catch err
                        fprintf('Skipping iteration due to error %s\n', err.message);
                        errorMarker(repIdx, instanceIdx, dIdx, uIdx) = 1;
                        
                        for e=1:length(err.stack)
                            fprintf('  - %s at %i\n',err.stack(e).name,err.stack(e).line);
                        end
                    end
                    
                    save([EXP_NAME '.mat'], 'experiment', 'experimentOptions', ...
                        'prepTime', 'trainTime', 'testTime', ...
                        'accuracy', 'certainty', 'errorMarker', ...
                        'DIMENSIONS', 'USERS', 'EXPERIMENT_NAMES' );
                    
                    instanceIdx = instanceIdx + 1;
                    
                end                                               
            end
        end
    end
end







