%
% Script that loads all preference datasets into memory without any
% significant pre-processing. Only constant zero columns are dropped from
% the datasets. The rest of preprocessing is done in the prepare_data
% script.
%

SUSHI = BinaryChoiceDataset; SUSHI.load('data/CGPPREF.he5', 'sushi');

% MOVIELENS = BinaryChoiceDataset; MOVIELENS.load('data/CGPPREF.he5', 'movielens'); 
%   MOVIELENS.dropColumns([ 3 4 7 9 12 14 18 ]); % Drop constant zero columns
  
ELECTION = BinaryChoiceDataset; ELECTION.load('data/CGPPREF.he5', 'election');                 

% TARIFFS = BinaryChoiceDataset; TARIFFS.load('data/TARIFFS.he5', 'reduced2013'); 
%   TARIFFS.dropColumns([3 4]);  % Drop constant zero columns
  
TARIFFSFULL = BinaryChoiceDataset; TARIFFSFULL.load('data/TARIFFS.he5', 'full2013'); 
  TARIFFSFULL.dropColumns(4); % Drop constant zero columns  
  
CARS1 = BinaryChoiceDataset; CARS1.load('data/CARS.he5', 'exp1');

% CARS2 = BinaryChoiceDataset; CARS2.load('data/CARS.he5', 'exp2');

DATASETS = { SUSHI, ELECTION, TARIFFSFULL, CARS1 };
DATASET_NAMES = { 'SUSHI', 'ELECTION', 'TARIFFSFULL', 'CARS1' };

