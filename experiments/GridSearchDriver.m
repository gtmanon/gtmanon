% Load and preprocess the datasets
prepare_data;

% Number of repetitions per combination of
REPETITION = 1:3;

TRAININGFRACTION = 0.85;

DIDX = 1; % Sushi

NT = [ 8, 10, 12, 15 ];
LENGTHSCALE = [ 0.5, 1, 2, 5, 10 ] * DATASETS_MEDIANDIST(DIDX);
NOISE = [ 0.5, 1, 2, 5, 10, 20 ];

P_NT = 1;
P_LENGTHSCALE = 2;
P_NOISE = 3;

EXPERIMENTS = allcomb(NT, LENGTHSCALE, NOISE);

%%%%%%%%%%%%%%%%%%% Nothing to configure below this line %%%%%%%%%%%%%%%%%%%

EXP_NAME = [ 'grid_' datestr(now,'ddmmyyyy_HHMMSS_FFF') ];

NE = size(EXPERIMENTS, 1);

prepTime = zeros(numel(REPETITION), NE);
trainTime = zeros(numel(REPETITION),NE);
testTime = zeros(numel(REPETITION), NE);

accuracy = zeros(numel(REPETITION), NE);
errorMarker = zeros(numel(REPETITION), NE);

for repIdx = 1:numel(REPETITION)
    
    for expIdx = 1:NE
        
        D = ClassificationDataset();
        D.copy(DATASETS_CLD{DIDX});
        
        E = CGPCExperiment(D);
        
        try
            E.trainingFraction = TRAININGFRACTION;
            E.setRandomSeed(expIdx*repIdx);
            
            E.setInput('EIGENSAFE', 1);
            E.setInput('EIGENLIMIT', 0.1);
            E.setInput('EIGENCOUNTLIMIT', 150);
            E.setInput('DEBUG', true);
            
            E.setInput('NT', EXPERIMENTS(expIdx, P_NT));
            E.setInput('LENGTHSCALE', EXPERIMENTS(expIdx, P_LENGTHSCALE));
            E.setInput('NOISE', EXPERIMENTS(expIdx, P_NOISE));
            
            E.run();
            
            prepTime(repIdx, expIdx) = E.prepT;
            trainTime(repIdx, expIdx) = E.trainT;
            testTime(repIdx, expIdx) = E.testT;
            accuracy(repIdx, expIdx) = E.accuracy;
            
        catch err
            fprintf('Skipping experiment due to error %s\n', err.message);
            errorMarker(repIdx, expIdx) = 1;
            
            for e=1:length(err.stack)
                fprintf('  - %s at %i\n',err.stack(e).name,err.stack(e).line);
            end
        end
        
        save([EXP_NAME '.mat'], ...
            'prepTime', 'trainTime', 'testTime', ...
            'accuracy', 'errorMarker', ...
            'EXPERIMENTS', 'TRAININGFRACTION' );
        
    end
end





