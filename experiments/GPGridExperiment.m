classdef GPGridExperiment < Experiment
%GPGridExperiment Base class for various experiments that work with
%gridded datasets
   
    properties(Access = 'protected')
        
        GDSTrainAll;
        YTrainAll;
        
        % Gridded training data and binary labels.
        %
        % These are all cell arrays of size CVfolds if fractionCV > 0. If
        % cvFraction = 0, the *Train members are cell arrays of size 1, and
        % the *CV members are undefined.
        GDSTrain;        
        YTrain;
        
        GDSCV;        
        YCV;
        
        % Gridded test data and binary labels
        GDSTest;        
        YTest;
        
        % Flag indicating whether data should be mirrored, i.e., added again with
        % features and labels reversed
        mirrorData;
        
        % Median dispersion of data per task and dimension
        taskDispersion;
        
        % Median quadratic label dispersion per task
        labelDispersion;
        
        % Gaussian process(es)
        GP;
        
        % Posterior mean of GP(s)
        Ef;
        
        % Posterior variance of GP(s)
        Varf;
                
        % Number of users
        NU;
                
        % Parameters for the Laplace inference, see the gp_set
        % documentation for details        
        eigenLimit;        
        eigenCountLimit;        
        eigenSafe;        
        EXMAX;       
        forceApproximation;        
        skipImplicit;
                
        % Debug output for experiment iterations
        DEBUG;       
        
        % Fraction of the training dataset held out for cross validation to 
        % determine task weights. If this is 0, the task weights will be
        % determined on the training data itself;
        fractionCV;
        
        % Number of folds for cross-validation
        CVfolds;
        
        % Function handle
        DSpostProcessor;
    end
    
    methods
        
        function obj = GPGridExperiment(classificationDataset)   
                        
            if(nargin == 0)
                classificationDataset = [];
            end
                                        
            obj = obj@Experiment(classificationDataset);
            obj.eigenLimit = 0.01;
            obj.eigenCountLimit = 250;
            obj.eigenSafe = 0.1;
            obj.EXMAX = 200;
            obj.forceApproximation = false;
            obj.skipImplicit = false;
            obj.mirrorData = false;
                                    
            % Display debugging information during the experiment
            obj.DEBUG = false;
            
            obj.fractionCV = 0.0;
            obj.CVfolds = 1;
            
        end
        
        function type = getDatasetType(obj)
            
            type = 'ClassificationDataset';                
            
        end
        
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using gridded feature differences of alternatives and Kronecker structured covariances.';
            
        end
        
        function prepare(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end                       
                        
            if(obj.hasInput('EIGENLIMIT'))
                obj.eigenLimit = obj.getInput('EIGENLIMIT');
            end
            
            if(obj.hasInput('EIGENCOUNTLIMIT'))
                obj.eigenCountLimit = obj.getInput('EIGENCOUNTLIMIT');
            end
            
            if(obj.hasInput('EIGENSAFE'))
                obj.eigenSafe = obj.getInput('EIGENSAFE');
            end
            
            if(obj.hasInput('EXMAX'))
                obj.EXMAX = obj.getInput('EXMAX');
            end
            
            if(obj.hasInput('FORCEAPPROX'))
                obj.forceApproximation = obj.getInput('FORCEAPPROX');
            end
            
            if(obj.hasInput('SKIPIMPLICIT'))
                obj.skipImplicit = obj.getInput('SKIPIMPLICIT');
            end
            
            if(obj.hasInput('MIRRORDATA'))
                obj.mirrorData = obj.getInput('MIRRORDATA');
            end
            
            if(obj.hasInput('FRACTIONCV'))
                obj.fractionCV = obj.getInput('FRACTIONCV');
            end
            
            if(obj.hasInput('CVFOLDS'))
                obj.CVfolds = obj.getInput('CVFOLDS');
            end
            
            if(obj.hasInput('DSPOSTPROCESSOR'))
                obj.DSpostProcessor = obj.getInput('DSPOSTPROCESSOR');
            end
            
            if(obj.hasInput('DEBUG'))
                obj.DEBUG = obj.getInput('DEBUG');
            end
               
            obj.NU = numel(unique(obj.D.getTasks()));            
            Ntrain = round(obj.D.NX() * obj.trainingFraction);
                        
            % Random, per task splitting
            [idxTrainOrg, idxTest] = obj.D.splitIndices(Ntrain, true, true);
            
            NCVsplit = floor(numel(idxTrainOrg) * (1 - obj.fractionCV));
            
            idxTrain = cell(1, obj.CVfolds);
            idxCV = cell(1, obj.CVfolds);
            
            if(obj.fractionCV > 0)
                for f = 1:obj.CVfolds
                    P = randperm(numel(idxTrainOrg));                    
                    idxTrain{f} = idxTrainOrg(P(1:NCVsplit));
                    idxCV{f} = idxTrainOrg(P(NCVsplit+1:end));
                end
            else
                idxTrain{1} = idxTrainOrg;
                idxCV{1} = idxTrainOrg;
            end
                        
            if(obj.mirrorData)   
                X = obj.D.X();
                Y = obj.D.Y();
                T = obj.D.getTasks();
                
                for f = 1:obj.CVfolds
                    start = size(X, 1);
                    
                    X = [X; -X(idxTrain{f},:) ]; % Reverse training points on the mirrored data    
                    Y = [Y; (Y(idxTrain{f},:) * -1) + 3 ]; % Reverse 1s and 2s on the mirrored data
                    T = [T; T(idxTrain{f},:) ];
                
                    idxTrain{f} = [ idxTrain{f}; start + (1:numel(idxTrain{f}))' ];
                end
                
%                 if(obj.fractionCV > 0)
%                     for f = 1:obj.CVfolds
%                         start = size(X, 1);
% 
%                         X = [X; -X(idxCV{f},:) ]; 
%                         Y = [Y; (Y(idxCV{f},:) * -1) + 3 ]; % Reverse 1s and 2s on the mirrored data
%                         T = [T; T(idxCV{f},:) ];
% 
%                         idxCV{f} = [ idxCV{f}; start + (1:numel(idxCV{f}))' ];
%                     end
%                 end
                
                obj.D.setX(X);
                obj.D.setY(Y,T);
            end
                        
            % Call post-processing on the data set, e.g., discretization
            if(~isempty(obj.DSpostProcessor))
                obj.DSpostProcessor(obj.D);
            end            
                                    
            % Switch dimensions, so that small dimensions can be grouped at
            % the right of Kronecker products
            [~, idx] = sort(obj.D.uniques(), 'descend');
            obj.D.switchColumns(idx);            
            
            % Compute the axes for the full dataset
            axes = GridDataset.computeAxes(obj.D);
                        
            obj.GDSTrain = cell(1,obj.CVfolds);
            obj.YTrain = cell(1,obj.CVfolds);
            
            obj.GDSCV = cell(1,obj.CVfolds);
            obj.YCV = cell(1,obj.CVfolds);
            
            idxTrainAll = [];
            
            % Then compute gridded datasets over the same grid, one only based 
            % on the training instances, the other on the test instances
            for f = 1:obj.CVfolds
                obj.GDSTrain{f} = obj.D.toGrid(false, axes, idxTrain{f});
                obj.YTrain{f} = obj.GDSTrain{f}.YBinary([], true);

                obj.GDSCV{f} = obj.D.toGrid(false, axes, idxCV{f});
                obj.YCV{f} = obj.GDSCV{f}.YBinary([], true);       
                
                idxTrainAll = [ idxTrainAll; idxTrain{f}; idxCV{f} ];
            end               
            
            idxTrainAll = unique(idxTrainAll);
            obj.GDSTrainAll = obj.D.toGrid(false, axes, idxTrainAll);
            obj.YTrainAll = obj.GDSTrainAll.YBinary([], true);   
                                                
            obj.GDSTest = obj.D.toGrid(false, axes, idxTest);
            obj.YTest = obj.GDSTest.YBinary([], true);   
            
            obj.taskDispersion = obj.D.maxDispersion();
            obj.labelDispersion = obj.D.medianLabelDispersionPerTask();  
            
        end       
        
        function train(obj)
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
        end
        
        function test(obj)
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
        end
        
        function cleanup(obj)            
            % NOOP            
        end
        
    end    
end





