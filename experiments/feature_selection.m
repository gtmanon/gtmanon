prepare_data;

RANDOM_SEED = 1;
START_DATASET = 4;

%%%%%%%%%%%%%%%%%%% NOTHING TO CONFIGURE BELOW THIS LINE %%%%%%%%%%%%%%%%%%%

stream = RandStream('mrg32k3a');
RandStream.setGlobalStream(stream);
stream.Substream = RANDOM_SEED;

opts = statset('display', 'iter');

for d = START_DATASET:numel(DATASETS)
    
    fprintf('Now performing feature selection for dataset %s.\n', DATASET_NAMES{d});
    D = DATASETS{d};
        
    % Pseudo instances and labels, see bcd_feature_criterion docs
    X = repmat(1:D.DX(), D.NC(), 1);
    y = (1:D.NC())';
    
    crit = @(XT,yT,Xt,yt) bcd_feature_criterion(DATASETS{d}, RANDOM_SEED, 'MPLFITC_NU', XT, yT, Xt, yt);    
    [fs,history] = sequentialfs(crit, X, y, 'nullmodel', false, 'options', opts);
    
    disp(fs);
    disp(history);
    
end