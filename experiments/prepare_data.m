%
% Perform pre-processing on the raw datasets.
%

load_data;

% Feature selection
%
% First, select the most predictive features based on the results of forward 
% feature selection with the predictive accuracy of Birlutiu's method as 
% criterion (see the feature_selection script for further detail).

% SUSHI
% Final columns included:  7 9 12 14 
% Crit: [0.2736 0.1247 0.0956 0.0947]

SUSHI.dropColumns([1:6 8 10 11 13 15]);

% MOVIELENS
% Final columns included:  1 2 4 5 7 10 11 
% Crit: [0.3989 0.3302 0.2564 0.2229 0.1889 0.1500 0.0988]

% MOVIELENS.dropColumns([3 6 8 9]);

% ELECTION
% Final columns included:  1 5 6 8 11 13 15 20 
% Crit: [0.2201 0.1111 0.0816 0.0793 0.0759 0.0735 0.0726 0.0724]
  
ELECTION.dropColumns([2 3 4 7 9 10 12 14 16 17 18 19]);

% TARIFFS
% Final columns included:  1 3 4 6 
% Crit: [0.3328 0.2517 0.2259 0.2138]

% TARIFFS.dropColumns([2 5 7 8]);

% TARIFFSFULL
% Final columns included:  4 6 7 8 
% Crit: [0.3311 0.2951 0.2738 0.2689]

TARIFFSFULL.dropColumns([1 2 3 5]);

% CARS1
% Final columns included:  1 3 4 5 
% Crit: [0.3302 0.2041 0.1452 0.1105]

CARS1.dropColumns(2);

% CARS2
% Final columns included:  all
% Crit: [0.3638 0.2807 0.2283 0.1729 0.1668 0.1657]

% Correcting various encodings

% In the tariff dataset there are several columns which should be
% log-transformed and for which zeros are unhandy: renewable content (3),
% price for 500 kwh (4), and minimum contract duration(8). Bump the zeros
% in these columns upward some, so that a log ratio becomes meaningful.
% TX = TARIFFS.X();
% TX(TX(:,2) == 0, 2) = 1.0;
% TARIFFS.setX(TX);

% Essentially the same for the full tariff dataset, except there is now
% also an indexed tariff colum (3), which shifts all othe columns one
% upwards
TX = TARIFFSFULL.X();
TX(TX(:,1) == 0, 1) = 1.0;
TARIFFSFULL.setX(TX); clear TX;

% In the two cars datasets, we need to coorect a few of the encodings:
%
% Body type: Sedan (1), SUV (2), Hatchback(3)
% Transmission: Manual (1), Automatic (2)
% Engine capacity: 2.5L, 3.5L, 4.5L, 5.5L, 6.2L
% Fuel consumed: Hybrid (1), Non-Hybrid (2)
%
% The body type variable should be turned into a dummy encoded variable,
% the transmission type is re-encoded into "has automatic", and the fuel
% type into "is hybrid"

CX = CARS1.X();
CX(:,4) = (CX(:,1) == 1);
CX(:,5) = (CX(:,1) == 2);
CX(CX(:,3) == 2, 3) = 0;
CX = CX(:,[4:end 2 3]);
CARS1.setX(CX);

% CX = CARS2.X();
% CX(:,5) = (CX(:,1) == 1);
% CX(:,6) = (CX(:,1) == 2);
% CX(:,7) = (CX(:,1) == 3);
% CX(:,2) = CX(:,2) - 1;
% CX(CX(:,4) == 2, 4) = 0;
% CX = CX(:,[5:end 2 3 4]);
% CARS2.setX(CX); clear CX;

% Normalize instances of the basic datasets

normfun = @(d) d.normalize();
cellfun(normfun, DATASETS, 'Un', 0);

% Conversion to classification dataset
%
% Depending on the method used, a classification-based representation is
% needed instead of actual binary choice datasets. The conversion is based
% on a feature comparison between preferred and dispreferred alternatives.
% The BinaryChoiceDataset provides several modes of doing this per feature
% (see the documentation of BinaryChoiceDataset). In short, for a choice
% between two alternatives a1, a2, the value in dimension d is computed as
% follows:
%
% Conversion mode       Meaning
% 0:                    Differences, X(a1,d) - X(a2,d)
% 1:                    Ratio, X(a1,d) / X(a2,d)
% 2:                    Log of the ratio, log(X(a1,d) / X(a2,d))
% 3:                    Log of the modified ratio, log((X(a1,d) + 1) / (X(a2,d) + 1))

CL_SUSHI = SUSHI.toClassificationDataset([0 0 0 0], true);
% CL_MOVIELENS = MOVIELENS.toClassificationDataset([], MIRROR); % All binary features
CL_ELECTION = ELECTION.toClassificationDataset([], false);
% CL_TARIFFS = TARIFFS.toClassificationDataset([0, 0, 0, 0], MIRROR);
CL_TARIFFSFULL = TARIFFSFULL.toClassificationDataset([ 0, 0, 0, 0, 0 ], true);
CL_CARS1 = CARS1.toClassificationDataset([], true);
% CL_CARS2 = CARS2.toClassificationDataset([], MIRROR);

DATASETS_CL = { CL_SUSHI, CL_ELECTION, CL_TARIFFSFULL, CL_CARS1 };

% Compute median distances between datapoints as basis for lengthscale
% determination

% DATASETS_MEDIANDIST = zeros(numel(DATASETS_CL), 1);
% for dIdx = 1:numel(DATASETS_CL)
%     X = DATASETS_CL{dIdx}.X();
%     NX = DATASETS_CL{dIdx}.NX();
%     
%     MAXX = 1E4;
%     if(size(X, 1) > MAXX)
%         % Approximate distances for large datasets
%         X = X(1:MAXX,:);
%         NX = MAXX;
%     end
%     
%     Q = repmat(sum(X.^2, 2), 1, NX);            
%     dist = Q + Q' - 2*(X*X');
%     idx = find(~tril(ones(size(dist)))); % Lower triangle indices            
%     DATASETS_MEDIANDIST(dIdx) = sqrt(0.5 * median(dist(idx)));
% end
% clear X Q dIdx dist idx MAXX NX

% Precomputed results
DATASETS_MEDIANDIST = [2.65396054779538 3.08425646683436 2.66024127242416 2.41226250577309];

% Discretization
%
% Some methods work better with discretized values. The following
% statements create these discretized versions of the classification
% datasets.

CLD_SUSHI = ClassificationDataset(); CLD_SUSHI.copy(CL_SUSHI); 
% CLD_SUSHI.discretize([ 3 3 15 15 ], 'jenks');
CLD_SUSHI.discretizeWithEdges({[],[],...
    [-3.1173;-2.7500;-2.0953;-1.6626;-1.1942;-0.8269;-0.5870;-0.2561;0.1186;0.4888;0.7054;1.0758;1.5250;1.9231;2.4120;3.1173], ...
    [-3.2205;-3.2089;-2.7003;-2.0954;-1.5868;-1.1135;-0.7490;-0.2403;0.1432;0.5203;0.9037;1.3465;1.6500;2.4599;2.7635;3.2205]});

% CLD_MOVIELENS = ClassificationDataset(); CLD_MOVIELENS.copy(CL_MOVIELENS); % All binary features

CLD_ELECTION = ClassificationDataset(); CLD_ELECTION.copy(CL_ELECTION); 
CLD_ELECTION.discretize([3 3 5 3 5 3 5 5], 'jenks');
CLD_ELECTION.discretizeWithEdges({...
    [-2.8036;-0.8714;0.6289;2.8036], ...
    [-2.3742;-1.3171;0.1384;2.3742], ...
    [-2.6676;-1.7957;-0.5997;0.2722;1.5097;2.6676], ...
    [-2.7088;-1.6263;0.2625;2.7088], ...
    [-3.0237;-2.0200;-0.5634;0.4493;1.6560;3.0237], ...
    [-2.5673;-1.5198;0.5410;2.5673], ...
    [-2.7180;-2.3398;-1.2136;0.0384;1.2136;2.7180], ...
    [-2.3872;-1.7320;-0.3700;0.2117;0.6322;2.3872]});

% CLD_ELECTION.discretize([3 3 5 3 8 3 8 8], 'jenks');
% CLD_ELECTION.discretizeWithEdges({...
%      [-2.8036;-0.8714;0.6289;2.8036], ...
%      [-2.3742;-1.3171;0.1384;2.3742], ...
%      [-2.6676;-1.7957;-0.5997;0.2722;1.5097;2.6676], ...
%      [-2.7088;-1.6263;0.2625;2.7088], ...
%      [-3.0237;-2.6687;-1.6560;-0.7315;-0.0729;0.6487;1.3677;2.2194;3.0237], ...
%      [-2.5673;-1.5198;0.5410;2.5673], ...
%      [-2.7180;-2.3398;-1.2136;-0.5187;-0.1250;0.3040;0.8223;2.0358;2.7180], ...
%      [-2.3872;-2.1525;-1.7320;-0.3700;-0.0230;0.2117;0.6322;1.8055;2.3872]});

% CLD_TARIFFS = ClassificationDataset(); CLD_TARIFFS.copy(CL_TARIFFS); CLD_TARIFFS.discretize([ 3 10 10 10 ], 'jenks');

CLD_TARIFFSFULL = ClassificationDataset(); CLD_TARIFFSFULL.copy(CL_TARIFFSFULL); 
% CLD_TARIFFSFULL.discretize([ 8 8 8 3 8 ], 'jenks');
CLD_TARIFFSFULL.discretizeWithEdges({...
    [-2.4587;-2.2848;-1.9868;-0.2235;-0.0248;0.1987;0.4719;2.2352;2.4587], ...
    [-5.6849;-3.6700;-2.3027;-1.2233;-0.2878;0.7268;1.9429;3.3102;5.6849], ...
    [-5.2506;-2.9490;-1.7982;-0.8631;0;0.8200;1.7262;2.8051;5.2506], ...
    [], ...
    [-6.3830;-5.1064;-3.0851;-1.2766;-0.1064;1.1702;2.5532;3.8298;6.3830]});

CLD_CARS1 = ClassificationDataset(); CLD_CARS1.copy(CL_CARS1); 
% CLD_CARS1.discretize([ 3 3 10 3 ], 'jenks');
CLD_CARS1.discretizeWithEdges({[], [], ...
    [-3.0690;-3.0690;-2.4884;-2.2396;-1.4101;-0.5806;0;0.8295;1.6589;2.2396;3.0690], ...
    []});

% CLD_CARS2 = ClassificationDataset(); CLD_CARS2.copy(CL_CARS2); CLD_CARS2.discretize([ 3 3 3 3 10 3 ], 'jenks');
DATASETS_CLD = { CLD_SUSHI, CLD_ELECTION, CLD_TARIFFSFULL, CLD_CARS1 };

% Normalization
%
% Normalize the instances of all remaining datasets

%cellfun(normfun, DATASETS_CL, 'Un', 0);
%cellfun(normfun, DATASETS_CLD, 'Un', 0);

% Throw out the individual dataset objects to un-clutter the workspace

clear MIRROR;
clear SUSHI CL_SUSHI CLD_SUSHI;
clear MOVIELENS CL_MOVIELENS CLD_MOVIELENS;
clear ELECTION CL_ELECTION CLD_ELECTION;
clear TARIFFS CL_TARIFFS CLD_TARIFFS;
clear TARIFFSFULL CL_TARIFFSFULL CLD_TARIFFSFULL;
clear CARS1 CL_CARS1 CLD_CARS1;
clear CARS2 CL_CARS2 CLD_CARS2;
clear normfun ans;
