PATH = '/Users/Markus/workspace/isr14/generated';

LINECOLOR = { 'k', 'k', 'k', 'k', 'k', 'k', 'k' };
LINESTYLE = { '-', ':', ':', '--', '-.', '-', '-' };
LINEMARKER = { '.', '.', 'o', '.', '.', 'o', '^' };
LINEWIDTH = { 2.5, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0 };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Part 1:
%
% Benchmark results
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

load('results_base.mat');

SELECTION = [5 1 2 4];
TRAININGSELECTION = 1:11; % Only up to 90%
DATASETSELECTION = 1:4;

prepTimeAll = prepTime(:,SELECTION,DATASETSELECTION,TRAININGSELECTION);
trainTimeAll = trainTime(:,SELECTION,DATASETSELECTION,TRAININGSELECTION);
testTimeAll = testTime(:,SELECTION,DATASETSELECTION,TRAININGSELECTION);
accuracyAll = accuracy(:,SELECTION,DATASETSELECTION,TRAININGSELECTION);
certaintyAll = certainty(:,SELECTION,DATASETSELECTION,TRAININGSELECTION);

experimentAll = experiment(SELECTION);
experimentOptionsAll = experimentOptions(SELECTION);

errorMarkerAll = errorMarker(:,SELECTION,DATASETSELECTION,TRAININGSELECTION);

EXPERIMENT_NAMES_ALL = EXPERIMENT_NAMES(SELECTION);
EXPERIMENT_NAMES_ALL = cellfun(@(n) strrep(n, 'COLGPExperiment METHOD=Birlutiu', 'Hierarchical GP'), EXPERIMENT_NAMES_ALL, 'Un', 0);
EXPERIMENT_NAMES_ALL = cellfun(@(n) strrep(n, 'COLGPExperiment METHOD=GPC', 'GP Classification'), EXPERIMENT_NAMES_ALL, 'Un', 0);
EXPERIMENT_NAMES_ALL = cellfun(@(n) ifthenelse(strfind(n, 'MixedLogitExperiment') == 1, 'Mixed Logit', n), EXPERIMENT_NAMES_ALL, 'Un', 0);
EXPERIMENT_NAMES_ALL = cellfun(@(n) ifthenelse(strfind(n, 'CGPCExperiment') == 1, 'GTM', n), EXPERIMENT_NAMES_ALL, 'Un', 0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Part 2:
%
% Additional benchmark results for COLGP (MPLFITC) which failed in the
% original run above
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


load('results_colgp.mat');

prepTime = prepTime(:,:,DATASETSELECTION,TRAININGSELECTION);
trainTime = trainTime(:,:,DATASETSELECTION,TRAININGSELECTION);
testTime = testTime(:,:,DATASETSELECTION,TRAININGSELECTION);
accuracy = accuracy(:,:,DATASETSELECTION,TRAININGSELECTION);
certainty = certainty(:,:,DATASETSELECTION,TRAININGSELECTION);

prepTimeAll = cat(2, prepTimeAll, prepTime);
trainTimeAll = cat(2, trainTimeAll, trainTime);
testTimeAll = cat(2, testTimeAll, testTime);
accuracyAll = cat(2, accuracyAll, accuracy);
certaintyAll = cat(2, certaintyAll, certainty);

experimentAll = [ experimentAll, experiment ];
experimentOptionsAll = [ experimentOptionsAll, experimentOptions ];

errorMarker = errorMarker(:,:,DATASETSELECTION,TRAININGSELECTION);
errorMarkerAll = cat(2, errorMarkerAll, errorMarker);

EXPERIMENT_NAMES_ALL = [ EXPERIMENT_NAMES_ALL EXPERIMENT_NAMES ];
EXPERIMENT_NAMES_ALL = cellfun(@(n) ifthenelse(strfind(n, 'COLGPExperiment METHOD=MPLFITC_NU') == 1, 'Collaborative GP', n), EXPERIMENT_NAMES_ALL, 'Un', 0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Analysis
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prepdata = @(M) (1 - errorMarkerAll) .* ~isnan(M) .* M;

samples = squeeze(sum(errorMarkerAll == 0,1));

accuracyAll(accuracyAll == 0) = NaN;
accuracy_mean = squeeze(nanmean(prepdata(accuracyAll),1));
accuracy_stddev = squeeze(nanstd(prepdata(accuracyAll),0,1));
accuracy_stderr = 1.96 * (accuracy_stddev ./ sqrt(samples));

trainTimeAll(trainTimeAll == 0)= NaN;
trainTime_mean = squeeze(nanmean(prepdata(trainTimeAll),1));
trainTime_stddev = squeeze(nanstd(prepdata(trainTimeAll),0,1));
trainTime_stderr = 1.96 * (trainTime_stddev ./ sqrt(samples));

testTimeAll(testTimeAll == 0) = NaN;
testTime_mean = squeeze(nanmean(prepdata(testTimeAll),1));
testTime_stddev = squeeze(nanstd(prepdata(testTimeAll),0,1));
testTime_stderr = 1.96 * (testTime_stddev ./ sqrt(samples));

LABELSELECTION = [1:5 7 9 11];

XLabels = arrayfun(@(p) sprintf('%d%%', p), round(TRAININGFRACTION(LABELSELECTION) * 100), 'Un', 0);

Ndatasets = size(accuracy_mean, 2);
for d = 1:Ndatasets
   
    accuracy_mean_D = squeeze(accuracy_mean(:,d,:));
    accuracy_stderr_D = squeeze(accuracy_stderr(:,d,:));
    trainTime_mean_D = squeeze(trainTime_mean(:,d,:));
    trainTime_stderr_D = squeeze(trainTime_stderr(:,d,:));
    testTime_mean_D = squeeze(testTime_mean(:,d,:));
    testTime_stderr_D = squeeze(testTime_stderr(:,d,:));
    
    hFig = figure();
    set(gca,'FontSize', 15);
    set(hFig, 'Visible', 'off');
    for alg = 1:size(accuracy_mean_D, 1)
        h = errorbar(TRAININGFRACTION, accuracy_mean_D(alg,:)', accuracy_stderr_D(alg,:)', ...
            'Color', LINECOLOR{alg}, 'LineStyle', LINESTYLE{alg}, ...
            'LineWidth', LINEWIDTH{alg}, 'Marker', LINEMARKER{alg});    
        hold on;
    end
        
    set(gca,'XTick', TRAININGFRACTION(LABELSELECTION))
    set(gca,'XTickLabel', XLabels);
    xlabel('Training Fraction');
    ylabel('Predictive Accuracy');
    xlim([.18, .92]);
    ylim([0.4, 1.0]);
    legend(EXPERIMENT_NAMES_ALL, 'Location', 'SouthEast');
    print(gcf, '-depsc', sprintf('%s/accuracy_%s.eps', PATH, DATASET_NAMES{d}), '-r 600');      
    hold off;
        
    hFig = figure();
    set(gca,'FontSize', 15);
    set(hFig, 'Visible', 'off');
    for alg = 1:size(accuracy_mean_D, 1)
        h = errorbar(TRAININGFRACTION, trainTime_mean_D(alg,:)', trainTime_stderr_D(alg,:)', ...
            'Color', LINECOLOR{alg}, 'LineStyle', LINESTYLE{alg}, ...
            'LineWidth', LINEWIDTH{alg}, 'Marker', LINEMARKER{alg});
        hold on;
    end
    
    set(gca,'XTick',TRAININGFRACTION(LABELSELECTION))
    set(gca,'XTickLabel', XLabels);
    % set(get(h, 'Parent'), 'YScale', 'log');
    xlabel('Training Fraction');
    ylabel('Training Time (s)');
    xlim([.18, .92]);
    legend(EXPERIMENT_NAMES_ALL, 'Location', 'NorthWest');
    print(gcf, '-depsc', sprintf('%s/trainingtime_%s.eps', PATH, DATASET_NAMES{d}), '-r 600');      
    hold off;
    
    hFig = figure();
    set(gca,'FontSize', 15);
    set(hFig, 'Visible', 'off');
    for alg = 1:size(accuracy_mean_D, 1)
        h = errorbar(TRAININGFRACTION, testTime_mean_D(alg,:)', testTime_stderr_D(alg,:)', ...
            'Color', LINECOLOR{alg}, 'LineStyle', LINESTYLE{alg}, ...
            'LineWidth', LINEWIDTH{alg}, 'Marker', LINEMARKER{alg});
        hold on;
    end
    
    set(gca,'XTick', TRAININGFRACTION(LABELSELECTION))
    set(gca,'XTickLabel', XLabels);
    xlabel('Training Fraction');
    ylabel('Test Time (s)');
    xlim([.18, .92]);
    legend(EXPERIMENT_NAMES_ALL, 'Location', 'East');
    print(gcf, '-depsc', sprintf('%s/testtime_%s.eps', PATH, DATASET_NAMES{d}), '-r 600');      
    hold off;
     
end