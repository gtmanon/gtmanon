classdef Experiment < handle
    
    properties(Access = 'public')
        
        % Fraction of the input data used for training. Must be in (0,1].
        % Defaults to 0.75.
        trainingFraction;        
                
    end
    
    properties(GetAccess = 'public', SetAccess = 'protected')
        
        % Training time in CPU seconds. Available only after the experiment terminates.
        trainT;
        
        % Test time in CPU seconds. Available only after the experiment terminates.
        testT; 
        
        % Preparation time in CPU seconds. Available only after the experiment terminates.
        prepT;
        
        % Accuracy indicator in [0,1]. Available only after the experiment
        % terminates.
        accuracy;
        
        % Optional input parameters. see setInput
        inData;
        
        % Optional outputs. see getOutput
        outData;
        
        % Random seed to which the random stream is set before running the
        % experiment
        randomSeed;
        
        % The actual dataset. This is either empty or of the type indicated
        % by the implementing subclass through getDatasetType().
        D;
        
    end    
    
    methods (Abstract=true) 
        
        prepare(obj);
        
        train(obj);
        
        test(obj);
        
        cleanup(obj);
        
        getDescription(obj);
        
        getDatasetType(obj);
        
    end
    
    methods(Static, Access='protected')
        
        function exlog(varargin)
            fprintf('%s: ', datestr(now,'mmmm dd, yyyy HH:MM:SS.FFF AM'));
            fprintf(varargin{:});
        end
        
        function dumpMap(map, prefix)
            
            keySet = keys(map);
            
            for keyIdx = 1:numel(keySet)
                key = keySet{keyIdx};
                value = map(key);
                
                if(ischar(value))
                    Experiment.exlog('%s: %s=%s\n', prefix, key, value);
                elseif(isscalar(value))                    
                    if(~isa(value, 'function_handle'))
                        Experiment.exlog('%s: %s=%f\n', prefix, key, value);
                    else
                        Experiment.exlog('%s: %s=(FUNCTION HANDLE)\n', prefix, key);
                    end
                else
                    Experiment.exlog('%s: %s=(STRUCTURED)\n', prefix, key);
                end
            end
            
        end
    end
    
    methods    
        
        function obj = Experiment(dataset)
            
            obj.trainingFraction = 0.75;
            
            obj.inData = containers.Map;
        
            obj.trainT = NaN;        
            obj.testT = NaN;        
            obj.prepT = NaN;        
            obj.accuracy = NaN;
            
            obj.outData = containers.Map;
            
            obj.randomSeed = 1;
            
            if(nargin >= 1 && ~isempty(dataset))
                obj.setDataset(dataset);
            end
        
        end
        
        function setDataset(obj, dataset)
            if(~isa(dataset, obj.getDatasetType()))
                error('This experiment requires a %s as input.', obj.getDatasetType());
            end
            
            obj.D = dataset;  
        end
        
        function name = getName(obj)
            name = class(obj);
        end
        
        function setRandomSeed(obj, seed)
            obj.randomSeed = seed;
        end
        
        function setInput(obj, param, value)
            % Store 'value' as optional named input parameter 'key'
            %
            % You can use this facility either to pass optional parameters
            % to the experiment (make sure that your experiment actually
            % uses the parameter 'param'), or to pass descriptive
            % information into the experiment, since the input and output
            % attributes are dumped together with the experimental result.
            obj.inData(param) = value;
        end    
        
        function ret = hasInput(obj, param)
            % Return true if param was given as optional parameter
            
            ret = obj.inData.isKey(param);            
        end
        
        function clearInput(obj)
            obj.inData = containers.Map;
        end
        
        function value = getOutput(obj, out)
            % Retrieve the optional output 'out'
            %
            % Experiment may choose to store optional, named outputs 
            % specific to the experiment which may be retrieved
            % using this facility. They are also logged together with the
            % experimental results.
            value = obj.outData(out);
        end
        
        function run(obj)
                          
            BASEDIR = pwd();
            
            obj.exlog('%s: Starting experiment (seed = %d).\n', obj.getName(), obj.randomSeed);
            obj.exlog('%s\n', obj.getDescription());
            
            % Clear optional outputs
            obj.outData = containers.Map;
            
            stream = RandStream('mrg32k3a');
            RandStream.setGlobalStream(stream);
            stream.Substream = obj.randomSeed;
            
            Experiment.dumpMap(obj.inData, 'IN');
                        
            obj.prepT = cputime;            
            try 
                obj.prepare();
            catch err
                cd(BASEDIR);
                
                for e=1:length(err.stack)
                    obj.exlog(sprintf('%s at %i\n',err.stack(e).name,err.stack(e).line));
                end  
                error('%s: Failure during prepare(): %s', obj.getName(), err.message);
            end            
            obj.prepT = cputime - obj.prepT;
            
            obj.exlog('%s: Done preparing (%.2f CPUsec). Starting training with %d%% of data.\n', ...
                obj.getName(), obj.prepT, obj.trainingFraction * 100);
            
            if(obj.hasSeparateTesting())
            
                obj.trainT = cputime;
                try
                    obj.train();
                catch err
                    cd(BASEDIR);
                    obj.cleanup(); 
                                       
                    for e=1:length(err.stack)
                        obj.exlog(sprintf('%s at %i\n',err.stack(e).name,err.stack(e).line));
                    end                    
                    error('%s: Failure during train(): %s', obj.getName(), err.message);
                end
                obj.trainT = cputime - obj.trainT;

                obj.exlog('%s: Done training (%.2f CPUsec). Starting testing.\n', ...
                    obj.getName(), obj.trainT);

                obj.testT = cputime;
                try
                    obj.test()
                catch err
                    cd(BASEDIR);
                    obj.cleanup(); 
                    
                    for e=1:length(err.stack)
                        obj.exlog(sprintf('%s at %i\n',err.stack(e).name,err.stack(e).line));
                    end            
                    error('%s: Failure during test(): %s', obj.getName(), err.message);
                end
                obj.testT = cputime - obj.testT;

                obj.exlog('%s: Done testing (%.2f CPUsec, accuracy %.4f). Finishing experiment.\n', ...
                    obj.getName(), obj.testT, obj.accuracy);
            
            else            
                try
                    [ obj.trainT, obj.testT ] = obj.train();
                catch err
                    cd(BASEDIR);
                    obj.cleanup();  
                    
                    for e=1:length(err.stack)
                        obj.exlog(sprintf('%s at %i\n',err.stack(e).name,err.stack(e).line));
                    end            
                    error('%s: Failure during train(): %s', obj.getName(), err.message);
                end
                
                obj.exlog('%s: Done training/testing (%.2f CPUsec train, %.2f CPUsec test, accuracy %.4f). Finishing experiment.\n', ...
                          obj.getName(), obj.trainT, obj.testT, obj.accuracy);                            
            end
            
            Experiment.dumpMap(obj.outData, 'OUT');
            
            try
                obj.cleanup();
            catch err
                
                for e=1:length(err.stack)
                    obj.exlog(sprintf('%s at %i\n',err.stack(e).name,err.stack(e).line));
                end                
                warning('%s: Failure during cleanup(): %s', obj.getName(), err.message);
            end
            
            cd(BASEDIR);
            
        end    
        
    end
    
    methods(Access = 'protected')
        
        function value = getInput(obj, param)
            value = obj.inData(param);
        end
        
        function setOutput(obj, out, value)
            obj.outData(out) = value;
        end
                
        function value = hasSeparateTesting(obj)
            % Indicates whether the experiment makes use of a separate
            % testing phase
            %
            % If this function returns false, Experiment.run() expects that
            % testing is done as part of the train() call, and that timeing
            % information is returned from that call which in this case has
            % the signature:
            %
            % [success, trainingTime, testTime] = train()
            
            value = 1;
        end
        
    end
    
end

