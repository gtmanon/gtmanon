classdef MOGPExperiment < Experiment

    properties(Constant)
        
        MOGP_DIR = 'benchmarks/mogp';
        
    end
    
    properties(Access = 'private')
        
        suffix; 
        
    end
    
    methods
        
        function obj = MOGPExperiment(binaryChoiceDataset)   
            
            if(nargin == 0)
                binaryChoiceDataset = [];
            end
            
            obj@Experiment(binaryChoiceDataset);
            
        end
                
        function type = getDatasetType(obj)
            type = 'BinaryChoiceDataset';
        end
        
    end
    
    methods
        
        function d = getDescription(obj)
            
            d = 'Binary choice prediction using GP preference learning as described in Abbasnejad et al. (IJCAI, 2013).';
            
        end
        
        function prepare(obj)     
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
           
            % Pseudo-unique filename suffix
            obj.suffix = datestr(now,'ddmmyyyy_HHMMSS_FFF');
            
            x = obj.D.X();    
            u = obj.D.getUsers().X();
    
            choiceUsers = obj.D.getChoiceUsers();
            [ preferred, dispreferred ] = obj.D.getChoiceIndices();
    
            N = size(preferred, 1);    
            [trainingIndices, testIndices] = obj.D.splitChoiceIndices(round(N * obj.trainingFraction), true);
    
            pref = [ choiceUsers(trainingIndices) preferred(trainingIndices), dispreferred(trainingIndices) ];
            pref_test = [ choiceUsers(testIndices) preferred(testIndices), dispreferred(testIndices) ];
    
            dlmwrite(strcat(obj.mogpDir, '/x_', obj.suffix , '.csv'), x);
            dlmwrite(strcat(obj.mogpDir, '/u_', obj.suffix , '.csv'), u);
            dlmwrite(strcat(obj.mogpDir, '/pref_', obj.suffix , '.csv'), pref);
            dlmwrite(strcat(obj.mogpDir, '/pref_', obj.suffix , '_test.csv'), pref_test);
                                   
        end
        
        function train(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
                       
            oldFolder = cd(obj.mogpDir);
            mogp_simple(obj.suffix, false);
            cd(oldFolder);
                      
        end
        
        function test(obj)
            
        end
        
        function cleanup(obj)
            
            if(isempty(obj.D))
                error('You must set a dataset, either through the constructor or through setDataset() before calling this method.');
            end
           
            delete(strcat(obj.mogpDir, '/x_', obj.suffix , '.csv'));
            delete(strcat(obj.mogpDir, '/u_', obj.suffix , '.csv'));
            delete(strcat(obj.mogpDir, '/pref_', obj.suffix , '.csv'));
            delete(strcat(obj.mogpDir, '/pref_', obj.suffix , '_test.csv'));            
            
        end
        
    end    
end

