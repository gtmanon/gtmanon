function test_suite = kron_pref_test
% Unit tests for preference learning with structure GPs
    
    initTestSuite;     
        
function fixture = setup(fixture)

    fixture.LS = [1, 2, 3]; % Lengthscale
    fixture.SIGMA = 0.5; % Noise stddev
    
    fixture.PLS = prior_logunif();      
    fixture.PSIGMA = prior_sqrtunif();
    
    fixture.gpcf_reg = gpcf_sexp('lengthScale', fixture.LS, ...
                         'magnSigma2', fixture.SIGMA, ...
                         'lengthScale_prior', fixture.PLS, ...
                         'magnSigma2_prior', fixture.PSIGMA);
                     
    fixture.gpcf_kron = gpcf_kronsexp('lengthScale', fixture.LS, ...
                         'magnSigma2', fixture.SIGMA, ...
                         'lengthScale_prior', fixture.PLS, ...
                          'magnSigma2_prior', fixture.PSIGMA);
    
    
function testSingleUser(fixture)


    % Utilities of dimensions: 1, 2, 2.5
    instances = [ 1 1 0;
                  0 1 1;
                  0 1 0;
                  0 0 0;
                  1 1 1;
                  0 0 1];
                                             
    users = [ 1 ];
    alternatives = [ 1 2; 2 3; 3 4; 4 5; 5 4; 1 5; 2 4; 1 6 ];    
    choiceUsers = [ 1; 1; 1; 1; 1; 1; 1; 1 ]; 
    choices = [ 2; 2; 1; 2; 1; 2; 1; 1 ];
    
    pdata = BinaryChoiceDataset;
    pdata.set(instances, users, choiceUsers, alternatives, choices);
    
    CLD = pdata.toClassificationDataset([0 0 0]);

    assertElementsAlmostEqual(CLD.X(), ...
                                [ 1     0    -1
                                  0     0     1
                                  0     1     0
                                 -1    -1    -1
                                  1     1     1
                                  0     0    -1
                                  0     1     1
                                  1     1    -1], 'relative', 0.0, 1e-10);    
                              
    [ GDS, rowGridIndices ] = CLD.toGrid(false, []);
    AX = GDS.getAxes()';
    Ytrain = GDS.YBinary([], true);
                              
    assertElementsAlmostEqual(rowGridIndices, ...
                                [ 22 15 17 1 27 13 18 25 ]', 'relative', 0.0, 1e-10);    
                 
    % Re-convert to 1/2 labels
    YtrainReconv = Ytrain.values;
    YtrainReconv(YtrainReconv == -1) = 2;
                            
    assertElementsAlmostEqual(rowGridIndices, Ytrain.indices, 'relative', 0.0, 1e-10);
    assertElementsAlmostEqual(choices, YtrainReconv, 'relative', 0.0, 1e-10);
                               
    K = KroneckerSEXPCov(AX);
    K.updateParameters(log([fixture.SIGMA fixture.LS]));

    Kfullkron = K.fullK();      
    
    gpreg = gp_set('lik', lik_probit_idnoise(), 'cf', fixture.gpcf_reg);
    Kfullreg = gp_trcov(gpreg, CLD.X());
    
    assertElementsAlmostEqual(Kfullkron(rowGridIndices,rowGridIndices), Kfullreg, 'relative', 0.0, 1e-10);


    gp = gp_set('lik', lik_probit_idnoise(), 'cf', fixture.gpcf_kron, ...
                'jitterSigma2', 1e-9, 'KroneckerCov', K, ...
                'EXMAX', 150, 'FORCEAPPROX', false, 'DEBUG', false, ...
                'SKIPIMPLICIT', false, 'EIGENLIMIT', 0.01, ...
                'EIGENCOUNTLIMIT', 250, 'EIGENSAFE', 0.1, ...
                'CGACCURACY', 1E-6);

    opt=optimset('TolFun', 1e-2, 'TolX', 1e-2, 'DerivativeCheck', 'on', 'Display', 'iter');
    opt.lambdalim = 1E6;

    gp=gp_optim(gp, AX, { Ytrain }, 'optimf', @fminscg, 'opt', opt);
    [ E, Var ] = gp_pred(gp, AX, { Ytrain }, AX);
    
    accuracy_kron = mean(sign(Ytrain.values) == sign(E(Ytrain.indices)));
        
    
      
    % assertTrue(all(userIndices == [1; 3]), 'User indices must be computed correctly');
    
