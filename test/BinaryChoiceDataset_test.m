function test_suite = BinaryChoiceDataset_test
% Unit tests for BinaryChoiceDataset
    
    initTestSuite;     
        
function testReduce1

    instances = [10; 20; 30; 40; 50; 60; 70; 80 ];
    users = [ 11; 12; 13; 14; 15 ];
    alternatives = [ 2 1 ; 3 1; 2 1 ];    
    choiceUsers = [ 1; 3; 2 ]; 
    choices = [ 1; 2; 1 ];
    
    pdata = BinaryChoiceDataset;
    pdata.set(instances, users, choiceUsers, alternatives, choices);
    
    % Create subset based on first two choices
    [pred, instanceIndices, userIndices] = pdata.reduce(2, false);
    
    assertTrue(pred.NX() == 3, 'Reduced number of instances must match');
    assertTrue(pred.DX() == 1, 'Dimensionality of instances must match');
    assertTrue(all(pred.X() == [10; 20; 30]), 'Instances must be correctly reduced' );
    
    assertTrue(size(pred.getChoices(), 1) == 2, 'Reduced number of choices must match');
    assertTrue(size(pred.getChoices(), 2) == 1, 'Dimensionality of choices must match');
    assertTrue(all(all(pred.getChoices() == [ 1 ; 2 ])), 'Choices must be correctly reduced');
    
    assertTrue(all(all(pred.getAlternatives() == [ 2 1; 3 1 ])), 'Alternatives must be correctly reduced');
    
    assertTrue(all(instanceIndices == [1; 2; 3]), 'Instance indices must be computed correctly');
    assertTrue(all(userIndices == [1; 3]), 'User indices must be computed correctly');
    
function testReduce2

    instances = [10, 11; 20, 21; 30, 31; 40, 41; ...
                 50, 51; 60, 61; 70, 71; 80, 81 ];
             
    users = [ 11; 12; 13; 14; 15 ];
    alternatives = [ 2 3 ; 3 2; 7 8; 5 1; 2 4 ];    
    choiceUsers = [ 1; 5; 1; 2; 4 ]; 
    choices = [ 1; 2; 1; 1; 2 ];             
             
    pdata = BinaryChoiceDataset;
    pdata.set(instances, users, choiceUsers, alternatives, choices);
    
    % Create subset based on first three choices
    [pred, instanceIndices, userIndices] = pdata.reduce(3, false);
    
    % Instance indices must be mapped as follows:
    % 2 -> 1, 3 -> 2, 7 -> 3, 8 -> 4    
    assertTrue(pred.NX() == 4, 'Reduced number of instances must match');
    assertTrue(pred.DX() == 2, 'Dimensionality of instances must match');
    assertTrue(all(all(pred.X() == [20, 21; 30, 31; 70, 71; 80, 81 ])), 'Instances must be correctly reduced');
    
    assertTrue(size(pred.getAlternatives(),1) == 3, 'Reduced number of choices must match');
    assertTrue(size(pred.getAlternatives(),2) == 2, 'Dimensionality of choices must still match');
    assertTrue(all(all(pred.getAlternatives() == [ 1 2; 2 1; 3 4 ])), 'Choices must be correctly reduced');
    
    assertTrue(all(instanceIndices == [2; 3; 7; 8]), 'Instance indices must be computed correctly');
    assertTrue(all(userIndices == [1; 5]), 'User indices must be computed correctly');
    
function testReducePerUser
    instances = [10; 20; 30; 40; 50; 60; 70; 80 ];
    
    users = [ 11; 12; 13; 14; 15 ];
    alternatives = [ 2 1 ; 3 1; 4 1; 2 1; 2 1; 3 2 ];    
    choiceUsers = [ 1; 1; 1; 2; 3; 3 ]; 
    choices = [ 1; 1; 1; 1; 1; 1 ];  
        
    pdata = BinaryChoiceDataset;
    pdata.set(instances, users, choiceUsers, alternatives, choices);
    
    % Create subset with a maximum of 2 choices per user
    [pred, instanceIndices, userIndices] = pdata.reduce(2, false, true);
    
    assertTrue(pred.NX() == 3, 'Reduced number of instances must match');    
    
    % Two choices for users 1 and 3, each. Plus one statement for user 2
    assertTrue(size(pred.getAlternatives(), 1) == 5, 'Reduced number of choices must match');
    assertTrue(size(pred.getAlternatives(), 2) == 2, 'Dimensionality of choices must still match');
    
    assertTrue(all(instanceIndices == [1; 2; 3]), 'Instance indices must be computed correctly');
    assertTrue(all(userIndices == [1; 2; 3]), 'User indices must be computed correctly');
    
function testSelectUsers
    instances = [10; 20; 30; 40; 50; 60; 70; 80 ];
    
    users = [ 11; 12; 13; 14; 15 ];
    alternatives = [ 2 1 ; 3 1; 4 1; 2 1; 2 1; 4 2 ];    
    choiceUsers = [ 1; 1; 1; 2; 3; 3 ]; 
    choices = [ 1; 1; 1; 1; 1; 1 ];  
        
    pdata = BinaryChoiceDataset;
    pdata.set(instances, users, choiceUsers, alternatives, choices);
    
    % Create subset with only users 2 and 3 
    [pred, instanceIndices, userIndices] = pdata.selectUsers([2, 3]);
    
    % Instance 4 is only referred to by the last choice of user 1 which 
    % should be filtered out.
    assertTrue(pred.NX() == 3, 'Reduced number of instances must match');    
    
    % Two choices for users 1 and 3, each. Plus one statement for user 2
    assertTrue(size(pred.getAlternatives(), 1) == 3, 'Reduced number of choices must match');
    assertTrue(size(pred.getAlternatives(), 2) == 2, 'Dimensionality of choices must still match');
    
    assertTrue(all(instanceIndices == [1; 2; 4]), 'Instance indices must be computed correctly');
    assertTrue(all(userIndices == [2; 3]), 'User indices must be computed correctly');
    
function testSynthetic

    S = SyntheticBinaryChoiceDataset('InstancesPerRelation', 10, ...
        'InstanceDimensionality', 5, ...
        'NumberOfClusters', 3, ...
        'RelationsPerCluster', 10, ...
        'SigmaRelation', 0.05, ...
        'SigmaPreference', 0.05);
    
    S.load();
    
    assertTrue(S.NX() == 10);
    assertTrue(S.DX() == 5);
    assertTrue(S.NC() == 1350);
    

