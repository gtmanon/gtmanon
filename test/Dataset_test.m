function test_suite = Dataset_test
% Unit tests for Dataset
    
    initTestSuite;     
        
function testCreation

    D = Dataset;
    
    D.setX([5, 2, 3
            5, 3, 12
            -1, 2, 5
            -1, 3, 8
            -1, 6, 7]);
                
    D.discretize([1,2,2], 'equalcount');
    
    assertElementsAlmostEqual(D.X(), ...
        [   1.4000    2.0000    4.0000
            1.4000    4.0000    9.0000
            1.4000    2.0000    4.0000
            1.4000    4.0000    9.0000
            1.4000    4.0000    9.0000 ]);
        
    D.setX([5, 2, 3
            5, 3, 12
            -1, 2, 5
            -1, 3, 8
            -1, 6, 7]);
        
    D.discretize([1,2,2], 'equalspace');
    
    assertElementsAlmostEqual(D.X(), ...
        [   1.4000    3.0000    5.2500
            1.4000    3.0000    9.7500
            1.4000    3.0000    5.2500
            1.4000    3.0000    9.7500
            1.4000    5.0000    5.2500 ]);
        
%    D.discretize([1,2,2], 'kmeans');
    
%    assertElementsAlmostEqual(D.X(), ...
%        [   1.4000    3.4000    7.0500
%            1.4000    3.4000    7.0500
%            1.4000    3.4000    7.0500
%            1.4000    3.4000    7.0500
%            1.4000    3.4000    7.0500 ]);