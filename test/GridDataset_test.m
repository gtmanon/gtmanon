function test_suite = GridDataset_test
% Unit tests for GridDataset
    
    initTestSuite;     
        
function testCreation

    D = LabeledDataset;
    D.setX([5, 2, 3
            5, 2, 4
            -1, 3, 3
            -1, 3, 3
            -1, 3, 4]);
        
    D.setY([11; 12; 13; 14; 15]);

    % First, a few test with majority votes, i.e. where multiple
    % conflicting grid observations are replaced by the most freuqently
    % occuring observation
    [ GDS, rowGridIndices ] = D.toGrid(true);
    
    assertElementsAlmostEqual(GDS.X(), ...
          [ -1     2     3
            -1     2     4
            -1     3     3
            -1     3     4
             5     2     3
             5     2     4
             5     3     3
             5     3     4 ], 'relative', 0.0, 1e-10);
         
    assertElementsAlmostEqual(rowGridIndices, [5; 6; 3; 3; 4], 'relative', 0.0, 1e-10);
        
    assertTrue(GDS.NX() == 8, 'Full grid must have 2 * 2 * 2 entries.');
    
    assertElementsAlmostEqual(GDS.Y().indices, [ 3; 4; 5; 6 ], 'relative', 0.0, 1e-10);
    assertElementsAlmostEqual(GDS.Y().values, [ 13; 15; 11; 12 ], 'relative', 0.0, 1e-10);
            
    try
        GDS.setY(1:9);
        assertFalse('setY must ensure the number of given Y values is right.');
    catch
        % Okay
    end
     
    try
        GDS.setY([5;6], [1;2]);
        assertFalse('setY must ensure the number of given Y values is right.');
    catch
        % Okay
    end
     
    GDS.setY((1:8)');
    assertElementsAlmostEqual(GDS.Y().values, (1:8)', 'relative', 0.0, 1e-10); 
    assertElementsAlmostEqual(GDS.Y().indices, (1:8)', 'relative', 0.0, 1e-10); 
     
    GDS.setY([98;99], [1;8]);
    assertElementsAlmostEqual(GDS.Y().values, [98; 99], 'relative', 0.0, 1e-10); 
    assertElementsAlmostEqual(GDS.Y().indices, [1; 8], 'relative', 0.0, 1e-10); 
            
    % Then a few experiments where it is okay to have conflicting
    % observations at one grid location
    [ GDS, rowGridIndices ] = D.toGrid(false);
    
    assertElementsAlmostEqual(GDS.X(), ...
          [ -1     2     3
            -1     2     4
            -1     3     3
            -1     3     4
             5     2     3
             5     2     4
             5     3     3
             5     3     4 ], 'relative', 0.0, 1e-10);
         
    assertElementsAlmostEqual(rowGridIndices, [5; 6; 3; 3; 4], 'relative', 0.0, 1e-10);        
    assertTrue(GDS.NX() == 8, 'Full grid must have 2 * 2 * 2 entries.');
    
    assertElementsAlmostEqual(GDS.Y().values, [11; 12; 13; 14; 15], 'relative', 0.0, 1e-10);
    assertElementsAlmostEqual(GDS.Y().indices, [5; 6; 3; 3; 4], 'relative', 0.0, 1e-10);

     