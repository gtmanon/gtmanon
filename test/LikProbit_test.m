function test_suite = LikProbit_test
% Unit tests for lik_probit_idnoise
    
    initTestSuite;     
        
function fixture = setup(fixture)

    fixture.DUMMY = [];
    

function testSynthetic(fixture)

    Ndraw = 100;

    axes = { [1; 2; 3], [4; 5], [6; 7]};
    Ngrid = 12;
    
    
    lik = lik_probit();
    likid = lik_probit_idnoise();
    likw = lik_probit_weighted();
    
    GDStoy = GridDataset();    
    labels = [ ones(8,1); 2 * ones(4,1) ];
    GDStoy.createGrid(axes, labels);
    Ytrain = GDStoy.YBinary([], true);
            
    for i = 1:Ndraw
        f = randn(Ngrid,1);

        ll = lik.fh.ll(lik, Ytrain.values, f(Ytrain.indices));
        llg = lik.fh.llg(lik, Ytrain.values, f(Ytrain.indices), 'latent');
        llg2 = lik.fh.llg2(lik, Ytrain.values, f(Ytrain.indices), 'latent');
        llg3 = lik.fh.llg3(lik, Ytrain.values, f(Ytrain.indices), 'latent');           
        
        llid = likid.fh.ll(likid, Ytrain, f);
        llgid = likid.fh.llg(likid, Ytrain, f, 'latent');
        llg2id = likid.fh.llg2(likid, Ytrain, f, 'latent');
        llg3id = likid.fh.llg3(likid, Ytrain, f, 'latent');
        
        llw = likw.fh.ll(likw, Ytrain, f);
        llgw = likw.fh.llg(likw, Ytrain, f, 'latent');
        llg2w = likw.fh.llg2(likw, Ytrain, f, 'latent');
        llg3w = likw.fh.llg3(likw, Ytrain, f, 'latent');
        
        assertElementsAlmostEqual(ll, llid);
        assertElementsAlmostEqual(llg, llgid);
        assertElementsAlmostEqual(llg2, llg2id);
        assertElementsAlmostEqual(llg3, llg3id);
        
        assertElementsAlmostEqual(ll, llw);
        assertElementsAlmostEqual(llg, llgw);
        assertElementsAlmostEqual(llg2, llg2w);
        assertElementsAlmostEqual(llg3, llg3w);
        
        % Optimized version that gets ll - llg2 together
        [ llidext, llgidext, llg2idext ] = likid.fh.llext(likid, Ytrain, f);
        [ likw, llwext, llgwext, llg2wext ] = likw.fh.llext(likw, Ytrain, f);
        
        assertElementsAlmostEqual(ll, llidext);
        assertElementsAlmostEqual(llg, llgidext);
        assertElementsAlmostEqual(llg2, llg2idext);
        
        assertElementsAlmostEqual(ll, llwext);
        assertElementsAlmostEqual(llg, llgwext);
        assertElementsAlmostEqual(full(diag(llg2)), full(llg2wext));
    end
         
    GDStoy = GridDataset();
    labels = [ ones(8,1); 2 * ones(2,1); 0; 0; 1 ];
    GDStoy.createGrid(axes, labels, ([7:-1:1 8 8 8 8 11 12])');
    Ytrain = GDStoy.YBinary([], true);
    
    Ytpos = (Ytrain.values ~= 0);
    
    for i = 1:Ndraw
        f = randn(Ngrid,1);

        ll = lik.fh.ll(lik, Ytrain.values(Ytpos), f(Ytrain.indices(Ytpos)));
        llg = lik.fh.llg(lik, Ytrain.values(Ytpos), f(Ytrain.indices(Ytpos)), 'latent');
        llg2 = lik.fh.llg2(lik, Ytrain.values(Ytpos), f(Ytrain.indices(Ytpos)), 'latent');
        llg3 = lik.fh.llg3(lik, Ytrain.values(Ytpos), f(Ytrain.indices(Ytpos)), 'latent');           
        
        llid = likid.fh.ll(likid, Ytrain, f);
        llgid = likid.fh.llg(likid, Ytrain, f, 'latent');
        llg2id = likid.fh.llg2(likid, Ytrain, f, 'latent');
        llg3id = likid.fh.llg3(likid, Ytrain, f, 'latent');
        
        llw = likw.fh.ll(likw, Ytrain, f);
        llgw = likw.fh.llg(likw, Ytrain, f, 'latent');
        llg2w = likw.fh.llg2(likw, Ytrain, f, 'latent');
        llg3w = likw.fh.llg3(likw, Ytrain, f, 'latent');

        %These two are not equal because in the grid case, the unobserved
        %locations are taken into account
        % assertElementsAlmostEqual(ll, llid);
        assertElementsAlmostEqual(sparse(Ytrain.indices(Ytpos), 1, llg, Ngrid, 1), llgid);
        assertElementsAlmostEqual(sparse(Ytrain.indices(Ytpos), 1, llg2, Ngrid, 1), llg2id);
        assertElementsAlmostEqual(sparse(Ytrain.indices(Ytpos), 1, llg3, Ngrid, 1), llg3id);
        
        assertElementsAlmostEqual(ll, llw);
        assertElementsAlmostEqual(sparse(Ytrain.indices(Ytpos), 1, llg, Ngrid, 1), llgw);
        assertElementsAlmostEqual(sparse(Ytrain.indices(Ytpos), 1, llg2, Ngrid, 1), llg2w);
        assertElementsAlmostEqual(sparse(Ytrain.indices(Ytpos), 1, llg3, Ngrid, 1), llg3w);
    end

    
function testrealData(fixture)
    
    BCD = BinaryChoiceDataset;
    BCD.load('data/TARIFFS.he5', 'reduced2013');  
    % Select the user with the first index only
    BCDred = BCD.selectUsers(1:1); 

    CLD = BCDred.toClassificationDataset();
    CLD.dropColumns([1,2,3,4,9]);
   
    CLDdisc = CLD;
    CLDdisc.discretize([ repmat(4, 1, 5)]);
    CLDdisc.normalize();

    [ GDS, rowGridIndices ] = CLDdisc.toGrid(true, []);
        
    lik = lik_probit_idnoise();
  
    fVal = [ 1.312967231195720
            -0.994239636287340
             0.813029193408642
            -0.542548799210199
             1.760572519167961
             0.993962766234403
            -0.917689898893957
             0.902231386957658
            -0.917354413126053
             1.214115226568158 ];
    
    % Majority vote
    y = GDS.YBinary([], true);
    f = sparse(rowGridIndices, 1, fVal, GDS.NX(), 1);
    
    % llGrid = lik.fh.ll(lik, y, f);
    llgGrid = lik.fh.llg(lik, y, f, 'latent');    
    llg2Grid = lik.fh.llg2(lik, y, f, 'latent');
    llg3Grid = lik.fh.llg3(lik, y, f, 'latent');
        
    y = double(CLDdisc.YBinary());
    f = fVal;
    
    llClass = lik.fh.ll(lik, y, f);
    llgClass = lik.fh.llg(lik, y, f, 'latent');
    llg2Class = lik.fh.llg2(lik, y, f, 'latent');
    llg3Class = lik.fh.llg3(lik, y, f, 'latent');
    
    %These two are not equal because in the grid case, the unobserved
    %locations are taken into account
    %assertElementsAlmostEqual(llGrid, llClass, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llgGrid(rowGridIndices), llgClass, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llg2Grid(rowGridIndices), llg2Class, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llg3Grid(rowGridIndices), llg3Class, 'relative', 0.0, 1e-8);
    
    % Keep overlapping observation
    y = GDS.YBinary([], false);
    f = sparse(rowGridIndices, 1, fVal, GDS.NX(), 1);
    
    % llGrid = lik.fh.ll(lik, y, f);
    llgGrid = lik.fh.llg(lik, y, f, 'latent');    
    llg2Grid = lik.fh.llg2(lik, y, f, 'latent');
    llg3Grid = lik.fh.llg3(lik, y, f, 'latent');
    
    %These two are not equal because in the grid case, the unobserved
    %locations are taken into account
    %assertElementsAlmostEqual(llGrid, llClass, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llgGrid(rowGridIndices), llgClass, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llg2Grid(rowGridIndices), llg2Class, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llg3Grid(rowGridIndices), llg3Class, 'relative', 0.0, 1e-8);
    
    % Double observations -> logs should double
    y.values = [ y.values; y.values];
    y.indices = [ y.indices; y.indices];
    
    llGrid = lik.fh.ll(lik, y, f);
    llgGrid = lik.fh.llg(lik, y, f, 'latent');    
    llg2Grid = lik.fh.llg2(lik, y, f, 'latent');
    llg3Grid = lik.fh.llg3(lik, y, f, 'latent');
    
    %These two are not equal because in the grid case, the unobserved
    %locations are taken into account
    %assertElementsAlmostEqual(llGrid, 2*llClass, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llgGrid(rowGridIndices), 2*llgClass, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llg2Grid(rowGridIndices), 2*llg2Class, 'relative', 0.0, 1e-8);
    assertElementsAlmostEqual(llg3Grid(rowGridIndices), 2*llg3Class, 'relative', 0.0, 1e-8);
   
    
function testrealDataOverlap(fixture)  
    
    Nruns = 10;
    
    BCD = BinaryChoiceDataset;
    BCD.load('data/CGPPREF.he5', 'sushi');  
    BCD.dropColumns([1:6 8 10 11 13 15]);

    CLD = BCD.toClassificationDataset([0 0 1 1], false);
    [ GDS, rowGridIndices ] = CLD.toGrid();
                
    for i = 1:Nruns
    
        lik = lik_probit_idnoise();
        likclass = lik_probit();
        
        f = rand(GDS.NX(), 1);      
        
        fClass = f(rowGridIndices);
        yClass = CLD.YBinary();
                    
        % llClass = likclass.fh.ll(likclass, yClass, fClass);
        llgClass = likclass.fh.llg(likclass, yClass, fClass, 'latent');
        llg2Class = likclass.fh.llg2(likclass, yClass, fClass, 'latent');
        llg3Class = likclass.fh.llg3(likclass, yClass, fClass, 'latent');
   
        % Keep overlapping observation
        y = GDS.YBinary([], false);
    
        % llGrid = lik.fh.ll(lik, y, f);
        llgGrid = lik.fh.llg(lik, y, f, 'latent');    
        llg2Grid = lik.fh.llg2(lik, y, f, 'latent');
        llg3Grid = lik.fh.llg3(lik, y, f, 'latent');
                    
        %These two are not equal because in the grid case, the unobserved
        %locations are taken into account
        % assertElementsAlmostEqual(llGrid, llClass, 'relative', 0.0, 1e-8);
        assertElementsAlmostEqual(llgGrid, accumarray(rowGridIndices, llgClass, size(llgGrid)), 'relative', 0.0, 1e-8);
        assertElementsAlmostEqual(llg2Grid, accumarray(rowGridIndices, llg2Class, size(llgGrid)), 'relative', 0.0, 1e-8);
        assertElementsAlmostEqual(llg3Grid, accumarray(rowGridIndices, llg3Class, size(llgGrid)), 'relative', 0.0, 1e-8);
    
        % Double observations -> logs should double
        y.values = [ y.values; y.values];
        y.indices = [ y.indices; y.indices];
        y.tasks = [ y.tasks; y.tasks];

        % llGrid = lik.fh.ll(lik, y, f);
        llgGrid = lik.fh.llg(lik, y, f, 'latent');    
        llg2Grid = lik.fh.llg2(lik, y, f, 'latent');
        llg3Grid = lik.fh.llg3(lik, y, f, 'latent');

        %These two are not equal because in the grid case, the unobserved
        %locations are taken into account
        % assertElementsAlmostEqual(llGrid, 2*llClass, 'relative', 0.0, 1e-8);
        assertElementsAlmostEqual(llgGrid, 2*accumarray(rowGridIndices, llgClass, size(llgGrid)), 'relative', 0.0, 1e-8);
        assertElementsAlmostEqual(llg2Grid, 2*accumarray(rowGridIndices, llg2Class, size(llgGrid)), 'relative', 0.0, 1e-8);
        assertElementsAlmostEqual(llg3Grid, 2*accumarray(rowGridIndices, llg3Class, size(llgGrid)), 'relative', 0.0, 1e-8);
            
    end
    
    
function testMixture(fixture)    

    % A mixture with a single, one-weighted component should give same result
    % as the plain likelihood    
    lik = lik_probit_idnoise();
    
    likw = lik_probit_weighted();
    Gamma = ones(10, 1);
    likw.Gamma = Gamma;
    likw.P = 1;
    
    f = (1:0.1:1.9)';
    y.values = (mod(1:10, 2) * 2 - 1)';
    y.indices = (1:10)';
    y.tasks = randi(3,10,1);
        
    ll = lik.fh.ll(lik, y, f);
    llg = lik.fh.llg(lik, y, f, 'latent');
    llg2 = lik.fh.llg2(lik, y, f, 'latent');
    llg3 = lik.fh.llg3(lik, y, f, 'latent');           

    llmix = likw.fh.ll(likw, y, f);
    llgmix = likw.fh.llg(likw, y, f, 'latent');
    llg2mix = likw.fh.llg2(likw, y, f, 'latent');
    llg3mix = likw.fh.llg3(likw, y, f, 'latent');

    assertElementsAlmostEqual(ll, llmix);
    assertElementsAlmostEqual(llg, llgmix);
    assertElementsAlmostEqual(llg2, llg2mix);
    assertElementsAlmostEqual(llg3, llg3mix);
    
    
 function testMixtureGradients(fixture)
      
    likw = lik_probit_weighted();

    DF = 1E-3;
    TOL = 1E-6;
    
    for run = 1:10
        Gamma = abs(rand(3, 2));

        likw.Gamma = Gamma;
        likw.P = 2;
        
        f = [ (1:0.1:1.9)' (2:0.1:2.9)' ];
        y.values = (mod(1:10, 2) * 2 - 1)';
        y.indices = (1:10)';
        y.tasks = randi(3,10,1);

        for i = 1:numel(f)
            ll = likw.fh.ll(likw, y, f);
            llg = likw.fh.llg(likw, y, f, 'latent');

            fplus = f; fplus(i) = fplus(i) + DF;
            llplus = likw.fh.ll(likw, y, fplus);
            llgradplus = ll + DF * llg(i);
            assertElementsAlmostEqual(llplus, llgradplus, 'relative', 0.0, TOL);

            fminus = f; fminus(i) = fminus(i) - DF;
            llminus = likw.fh.ll(likw, y, fminus);
            llgradminus = ll - DF * llg(i);
            assertElementsAlmostEqual(llminus, llgradminus, 'relative', 0.0, TOL);
        end

        for i = 1:size(f,1)
            for t1 = 1:2
                for t2 = 1:2
                    llg = likw.fh.llg(likw, y, f, 'latent');
                    llg2 = likw.fh.llg2(likw, y, f, 'latent');

                    fplus = f; fplus(i,t2) = fplus(i,t2) + DF;
                    llgplus = likw.fh.llg(likw, y, fplus, 'latent');
                    llggradplus = llg(i,t1) + DF * llg2(i,t1 + (t2-1)*2);               
                    assertElementsAlmostEqual(llgplus(i,t1), llggradplus, 'relative', 0.0, TOL);

                    fminus = f; fminus(i,t2) = fminus(i,t2) - DF;
                    llgminus = likw.fh.llg(likw, y, fminus, 'latent');
                    llggradminus = llg(i,t1) - DF * llg2(i,t1 + (t2-1)*2);
                    assertElementsAlmostEqual(llgminus(i,t1), llggradminus, 'relative', 0.0, TOL);   
                end
            end
        end    
    end
    
    
function testMixtureGradientsCombined(fixture)
    % Same as testMixtureGradients but with the combined ll/llg/llg2 call
      
    likw = lik_probit_weighted();
    
    DF = 1E-3;
    TOL = 1E-6;
    
    for run = 1:10
        Gamma = abs(rand(3, 3));

        likw.Gamma = Gamma;
        likw.P = 3;
        
        f = [ (-1.9:0.1:-1.0)' (1:0.1:1.9)' (2:0.1:2.9)' ];
        y.values = (mod(1:10, 2) * 2 - 1)';
        y.indices = (1:10)';
        y.tasks = randi(3,10,1);

        for i = 1:numel(f)
            [likw, ll, llg, ~] = likw.fh.llext(likw, y, f);

            fplus = f; fplus(i) = fplus(i) + DF;
            llplus = likw.fh.ll(likw, y, fplus);
            llgradplus = ll + DF * llg(i);
            assertElementsAlmostEqual(llplus, llgradplus, 'relative', 0.0, TOL);

            fminus = f; fminus(i) = fminus(i) - DF;
            llminus = likw.fh.ll(likw, y, fminus);
            llgradminus = ll - DF * llg(i);
            assertElementsAlmostEqual(llminus, llgradminus, 'relative', 0.0, TOL);
        end

        for i = 1:size(f,1)
            for t1 = 1:3
                for t2 = 1:3                    
                    [likw, ~, llg, llg2mat, llg2] = likw.fh.llext(likw, y, f);
                    
                    fplus = f; fplus(i,t2) = fplus(i,t2) + DF;
                    llgplus = likw.fh.llg(likw, y, fplus, 'latent');
                    llggradplus = llg(i,t1) + DF * llg2(i,t1 + (t2-1)*3);               
                    assertElementsAlmostEqual(llgplus(i,t1), llggradplus, 'relative', 0.0, TOL);

                    fminus = f; fminus(i,t2) = fminus(i,t2) - DF;
                    llgminus = likw.fh.llg(likw, y, fminus, 'latent');
                    llggradminus = llg(i,t1) - DF * llg2(i,t1 + (t2-1)*3);
                    assertElementsAlmostEqual(llgminus(i,t1), llggradminus, 'relative', 0.0, TOL);   
                end
            end
        end    
        
        % Check full sparse matrix representation of llg2        
        [likw, ~, ~, llg2mat, llg2] = likw.fh.llext(likw, y, f);
        
        nf = size(f,1);
        
        for rowp = 1:3
            for colp = 1:3                           
                assertElementsAlmostEqual(llg2(:,(colp-1)*3 + rowp), ...
                    diag(llg2mat(1+(rowp-1)*nf:rowp*nf,1+(colp-1)*nf:colp*nf)), 'relative', 0.0, TOL);
            end
        end
        
        
        assertElementsAlmostEqual(norm(llg2mat - llg2mat'), 0, 'relative', 0.0, TOL);
        
    end
    
function testIDNoise(fixture)
    
    TOL = 1E-7;
    
    % An input-dependent noise model where all noise variances are 1 
    % should give same result as the plain probit likelihood    
    lik = lik_probit();    
    likid = lik_probit_idnoise();
    
    VarT = ones(3, 1);
    likid.VarT = VarT;
    
    f = (-1:0.2:0.9)';
    y.values = (mod(1:10, 2) * 2 - 1)';
    y.indices = (1:10)';
    y.tasks = randi(3,10,1);
        
    ll = lik.fh.ll(lik, y.values, f);
    llg = lik.fh.llg(lik, y.values, f, 'latent');
    llg2 = lik.fh.llg2(lik, y.values, f, 'latent');
    llg3 = lik.fh.llg3(lik, y.values, f, 'latent');           

    llid = likid.fh.ll(likid, y, f);
    llgid = likid.fh.llg(likid, y, f, 'latent');
    llg2id = likid.fh.llg2(likid, y, f, 'latent');
    llg3id = likid.fh.llg3(likid, y, f, 'latent');

    assertElementsAlmostEqual(ll, llid, 'absolute', TOL);
    assertElementsAlmostEqual(llg, llgid, 'absolute', TOL);
    assertElementsAlmostEqual(llg2, llg2id, 'absolute', TOL);
    assertElementsAlmostEqual(llg3, llg3id, 'absolute', TOL);   
    
    
function testIDGradients(fixture)      
    % Finite differences gradients check for input-dependent noise probit
    likid = lik_probit_idnoise();

    DF = 1E-4;
    TOL = 1E-6;
    
    for run = 1:10
        VarT = 2 * abs(rand(3, 1)) + 0.1;
        likid.VarT = VarT;
        
        f = (-1:0.2:0.9)';
        y.values = (mod(1:10, 2) * 2 - 1)';
        y.indices = (1:10)';
        y.tasks = randi(3,10,1);

        for i = 1:numel(f)
            ll = likid.fh.ll(likid, y, f);
            llg = likid.fh.llg(likid, y, f, 'latent');

            fplus = f; fplus(i) = fplus(i) + DF;
            llplus = likid.fh.ll(likid, y, fplus);
            llgradplus = ll + DF * llg(i);
            assertElementsAlmostEqual(llplus, llgradplus, 'absolute', TOL);

            fminus = f; fminus(i) = fminus(i) - DF;
            llminus = likid.fh.ll(likid, y, fminus);
            llgradminus = ll - DF * llg(i);
            assertElementsAlmostEqual(llminus, llgradminus, 'absolute', TOL);
        end

        for i = 1:numel(f)
            llg = likid.fh.llg(likid, y, f, 'latent');
            llg2 = likid.fh.llg2(likid, y, f, 'latent');

            fplus = f; fplus(i) = fplus(i) + DF;
            llgplus = likid.fh.llg(likid, y, fplus, 'latent');
            llggradplus = llg(i) + DF * llg2(i);               
            assertElementsAlmostEqual(llgplus(i), llggradplus, 'absolute', TOL);

            fminus = f; fminus(i) = fminus(i) - DF;
            llgminus = likid.fh.llg(likid, y, fminus, 'latent');
            llggradminus = llg(i) - DF * llg2(i);
            assertElementsAlmostEqual(llgminus(i), llggradminus, 'absolute', TOL);   
        end    
    end
    
    
function testIDGradientsCombined(fixture)
    % Same as testMixtureGradients but with the combined ll/llg/llg2 call
      
    likid = lik_probit_idnoise();
    
    DF = 1E-4;
    TOL = 1E-6;
    
    for run = 1:10
        VarT = 2 * abs(rand(3, 1)) + 0.1;
        likid.VarT = VarT;
        
        f = (-1:0.2:0.9)';
        y.values = (mod(1:10, 2) * 2 - 1)';
        y.indices = (1:10)';
        y.tasks = randi(3,10,1);

        for i = 1:numel(f)
            [ll, llg, ~] = likid.fh.llext(likid, y, f);

            fplus = f; fplus(i) = fplus(i) + DF;
            llplus = likid.fh.ll(likid, y, fplus);
            llgradplus = ll + DF * llg(i);
            assertElementsAlmostEqual(llplus, llgradplus, 'absolute', TOL);   

            fminus = f; fminus(i) = fminus(i) - DF;
            llminus = likid.fh.ll(likid, y, fminus);
            llgradminus = ll - DF * llg(i);
            assertElementsAlmostEqual(llminus, llgradminus, 'absolute', TOL);   
        end

        for i = 1:numel(f)
            [~, llg, llg2] = likid.fh.llext(likid, y, f);
            
            fplus = f; fplus(i) = fplus(i) + DF;
            llgplus = likid.fh.llg(likid, y, fplus, 'latent');
            llggradplus = llg(i) + DF * llg2(i);               
            assertElementsAlmostEqual(llgplus(i), llggradplus, 'absolute', TOL);

            fminus = f; fminus(i) = fminus(i) - DF;
            llgminus = likid.fh.llg(likid, y, fminus, 'latent');
            llggradminus = llg(i) - DF * llg2(i);
            assertElementsAlmostEqual(llgminus(i), llggradminus, 'absolute', TOL);   
        end  
        
    end