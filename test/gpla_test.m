function test_suite = gpla_test
% Unit tests for Kronecker probit classification
    
    initTestSuite;     
        
function fixture = setup(fixture)    

    fixture.BCD = BinaryChoiceDataset;
    fixture.BCD.load('data/CGPPREF.he5', 'sushi');  
    fixture.BCD = fixture.BCD.selectUsers(1:5);
    fixture.BCD.dropColumns([1:6 8 10 11 13 15]);
    
    fixture.CLD = fixture.BCD.toClassificationDataset();
    distVals = [ -1 -1 6 6 ];
    fixture.CLD.discretize(distVals, 'equalcount');

    [~, idx] = sort(distVals, 'descend');
    fixture.CLD.switchColumns(idx);
    fixture.CLD.normalize(); 
    
    [ fixture.GDS, fixture.rowGridIndices ] = fixture.CLD.toGrid(false, []);
    
    fixture.JITTER = 1E-9;
    
    fixture.OPT = optimset('TolFun', 1e-5, 'TolX', 1e-5, ...
                           'DerivativeCheck', 'off', 'Display', 'iter');
    fixture.OPT.lambdalim = 1E6;
    
    
    fixture.LS = 2 * ones(1, fixture.CLD.DX());
    fixture.PLS = prior_logunif();
    
    fixture.SIGMA = 0.5;    
    fixture.PSIGMA = prior_sqrtunif();
    
    % Debug mode for the Laplace inference
    fixture.DEBUG = false;
    
    
function testToyexampleFull(fixture)

    axes = { [1; 2; 3], [4; 5] };
    
    GDStoy = GridDataset();
    
    labels = [ ones(5,1); 2 ];
    GDStoy.createGrid(axes, labels);
    
    LStoy = 2 * ones(1, numel(axes));
    
    %%% Gridded computation %%%
    
    gpcf_kron = gpcf_kronsexp('lengthScale', LStoy, ...
                              'magnSigma2', fixture.SIGMA, ...
                              'lengthScale_prior', fixture.PLS, ...
                              'magnSigma2_prior', fixture.PSIGMA);
                          
    K = KroneckerSEXPCov(axes);
    K.updateParameters([log(fixture.SIGMA) log(LStoy)]);
    
    Ytrain = GDStoy.YBinary([], true);

    gp_kron = gp_set('lik', lik_probit_idnoise(), ...
                'cf', gpcf_kron, ...
                'jitterSigma2', fixture.JITTER, ...
                'KroneckerCov', K, ...
                'EXMAX', 200, ...
                'DEBUG', fixture.DEBUG, ...
                'FORCEAPPROX', true, ...
                'EIGENLIMIT', 1E-8, ...
                'EIGENCOUNTLIMIT', 999, ...
                'EIGENSAFE', 1E-8, ...
                'CGACCURACY', 1E-8);
            
    gp_kron = gp_optim(gp_kron, axes, { Ytrain }, 'opt', fixture.OPT);

    [ E_kron, Var_kron ] = gp_pred(gp_kron, axes, ...
                                   { Ytrain }, axes);

    %%% Regular computation %%%
    
    gpcf_reg = gpcf_sexp('lengthScale', LStoy, ...
                         'magnSigma2', fixture.SIGMA, ...
                         'lengthScale_prior', fixture.PLS, ...
                         'magnSigma2_prior', fixture.PSIGMA);

    gp = gp_set('lik', lik_probit(), ...
        'cf', gpcf_reg, ...
        'jitterSigma2', fixture.JITTER, ...
        'latent_method', 'Laplace');
    
    gp = gp_optim(gp, GDStoy.X(), Ytrain.values, 'opt', fixture.OPT);

    [ E_reg, Var_reg ] = gp_pred(gp, GDStoy.X(), Ytrain.values);

    assertElementsAlmostEqual(E_kron, E_reg, 'relative', 1e-2);
    assertElementsAlmostEqual(Var_kron, Var_reg, 'relative', 1e-2);    
   
    
function testToyexampleExt(fixture)
    % This expands over Toy example full by also having grid locations with
    % no and with multiple observations, and shuffled observation indices

    axes = { [1; 2; 3], [4; 5], [6; 7]};
    
    GDStoy = GridDataset();
    
    labels = [ ones(8,1); 2 * ones(2,1); 0 ];
    GDStoy.createGrid(axes, labels, ([7:-1:1 8 8 8 11])');
    
    LStoy = 2 * ones(1, numel(axes));
    
    %%% Gridded computation %%%
    
    gpcf_kron = gpcf_kronsexp('lengthScale', LStoy, ...
                              'magnSigma2', fixture.SIGMA, ...
                              'lengthScale_prior', fixture.PLS, ...
                              'magnSigma2_prior', fixture.PSIGMA);
                          
    K = KroneckerSEXPCov(axes);
    Ytrain = GDStoy.YBinary([], true);

    gp_kron = gp_set('lik', lik_probit_idnoise(), ...
                'cf', gpcf_kron, ...
                'jitterSigma2', fixture.JITTER, ...
                'KroneckerCov', K, ...  
                'DEBUG', fixture.DEBUG, ...
                'EXMAX', 200, ...
                'FORCEAPPROX', true, ...
                'EIGENLIMIT', 1E-8, ...
                'EIGENCOUNTLIMIT', 999, ...
                'EIGENSAFE', 1E-8, ...
                'CGACCURACY', 1E-8);
            
    gp_kron = gp_optim(gp_kron, axes, { Ytrain }, 'opt', fixture.OPT);

    [ E_kron, Var_kron ] = gp_pred(gp_kron, axes, ...
                                   { Ytrain }, axes);

    %%% Regular computation %%%
    
    gpcf_reg = gpcf_sexp('lengthScale', LStoy, ...
                         'magnSigma2', fixture.SIGMA, ...
                         'lengthScale_prior', fixture.PLS, ...
                         'magnSigma2_prior', fixture.PSIGMA);

    gp = gp_set('lik', lik_probit(), ...
        'cf', gpcf_reg, ...
        'jitterSigma2', fixture.JITTER, ...
        'latent_method', 'Laplace');
    
    X = GDStoy.X();    
    Ypos = (Ytrain.values ~= 0);
    
    
    gp = gp_optim(gp, X(Ytrain.indices(Ypos),:), Ytrain.values(Ypos), 'opt', fixture.OPT);
    [ E_reg, Var_reg ] = gp_pred(gp, X(Ytrain.indices(Ypos),:), Ytrain.values(Ypos));

    assertElementsAlmostEqual(E_kron(Ytrain.indices(Ypos)), E_reg, 'relative', 1e-2);
    assertElementsAlmostEqual(Var_kron(Ypos), Var_reg, 'relative', 1e-2);    
    

    
function testRealdata(fixture)
   
    %%% Gridded computation %%%
    
    gpcf_kron = gpcf_kronsexp('lengthScale', fixture.LS, ...
                              'magnSigma2', fixture.SIGMA, ...
                              'lengthScale_prior', fixture.PLS, ...
                              'magnSigma2_prior', fixture.PSIGMA);
                          
    K = KroneckerSEXPCov(fixture.GDS.getAxes()');
    Ytrain = fixture.GDS.YBinary([], true);

    gp_kron = gp_set('lik', lik_probit_idnoise(), ...
                'cf', gpcf_kron, ...
                'jitterSigma2', fixture.JITTER, ...
                'KroneckerCov', K, ...                
                'EXMAX', 200, ...
                'DEBUG', fixture.DEBUG, ...
                'FORCEAPPROX', true, ...
                'EIGENLIMIT', 0.01, ...
                'EIGENCOUNTLIMIT', 999, ...
                'EIGENSAFE', 0.01, ...
                'CGACCURACY', 1E-6);
            
    gp_kron = gp_optim(gp_kron, fixture.GDS.getAxes()', { Ytrain }, 'opt', fixture.OPT);

    [ E_kron, Var_kron ] = gp_pred(gp_kron, fixture.GDS.getAxes()', ...
                                   { Ytrain }, fixture.GDS.getAxes()');

    %%% Regular computation %%%
    
    gpcf_reg = gpcf_sexp('lengthScale', fixture.LS, ...
                         'magnSigma2', fixture.SIGMA, ...
                         'lengthScale_prior', fixture.PLS, ...
                         'magnSigma2_prior', fixture.PSIGMA);

    gp = gp_set('lik', lik_probit(), ...
        'cf', gpcf_reg, ...
        'jitterSigma2', fixture.JITTER, ...
        'latent_method', 'Laplace');
    
    gp = gp_optim(gp, fixture.CLD.X(), fixture.CLD().YBinary(), 'opt', fixture.OPT);

    [ E_reg, Var_reg ] = gp_pred(gp, fixture.CLD.X(), fixture.CLD().YBinary());

    assertElementsAlmostEqual(E_kron(Ytrain.indices), E_reg, 'relative', 0.0, 1e-2);
    assertElementsAlmostEqual(Var_kron, Var_reg, 'relative', 0.0, 1e-2);
    
    
function testToyexampleNoiseOut(fixture)

    % See if high noise levels give the same results as not including a
    % particular user's data at all.

    axes = { [1; 2; 3], [4; 5] };
    
    GDStoyfull = GridDataset();    
    labels = [ 1; 1; 1; 2; 2; 2 ];
    indices = (1:6)';
    users = [1; 1; 2; 1; 1; 2];
    GDStoyfull.createGrid(axes, labels, indices);
    GDStoyfull.setTasks(users);
    
    % Same dataset, but only the data of the first user
    GDStoyone = GridDataset();    
    labels = [ 1; 1; 2; 2 ];
    indices = [1; 2; 4; 5];
    users = [ 1; 1; 1; 1 ];
    GDStoyone.createGrid(axes, labels, indices);
    GDStoyone.setTasks(users);   
    
    LStoy = 1 * ones(1, numel(axes));
    
    gpcf_kron = gpcf_kronsexp('lengthScale', LStoy, ...
                              'magnSigma2', fixture.SIGMA, ...
                              'lengthScale_prior', fixture.PLS, ...
                              'magnSigma2_prior', fixture.PSIGMA);
                          
    K = KroneckerSEXPCov(axes);
    K.updateParameters([log(fixture.SIGMA) log(LStoy)]);
    
    Ytrainfull = GDStoyfull.YBinary([], true);

    gp_full = gp_set('lik', lik_probit_idnoise(), ...
                'cf', gpcf_kron, ...
                'jitterSigma2', fixture.JITTER, ...
                'KroneckerCov', K, ...
                'EXMAX', 200, ...
                'DEBUG', fixture.DEBUG, ...
                'FORCEAPPROX', true, ...
                'EIGENLIMIT', 1E-6, ...
                'EIGENCOUNTLIMIT', 999, ...
                'EIGENSAFE', 1E-6, ...
                'CGACCURACY', 1E-6, ...
                'SIGMAT', [1; 1E8]); % Blank out second user
            
%    gp_full = gp_optim(gp_full, axes, { Ytrainfull }, 'opt', fixture.OPT);

    [ E_full, Var_full ] = gp_pred(gp_full, axes, ...
                                   { Ytrainfull }, axes);


    K = KroneckerSEXPCov(axes);
    K.updateParameters([log(fixture.SIGMA) log(LStoy)]);
    
    Ytrainone = GDStoyone.YBinary([], true);

    gp_one = gp_set('lik', lik_probit_idnoise(), ...
                'cf', gpcf_kron, ...
                'jitterSigma2', fixture.JITTER, ...
                'KroneckerCov', K, ...
                'EXMAX', 200, ...
                'DEBUG', fixture.DEBUG, ...
                'FORCEAPPROX', false, ...
                'EIGENLIMIT', 1E-6, ...
                'EIGENCOUNTLIMIT', 999, ...
                'EIGENSAFE', 1E-6, ...
                'CGACCURACY', 1E-6);
            
%    gp_one = gp_optim(gp_one, axes, { Ytrainone }, 'opt', fixture.OPT);

    [ E_one, Var_one ] = gp_pred(gp_one, axes, ...
                                   { Ytrainone }, axes);
                               
                               
    assertElementsAlmostEqual(E_full, E_one, 'relative', 0.0, 1e-2);
    assertElementsAlmostEqual(Var_full(Ytrainone.indices), Var_one, 'relative', 0.0, 1e-2);    
       