function [ active, Nactive, activefull  ] = activerows(Ain, blocks, tol)

    if(nargin < 3)
        tol = 1E-12;
    end

    NA = size(Ain,1);
    Nblock = NA / blocks;
    
    active = find(sum(reshape((sum(abs(Ain),2) > tol), [], blocks), 2) > 0);
    
    if(nargout > 1)
        Nactive = numel(active);
                              
        if(nargout > 2)
            activefull = repmat(active, 1, blocks) + repmat((0:blocks-1)*Nblock, Nactive, 1);
            activefull = activefull(:);
        end
    end

end

