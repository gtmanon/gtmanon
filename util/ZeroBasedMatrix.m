classdef ZeroBasedMatrix < handle
%ZEROBASEDMATRIX    Thin wrapper around a two-dimensional matrix that
%                   provides access methods for zero-based indexing.

    properties(Access='public')
        mat;
    end
    
    methods
        
        function initialize(obj, rows, cols)
            obj.mat = zeros(rows, cols);
        end
        
        function initializeFromMatrix(obj, initMat)
            obj.mat = initMat;
        end
        
        function val = get(obj, zeroRow, zeroCol)
            val = obj.mat(zeroRow + 1, zeroCol + 1);
        end
            
        function set(obj, zeroRow, zeroCol, val)
            obj.mat(zeroRow + 1, zeroCol + 1) = val;
        end
        
        function sz = size(obj, dim)
            sz = size(obj.mat, dim);
        end
        
        function outMat = value(obj)
            outMat = obj.mat;
        end
        
    end
    
end

