function x = conjgrad(Afun,b,initial,tol)
% CONJGRAD  Conjugate Gradient Method.
%   X = CONJGRAD(Afun,B) attemps to solve the system of linear equations 
%   Afun(X)=B for X. Afun evaluated at X must return a vector of length N,
%   and the right hand side column vector B must have length N.
%
%   X = CONJGRAD(A,B,INITIAL,TOL) specifies the initial value of X and the
%   tolerance of the method. The default initial value is B, the default
%   is 1e-10.
%
% Original by Yi Cao at Cranfield University, 18 December 2008
% Updated on 6 Feb 2014.
%
% Modified to work on functions Afun by XXX on 4/28/14.
%
    if nargin<4
        tol=1e-10;
        
        if nargin < 3
            initial = b;
        end
    end
  
    x = initial;
    r = b - Afun(x);
    if norm(r) < tol
        return
    end
    y = -r;
    z = Afun(y);
    s = y'*z;
    t = (r'*y)/s;
    x = x + t*y;
  
    for k = 1:numel(b);
       r = r - t*z;
       if( norm(r) < tol )
            return;
       end
       B = (r'*z)/s;
       y = -r + B*y;
       z = Afun(y);
       s = y'*z;
       t = (r'*y)/s;
       x = x + t*y;
    end
 end
