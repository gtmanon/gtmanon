function X = blockdiagonalsolve(A, B)
%BLOCKDIAGONALSOLVE Solve system where AX=C is lower triagular and C a block
%diagonal matrix with all B blocks on the diagonal (not necessarily
%square).
   
    Nblocks = size(A,1) / size(B,1);
    Ablsz = size(A,1) / Nblocks;
    
    X = cell(Nblocks,Nblocks);
    
    blkidx = @(b) ((b-1)*Ablsz+1):b*Ablsz;
    
    for col = Nblocks:-1:1
        for row = col:Nblocks
            
            if(row == col)
                B2 = B;           
            else
                B2 = zeros(size(B));
            end
            %B2 = zeros(size(B));
            for addRow = col:(row-1)
                B2 = B2 - A(blkidx(row),blkidx(addRow))*X{addRow,col};
            end
            
            X{row,col} = A(blkidx(row),blkidx(row)) \ B2;
        end
    end
end

