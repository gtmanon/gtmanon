function breaks = jenks(data, n_classes)
%JENKS  Compute Jenks' natural breaks given the column vector data
%
%   Returns a column vector of size (n_classes+1)x1 of natural breaks into
%   n_classes classes.
%
%   This implementation is a literal translation of the JavaScript
%   implementation at https://github.com/tmcw/simple-statistics.

    NU = numel(unique(data));

    if n_classes >= NU
        % warning('More classes (%d) than unique data items (%d). Returning original values.', n_classes, NU);
        sud = sort(unique(data));
        breaks = [ sud; sud(end) ];
        return;
    end

    if size(data, 2) ~= 1
        error('data must be a column vector');
    end

    zeroData = ZeroBasedMatrix();
    zeroData.initializeFromMatrix(sort(data, 'ascend'));

    lower_class_limits = jenksMatrices(zeroData, n_classes);
    breaks = jenksBreaks(zeroData, lower_class_limits, n_classes);
    
    % jenksBreaks operates on a ZeroBasedMatrix and similarly returns an
    % object of that type
    breaks = breaks.value();


function [lower_class_limits, variance_combinations] = jenksMatrices(data, n_classes)

    ND = data.size(1);

    variance = 0;

    lower_class_limits = ZeroBasedMatrix();
    lower_class_limits.initialize(ND + 1, n_classes + 1);

    variance_combinations = ZeroBasedMatrix();
    variance_combinations.initialize(ND + 1, n_classes + 1);

    for i=1:n_classes
        lower_class_limits.set(1,i,1);
        variance_combinations.set(1,i,0);
        for j = 2:ND
            variance_combinations.set(j,i,Inf);
        end
    end

    for l = 2:ND
        sum = 0;
        sum_squares = 0;
        w = 0;

        for m = 1:l
            lower_class_limit = l - m + 1;
            val = data.get(lower_class_limit - 1,0);

            w = w + 1;
            sum = sum + val;
            sum_squares = sum_squares + val^2;
            variance = sum_squares - (sum * sum) / w;

            i4 = lower_class_limit - 1;

            if (i4 ~= 0)
                for j = 2:n_classes
                    if (variance_combinations.get(l,j) >= (variance + variance_combinations.get(i4, j - 1)))
                        lower_class_limits.set(l,j,lower_class_limit);
                        variance_combinations.set(l,j,variance + variance_combinations.get(i4, j - 1));
                    end
                end
            end
        end

        lower_class_limits.set(l,1,1);
        variance_combinations.set(l,1,variance);
    end

function kclass = jenksBreaks(data, lower_class_limits, n_classes)

    ND = data.size(1);

    k = ND - 1;
    countNum = n_classes;

    kclass = ZeroBasedMatrix();
    kclass.initialize(n_classes+1, 1);

    kclass.set(n_classes,0,data.get(ND-1,0));
    kclass.set(0,0,data.get(0,0));

    while (countNum > 1)
        kclass.set(countNum - 1,0,data.get(lower_class_limits.get(k,countNum) - 2,0));
        k = lower_class_limits.get(k,countNum) - 1;
        countNum = countNum - 1;
    end
