function diagest = est_diag(fun, D, N)
%Estimate the diagonal of an implicit square matrix.
%
%fun is the handle to a function that, given a vector x, returns y = Ax the
%result of applying the implicit matrix to x. fun must be capable of
%vectorization, that is, given matrix X (where each column is a single x)
%as input, it must return a similar matrix Y as output.
%
%D is the size of A (i.e., the length of each x)
%
%N is the number of samples to use for estimation.
%

    % Size of the smallest Hadamard matrix of size at least D
    HN = 2^ceil(log2(D));  
    
    % Generate N column vectors from the Hadamard matrix of size HN and cut
    % them to length D.
    V = fwht(sparse(randsample(HN, N), 1:N, 1, HN, N)) * HN;
    V = V(1:D,:);     
    
    % Statistically sligthly less efficient alternative: use random -1/+1 vectors
    % V = 2 * (randi(2, D, N) - 1.5);
       
    % Scalar division by N is equivalent to component-wise division with
    % the vector sum(V.^2,2) because all squares of V are ones in a
    % Hadamard matrix
    diagest = sum(bsxfun(@times, fun(V), V), 2) / N;
    
end
    

