classdef KroneckerLaplaceHelper
    %KRONECKERLAPLACEHELPER Collection of helper methods for using KroneckerCov objects in Laplace inference
    
    properties
        
        % Reference to the underlying KroneckerCov object
        K;
        
        % Reference to the underlying Cache or CacheCollaborative object
        C;
        
    end
    
    methods
        
        function obj = KroneckerLaplaceHelper(Kin, Cin)
            
            obj.K = Kin;
            obj.C = Cin;
            
        end
        
        function x = BSolveCollaborative(obj, b, initial, tol)
            
            if(size(b,1) ~= obj.K.N*obj.C.Nproc || size(b,2) > 1)
                error('b must be a column vector of size N*NPROC');
            end
            
            if nargin<4
                tol=1e-10;                
                if nargin < 3
                    initial = b;
                end
            end
                   
            if(size(initial,1) ~= obj.K.N*obj.C.Nproc || size(initial,2) > 1)
                error('b must be a column vector of size N*NPROC');
            end
            
            Lobs = obj.C.L(obj.C.Lrows,obj.C.Lrows);
            
            x = initial;            
            x = x(obj.C.Lrows);
            b = b(obj.C.Lrows);
                      
            Ng = numel(obj.K.KsGrouped);
            Nd = zeros(Ng, 1);
            KsGFiltered = cell(Ng, 1);            
            for g = 1:numel(obj.K.KsGrouped)
                Nd(g) = numel(obj.C.ObsFilterS{g});
                KsGFiltered{g} = obj.K.KsGrouped{g}(obj.C.ObsFilterS{g},obj.C.ObsFilterS{g});
            end
            
            % Convert a vector of size Lrows X 1 to the corresponding matrix
            % of size NObsCoveredRowsS X NPROC
            tocoveredrows = @(x) reshape(sparse(obj.C.ObsRowMapping, 1, x, obj.C.NObsCoveredRows, 1), [], obj.C.Nproc);            
            toobservedrows = @(x) x(obj.C.ObsRowMapping);
            Afun = @(x) x + Lobs'*toobservedrows(obj.K.KMultGroupFiltered(tocoveredrows(Lobs*x), obj.C.ObsFilterS));
                      
            
            %%%
            % r = b - Afun(x);
            r = sparse(obj.C.ObsRowMapping, 1, Lobs*x, obj.C.NObsCoveredRows, 1);
            for g = 1:numel(obj.K.KsGrouped)
                r = reshape(r, Nd(g), obj.C.NObsCoveredRows / Nd(g));
                r = r'*KsGFiltered{g};
            end
            r = reshape(r, obj.C.Nproc, obj.C.NObsCoveredRowsS)';
            r = (x + Lobs'*r(obj.C.ObsRowMapping));
            r = b - r;
            %%%
                                    
            if norm(r) < tol
                x = sparse(obj.C.Lrows, 1, x, obj.K.N * obj.C.Nproc, 1);
                return
            end
            y = -r;
            
            %%%                    
            % z = Afun(y);                
            z = sparse(obj.C.ObsRowMapping, 1, Lobs*y, obj.C.NObsCoveredRows, 1);
            for g = 1:numel(obj.K.KsGrouped)                    
                z = reshape(z, Nd(g), obj.C.NObsCoveredRows / Nd(g));
                z = z'*KsGFiltered{g};
            end             
            z = reshape(z, obj.C.Nproc, obj.C.NObsCoveredRowsS)';
            z = (y + Lobs'*z(obj.C.ObsRowMapping));
            %%%
            
            s = y'*z;
            t = (r'*y)/s;
            x = x + t*y;
            
            for k = 1:numel(b);
                r = r - t*z;
                if( norm(r) < tol )
                    x = sparse(obj.C.Lrows, 1, x, obj.K.N * obj.C.Nproc, 1);
                    return;
                end
                B = (r'*z)/s;
                y = -r + B*y;
                
                %%%                    
                % z = Afun(y);                
                z = sparse(obj.C.ObsRowMapping, 1, Lobs*y, obj.C.NObsCoveredRows, 1);
                for g = 1:numel(obj.K.KsGrouped)                    
                    z = reshape(z, Nd(g), obj.C.NObsCoveredRows / Nd(g));
                    z = z'*KsGFiltered{g};
                end             
                z = reshape(z, obj.C.Nproc, obj.C.NObsCoveredRowsS)';
                z = (y + Lobs'*z(obj.C.ObsRowMapping));
                %%%
                
                s = y'*z;
                t = (r'*y)/s;
                x = x + t*y;
            end
            
            x = sparse(obj.C.Lrows, 1, x, obj.K.N * obj.C.Nproc, 1);            
        end
        
        
        function x = BSolve(obj, b, initial, tol)
            % bb = conjgrad(@(x) (x + sW.*gp.KroneckerCov.KMultGroup(sW.*x)), sW.*kb, bb, gp.CGACCURACY);  
          
            if nargin < 4
                tol = 1e-10;        
                if nargin < 3
                    initial = b;
                end
            end
            
            brows = size(b, 1);
            bcols = size(b, 2);
            colidx = repmat(1:bcols, obj.C.Nobs, 1);
            colidx = colidx(:);
            
            b = b(obj.C.Iobs);
            L = obj.C.L(obj.C.Iobs);
            initial = initial(obj.C.Iobs);
                        
            x = initial;
            % r = b - Afun(x);
            r = b - (x + bsxfun(@times, L, obj.K.KMultGroupFiltered(bsxfun(@times, L, x), obj.C.ObsFilter)));
            if sqrt(sum(sum(r.^2))) < tol
                x = sparse(repmat(obj.C.Iobs, bcols, 1), colidx, x, brows, bcols);     
                return
            end
            y = -r;
            
            % z = Afun(y);
            z = (y + bsxfun(@times, L, obj.K.KMultGroupFiltered(bsxfun(@times, L, y), obj.C.ObsFilter)));
            s = sum(y.*z);
            t = sum(r.*y)/s;
            x = x + bsxfun(@times, t, y);

            for k = 1:brows;
               r = r - t*z;
               if(sqrt(sum(sum(r.^2))) < tol)
                 x = sparse(repmat(obj.C.Iobs, bcols, 1), colidx, x, brows, bcols);     
                 return
               end
               B = sum(r.*z)/s;
               y = -r + bsxfun(@times, B, y);
               
               % z = Afun(y);
               % z = (y + L .* obj.KMultGroupFiltered(L .* y, dimF));
               xx = bsxfun(@times, L, y);
               for g = 1:numel(obj.K.KsGrouped)   
                   Ng = numel(obj.C.ObsFilter{g});
                   xx = reshape(xx, Ng, (numel(obj.C.Iobs)*bcols) / Ng);
                   xx = xx'*obj.K.KsGrouped{g}(obj.C.ObsFilter{g},obj.C.ObsFilter{g});
               end   
               
               xx = reshape(xx, bcols, numel(obj.C.Iobs))';
               z = y + bsxfun(@times, L, xx);
                              
               s = sum(y.*z);
               t = sum(r.*y)/s;
               x = x + bsxfun(@times, t, y);
            end      
                        
            x = sparse(repmat(obj.C.Iobs, bcols, 1), colidx, x, brows, bcols);        
        end
        
    end
    
end

