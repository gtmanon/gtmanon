classdef KroneckerCov < handle
    %KRONECKERCOV Encapsulates a covariance matrix representable as a Kronecker product of smaller matrices of one-dimensional covariances Kd
    %
    %Many of the operations in this class are well documented in (Saatchi,
    %2011). 
    %   
    %References
    %   Saatci, Y. "Scalable Inference for Structured Gaussian Process 
    %               Models", PhD. thesis, University of Cambridge, 2011
    %   
    properties(GetAccess = 'public', SetAccess = 'protected')
            
        % Covariances for each dimension. Be sure to call prepareCov before 
        % using this internally
        Ks; 
              
        KGrouping;
        
        KsGrouped;
                
        % see setGroupingParameters
        GROUP_SIZE;
        LAST_GROUP_SIZE;
        
        % Kronecker product of diag(Ks)(1..idx-1) and (idx+1 ... end). Be 
        % sure to call prepareCov before using this internally
        kronDiagKsToIdx;
        kronDiagKsFromIdx;       
        
        % Eigenvectors of the Ks. Be sure to call prepareEigen before using 
        % this internally
        Qs;    
        
        % Transposed Eigenvector matrix, i.e. Qs{i}' of the Ks. Be sure to 
        % call prepareEigen before using this internally
        QsT;
        
        % Eigenvalues of the Ks. Be sure to call prepareEigen before using 
        % this internally
        LambdaKs;              
        
        % Kronecker product of LambdaKs(1..idx) and (idx ... end). Be sure 
        % to call prepareEigen before using this internally
        kronLambdaKsToIdx;
        kronLambdaKsFromIdx;
        
        % Eigenvalues of the full covariance matrix kron({Ks}). Be sure to 
        % call prepareEigen before using this internally
        diagLambdaK;
        
        % Diagonal of the full covariance matrix kron({Ks}). Be sure to 
        % call prepareCov before using this internally
        diagK;
        
        % Diagonal of the inverse of the full covariance matrix kron({Ks}). 
        % Be sure to call prepareEigen before using this internally
        diagKinv;
        
        % Indicates whether the Ks covariance matrices are prepared.
        % Row vector with one 0/1 element per dimension
        covPrepared;
        
        % Indicates whether the eigen-decomp related members are prepared.
        % Row vector with one 0/1 element per dimension
        eigenPrepared;   
        
        groupingPrepared;
        
        % Packed parameter row vector currently set for this instance.
        % Note: these are in original space, whereas updateParameters
        % expects its inputs to be in log-space
        currentParams;
    end
    
    properties(GetAccess = 'public', SetAccess = 'protected')
        
        axes; % 1xD cell array where each cell c contains a column vector with
              % grid values along dimension c
        
        D; % Dimensionality of the grid
        
        Ns; % Vector of grid sizes along each axis
        
        Ngs; % Vector of sizes for the square matrices KsGrouped
        
        N; % Total size of the Kronecker product in one dimension i.e. prod(Ns)
                
    end  
    
    methods(Access = 'public')
        
        function obj = KroneckerCov(newAxes)
            % Construct a new KroneckerCov object
            %
            % newAxes must be a 1xD cell array, one cell for each grid
            % dimensions. Each cell must contain a column vector of grid 
            % coordinates for the respective dimension.
            
            if(isempty(newAxes) || ~iscell(newAxes) || ...
               size(newAxes,1) ~= 1 || size(newAxes,2) < 1)
                error('newAxes must be a 1xD cell array with one cell for each grid dimensions');
            end
                        
            obj.axes = newAxes;                
            obj.D = size(newAxes,2);
            
            Ncols = cellfun(@(x) size(x,2), newAxes);
            if(any(Ncols ~= 1))
                error('All individual axes must be given as column vectors');
            end
            
            
            obj.Ns = cellfun(@(x) size(x,1), newAxes);
            obj.N = prod(obj.Ns);
            
            obj.covPrepared = zeros(1, obj.D);
            obj.eigenPrepared = zeros(1, obj.D);
            
            % Default grouping parameters
            obj.setGroupingParameters(10, 10);
            obj.groupingPrepared = 0;
            
            obj.currentParams = [];                      
        end
        
        function prepareAll(obj)
            %Perform all initializations that are otherwise done lazily
            
            if(~min(obj.covPrepared))
                obj.prepareCov();   
            end
            
            if(~min(obj.eigenPrepared))
                obj.prepareEigen();   
            end
            
            if(~obj.groupingPrepared)
                obj.prepareGrouping();
            end            
        end
        
        function setGroupingParameters(obj, groupSize, lastGroupSize)
                 
            if(groupSize < 2 || lastGroupSize < 2)
                error('groupSize and lastGroupSize must be >= 2.');
            end
            
            obj.GROUP_SIZE = groupSize;
            obj.LAST_GROUP_SIZE = lastGroupSize;
                             
            d = obj.D;
            currentSize = 1;
            currentGroup = [];
            currentGroupIdx = 1;
            
            while d > 0                
                if((currentGroupIdx == 1 && currentSize >= obj.LAST_GROUP_SIZE) || ...
                    currentGroupIdx > 1 && currentSize >= obj.GROUP_SIZE)
                
                    obj.KGrouping{currentGroupIdx} = currentGroup;                   
                    currentGroupIdx = currentGroupIdx + 1;
                    
                    currentSize = 1;
                    currentGroup = [];                    
                end
                
                currentSize = currentSize * obj.Ns(d);    
                currentGroup = [currentGroup d];
                                
                d = d - 1;
            end
            
            if(~isempty(currentGroup))
                obj.KGrouping{currentGroupIdx} = currentGroup;
            end            
        end
        
        function updateParameters(obj, newParams)   
            %Set the covariance parameters
            %
            %newParams is a row vector with log-transformed parameter
            %values. The size and arrangement of newParams depends on the
            %underlying covariance function.
            
            if(isempty(obj.currentParams))
                % Invalidate all parameters
                obj.currentParams = -1 * ones(size(newParams));
            end
            
            changedAxes = unique(obj.paramToAxis(find(abs(newParams - obj.currentParams))));

            obj.covPrepared(changedAxes) = 0;
            obj.eigenPrepared(changedAxes) = 0;
            obj.groupingPrepared = 0;
            
            obj.currentParams = exp(newParams);            
        end
        
        
        function Ks = getCovariances(obj)
            
            if(~min(obj.covPrepared))
                obj.prepareCov();   
            end
            
            Ks = obj.Ks;
            
        end
        
        function [ alpha, logdetKSigma2Eye ] = KSigma2EyeSolve(obj, sigma2, y, invertK)
            % Solve (K + sigma^2*eye(n))*alpha = y where K is an (NxN) 
            % Kronecker matrix and y an n-vector.
            %
            % y can either be a column vector of a size compatible to 
            % (K + sigma^2*eye(n)), or it can be a matrix with such column
            % vectors in its columns. In the latter case, alpha will
            % similarly be a matrix with one solution for each column of y
            % in its columns.
            %
            % If the optional invertK parameter is set to true, the method
            % determines the solution to (K^-1 + sigma^2*eye(n))*alpha = y
            % (note the inverted K) instead.
            %

            if(size(y,1) ~= obj.N)
                error('y must be a column vector or matrix with N rows.');
            end

            if(~min(obj.eigenPrepared))
                obj.prepareEigen();
            end
            
            if(nargin > 3 && invertK)
                alpha = obj.kron_mv_eigen(y, (1 ./ obj.diagLambdaK) + sigma2, @rdivide);                
                
                if nargout > 1
                    logdetKSigma2Eye = sum(log((1 ./ obj.diagLambdaK) + sigma2));
                end
            else                    
                alpha = obj.kron_mv_eigen(y, obj.diagLambdaK + sigma2, @rdivide);
                
                if nargout > 1
                    logdetKSigma2Eye = sum(log(obj.diagLambdaK + sigma2));
                end
            end            
        end
         
        function alpha = KSolve(obj, y)
            % Solve K*alpha = y where K is an (NxN) Kronecker matrix and y 
            % an n-vector.
            %
            % y must be a column vector of a size compatible to K
            %

            if(size(y,1) ~= obj.N)
                error('y must be a matrix with N rows.');
            end

            if(~min(obj.eigenPrepared))
                obj.prepareEigen();
            end
            
            alpha = obj.kron_mv_eigen(y, obj.diagLambdaK, @rdivide);                      
        end
        
        % TODO: This should go into a separate helper class.
        function x = BSolve(obj, L, b, dimF, frows, initial, tol)
            % bb = conjgrad(@(x) (x + sW.*gp.KroneckerCov.KMultGroup(sW.*x)), sW.*kb, bb, gp.CGACCURACY);  
          
            if nargin < 7
                tol = 1e-10;        
                if nargin < 6
                    initial = b;
                end
            end
            
            brows = size(b, 1);
            bcols = size(b, 2);
            colidx = repmat(1:bcols, numel(frows), 1);
            colidx = colidx(:);
            
            b = b(frows,:);
            L = L(frows,:);
            initial = initial(frows,:);
                        
            x = initial;
            % r = b - Afun(x);
            r = b - (x + bsxfun(@times, L, obj.KMultGroupFiltered(bsxfun(@times, L, x), dimF)));
            if sqrt(sum(sum(r.^2))) < tol
                x = sparse(repmat(frows, bcols, 1), colidx, x, brows, bcols);     
                return
            end
            y = -r;
            
            % z = Afun(y);
            z = (y + bsxfun(@times, L, obj.KMultGroupFiltered(bsxfun(@times, L, y), dimF)));
            s = sum(y.*z);
            t = sum(r.*y)/s;
            x = x + bsxfun(@times, t, y);

            for k = 1:brows;
               r = r - t*z;
               if(sqrt(sum(sum(r.^2))) < tol)
                 x = sparse(repmat(frows, bcols, 1), colidx, x, brows, bcols);     
                 return
               end
               B = sum(r.*z)/s;
               y = -r + bsxfun(@times, B, y);
               
               % z = Afun(y);
               % z = (y + L .* obj.KMultGroupFiltered(L .* y, dimF));
               xx = bsxfun(@times, L, y);
               for g = 1:numel(obj.KsGrouped)   
                   Ng = numel(dimF{g});
                   xx = reshape(xx, Ng, (numel(frows)*bcols) / Ng);
                   xx = xx'*obj.KsGrouped{g}(dimF{g},dimF{g});
               end   
               
               xx = reshape(xx, bcols, numel(frows))';
               z = y + bsxfun(@times, L, xx);
                              
%                xx = L .* y;
%                for g = 1:numel(obj.KsGrouped)              
%                    xx = reshape(xx, obj.Ngs(g), nb / obj.Ngs(g));
%                    xx = xx'*obj.KsGrouped{g};
%                end               
%                xx = xx(:);               
%                z = y + L .* xx;

               s = sum(y.*z);
               t = sum(r.*y)/s;
               x = x + bsxfun(@times, t, y);
            end      
                        
            x = sparse(repmat(frows, bcols, 1), colidx, x, brows, bcols);        
        end
                    
        function x = KMult(obj, b)
            % Compute the matrix-vector product x = K * b using a grouped Kronecker representation of K            
            if(~obj.groupingPrepared)
                obj.prepareGrouping();
            end
            
            [brows, bcols] = size(b);
            btotal = brows * bcols;            
            x = b;                        
            
            for g = 1:numel(obj.KsGrouped)
                Nd = size(obj.KsGrouped{g}, 1);
                
                x = reshape(x, Nd, btotal / Nd);
                x = x'*obj.KsGrouped{g};                    
            end                 
           
            x = reshape(x, bcols, brows);
            x = x';                
        end
        
        
        function x = KMultGroupFiltered(obj, b, groupedDimensionFilter)
            % Compute the matrix-vector product x = K * b using a grouped Kronecker representation of K            
            if(~obj.groupingPrepared)
                obj.prepareGrouping();
            end
            
            [brows, bcols] = size(b);
            btotal = brows * bcols;            
            x = b;                        
            
            for g = 1:numel(obj.KsGrouped)
                Nd = numel(groupedDimensionFilter{g});
                
                x = reshape(x, Nd, btotal / Nd);
                x = x'*obj.KsGrouped{g}(groupedDimensionFilter{g},groupedDimensionFilter{g});                    
            end                 
           
            x = reshape(x, bcols, brows);
            x = x';                
        end       
        
        function x = KSqMult(obj, b)
            %Compute the matrix-vector product x = (K*K) * b
            %
            %b can either be a column vector of a size compatible to K, or 
            %a matrix with multiple such vectors in the columns. In the latter 
            %case, x will also be a matrix containing the desired products 
            %in its columns.    
            
            if(~min(obj.covPrepared))
                obj.prepareCov();
            end
               
            [brows, bcols] = size(b);
            btotal = brows * bcols;            
            x = b;                        
            
            for d = obj.D:-1:1
                x = reshape(x, obj.Ns(d), btotal / obj.Ns(d));
                x = x'*(obj.Ks{d} .^ 2);                    
            end                 
            
            x = reshape(x, bcols, brows);
            x = x';       
        end
        
        
        function alpha = KdpMult(obj, x, i, dKi_dthi)
            % Compute the matrix-vector product alpha = dK/dtheta_i * x 
            %
            % Here dK/dtheta_i is the derivative of the matrix K w.r.t. the
            % i-th hyperparameter and dKi_dthi is the derivative of K
            % evaluated along the i-th axis.
            
            if(~min(obj.eigenPrepared))
                obj.prepareEigen();
            end
            
            [QKi_dthi, LambdaKi_thi] = eig(dKi_dthi);
            
            Qsalt = cell(1,obj.D);
            Qsalt{i} = QKi_dthi;
                        
            diagLambdaK_thi = kron(obj.kronLambdaKsToIdx{i}, ...
                                   kron(diag(LambdaKi_thi), obj.kronLambdaKsFromIdx{i}));
              
            alpha = obj.kron_mv_ext(x, diagLambdaK_thi, @times, Qsalt);                                             
        end
        
        
        function Alpha = KdpMultAll(obj, b, dK_dth)
            % Compute all matrix-vector products alpha = dKi/dtheta_i * b
            %
            % Here dK/dtheta_i is the derivative of the matrix K w.r.t. the
            % i-th hyperparameter and dKi_dthi is the derivative of K
            % evaluated along the i-th axis.            
            if(~min(obj.covPrepared))
                obj.prepareCov();
            end
            
            NP = numel(dK_dth);
            A = obj.paramToAxis(1:NP);
            
            [brows, bcols] = size(b);
            btotal = brows * bcols;            
            
            Alpha = cell(1, NP);                        
            x = b;
            
            for p = NP:-1:1                           
                d = A(p);
                x = reshape(x, obj.Ns(d), btotal /obj.Ns(d));
                
                Alpha{p} = x'*dK_dth{p};
                
                if(p == min(find(A==d)))
                    % We have moved to a different axis, maintain running
                    % tally of current x.                    
                    x = x'*obj.Ks{d};
                    
                    for p2 = min(find(A>d)):NP  
                        Alpha{p2} = reshape(Alpha{p2}, obj.Ns(d), btotal /obj.Ns(d));
                        Alpha{p2} = Alpha{p2}'*obj.Ks{d};
                    end          
                end
            end 
            
            for p = NP:-1:1  
                Alpha{p} = reshape(Alpha{p}, bcols, brows);
                Alpha{p} = Alpha{p}';
            end                                          
        end       
                
        function d = diagKdp(obj, i, dKi_dthi)
            % Compute the diagonal of dK/dtheta_i
            %
            % Here dK/dtheta_i is the derivative of the matrix K w.r.t. the
            % i-th hyperparameter and dKi_dthi is the derivative of K
            % evaluated along the i-th axis.
                      
            if(~min(obj.covPrepared))
                obj.prepareCov();
            end
            
            d = kron(obj.kronDiagKsToIdx{i}, kron(diag(dKi_dthi), obj.kronDiagKsFromIdx{i}));            
        end
        
        function K = fullK(obj, idx, Kdp)            
            if(~min(obj.covPrepared))
                obj.prepareCov();
            end
            
            K = 1;
            for d = 1:obj.D
                if(nargin == 1 || d ~= idx)
                    K = kron(K, obj.Ks{d});
                else
                    K = kron(K, Kdp);
                end                    
            end            
        end
                
        function Ksub = subK(obj, rowIdx, colIdx)            
            %Return a submatrix of K containing only the rows (and columns) 
            %contained in the index matrix rowIdx, see row2idx for details.
            %
            %If the optional colIdx is given, it must have the same format
            %as rowIdx and represent indices into the columns of K
            if(~min(obj.covPrepared))
                obj.prepareCov();
            end
            
            if(nargin < 3)
                colIdx = rowIdx;
            end
            
            Ksub = obj.Ks{obj.D}(rowIdx(:,obj.D), colIdx(:,obj.D));
            for d = obj.D-1:-1:1 
                Ksub = bsxfun(@times, Ksub, obj.Ks{d}(rowIdx(:,d), colIdx(:,d)));                
            end           
        end   
        
        function Kdpsub = subKdp(obj, rowIdx, i, Kdp)            
            %Return a submatrix of K containing only the rows (and columns) 
            %contained in the index matrix rowIdx, see row2idx for details.
            
            if(~min(obj.covPrepared))
                obj.prepareCov();
            end
                       
            Kdpsub = Kdp(rowIdx(:,i), rowIdx(:,i));            
            for d = obj.D:-1:1 
                if(d ~= i)
                    Kdpsub = bsxfun(@times, Kdpsub, obj.Ks{d}(rowIdx(:,d), rowIdx(:,d)));                   
                end
            end           
        end
        
        function Qsub = eigenLR(obj, rowIdx, eigenIdx)
            %Return the eigenvectors enumerated in the index matrix eigenIdx
            %
            %eigenIdx is an index matrix containing one set of indices into
            %the covariance matrices Ki per row. That is eigenIdx has obj.D 
            %columns and as many rows as desired eigenvectors. See
            %KroneckerCov.row2idx for how to generate this matrix from the
            %column numbers of the desired eigenvectors.
            %
            %Similarly, rowIdx can be used to obtain only
            %certain rows of the eigenvectors specified in eigenIdx. The
            %expected format for rowIdx is the same.
                        
            if(~min(obj.eigenPrepared))
                obj.prepareEigen();
            end
            
            %Nvec = size(eigenIdx, 1);
            
            Qsub = obj.Qs{1}(rowIdx(:,1), eigenIdx(:,1));
            for d = 2:obj.D                            
                Qsub = bsxfun(@times, obj.Qs{d}(rowIdx(:,d), eigenIdx(:,d)), Qsub);                
            end              
        end            
              
        function x = kron_mv(obj, b)
            %Evaluate kron(Ks(1), ..., Ks(m))*b efficiently
            %
            %b can either be a column vector of a size compatible to the
            %kron(.) evaluation, or a matrix with multiple such vectors in
            %the columns. In the latter case, x will also be a matrix
            %containing the desired products in its columns.                       
            %
            %Adapted from Y. Saatchi, see https://github.com/ysaatchi/gp-grid
            
            [brows, bcols] = size(b);
            btotal = brows * bcols;            
            x = b;                        
            
            for d = obj.D:-1:1
                x = reshape(x, obj.Ns(d), btotal / obj.Ns(d));
                x = x'*obj.Ks{d};                    
            end                 
           
            x = reshape(x, bcols, brows);
            x = x';
        end  
        
        function x = kron_mv_eigen(obj, b, alpha, diagOp)
            %Evaluate kron(Ks(1), ..., Ks(m))*b efficiently
            %
            %b can either be a column vector of a size compatible to the
            %kron(.) evaluation, or a matrix with multiple such vectors in
            %the columns. In the latter case, x will also be a matrix
            %containing the desired products in its columns.
            %
            %alpha must be a column vector of a size compatible to the
            %kron(.) evaluation, containing the diagonal of egivenvalues of
            %K.
            %
            %diagOp is the (bsxfun) operation through which alpha is
            %applied. @times amounts to multiplication with K, @rdivide to
            %multiplication with K^-1                        
            [brows, bcols] = size(b);
            btotal = brows * bcols;            
            x = b;                        
            
            for d = obj.D:-1:1
                x = reshape(x, obj.Ns(d), btotal / obj.Ns(d));
                x = x'*obj.Qs{d};                    
            end                 
            x = reshape(x, bcols, brows);
            x = x';
            x = bsxfun(diagOp, x, alpha);
            
            for d = obj.D:-1:1
                x = reshape(x, obj.Ns(d), btotal / obj.Ns(d));
                x = x'*obj.QsT{d};                     
            end  
            
            x = reshape(x, bcols, brows);
            x = x';
        end 
                     
        function x = kron_mv_ext(obj, b, alpha, diagOp, Qsalt) 
            %Evaluate kron(Ks(1), ..., Ks(m))*b efficiently
            %
            %b can either be a column vector of a size compatible to the
            %kron(.) evaluation, or a matrix with multiple such vectors in
            %the columns. In the latter case, x will also be a matrix
            %containing the desired products in its columns.
            %
            %alpha must be a column vector of a size compatible to the
            %kron(.) evaluation, containing the diagonal of egivenvalues of
            %K.            
            %
            %diagOp is the (bsxfun) operation through which alpha is
            %applied. @times amounts to multiplication with K, @rdivide to
            %multiplication with K^-1
            %
            %Qsalt is a cell array of replacement eigenvectors. Qsalt must
            %be of size obj.D. For each empty cell, the original
            %eigenvectors obj.Qs{d} are used. For each non-empty cell, the
            %eigenvectors Qsalt{d} are used in that dimension. Make sure
            %that alpha has the proper size if you are using smaller
            %eigenvector matrices (for example, because you are working
            %with dimension filters).
            
            [brows, bcols] = size(b);
            btotal = brows * bcols;            
            x = b;  
            
            for d = obj.D:-1:1
                if(isempty(Qsalt{d}))
                    x = reshape(x, obj.Ns(d), btotal /obj.Ns(d));
                    x = x'*obj.Qs{d};
                else
                    x = reshape(x, size(Qsalt{d},1), []);
                    x = x'*Qsalt{d};
                end                                              
            end     

            x = reshape(x, bcols, brows);
            x = x';
            x = bsxfun(diagOp, x, alpha);

            for d = obj.D:-1:1
                if(isempty(Qsalt{d}))
                    x = reshape(x, obj.Ns(d), btotal /obj.Ns(d));
                    x = x'*obj.QsT{d};
                else
                    x = reshape(x, size(Qsalt{d},2), []);
                    x = x'*Qsalt{d}';
                end                                             
            end  
                
            x = reshape(x, bcols, brows);
            x = x';
        end
               
        function idx = row2idx(obj, rows)
            % Convert row indeces into indices for the component matrices
            % of the Kronecker product 
            %
            % Rows is a column vector of R row indices in the range 1:obj.N, the
            % return value is a matrix with R rows and obj.D columns
            % containing one set of indices per row.
                       
            if(size(rows, 2) ~= 1)
                error('rows must be a column vector.');
            end
            
            Nrows = size(rows, 1);
            
            idx = zeros(Nrows, obj.D);
            total = obj.N;
    
            for nd = 1:obj.D
                total = total / obj.Ns(nd);
                idx(:,nd) = floor((rows - 1) ./ total) + 1;
                rows = rows - (idx(:,nd) - 1) * total;
            end            
        end
        
        function idx = row2groupedidx(obj, rows)
            % Convert row indeces into indices for the grouped component 
            % matrices of the Kronecker product 
            %
            % rows is a column vector of R row indices in the range 1:obj.N, the
            % return value is a matrix with R rows and numel(obj.KGrouping) columns
            % containing one set of indices per row.
            %
            % see also row2idx()
                       
            if(size(rows, 2) ~= 1)
                error('rows must be a column vector.');
            end
            
            if(~obj.groupingPrepared)
                obj.prepareGrouping();
            end
            
            Nrows = size(rows, 1);
            
            gD = numel(obj.KGrouping);
            
            idx = zeros(Nrows, gD);
            total = obj.N;
    
            for nd = gD:-1:1
                total = total / size(obj.KsGrouped{nd}, 1);
                idx(:,nd) = floor((rows - 1) ./ total) + 1;
                rows = rows - (idx(:,nd) - 1) * total;
            end            
        end
        
        
        function [dimFilter, coveredRows, rowMapping] = createDimensionFilter(obj, rows)
            % Given a set of rows (i.e., indices into 1:obj.N), compute a
            % minimal set of indices into each dimension required to cover
            % all rows.
            %
            % Specifically, for a subset of rows of K, it may be the case
            % that not all dimensions of each Ks are required to build the
            % submatrix K(rows, rows). A dimension filter finds the minimal
            % number of indices in each dimension such that the product
            % kron_1^D(Ks(dimFilter{d})) covers K(rows, rows). Note that in
            % general we will get a set of indices that, if fully expanded
            % through kron() will yield a K that is larger than K(rows,
            % rows). Therefore, working with dimension filters only pays
            % off when the number of rows is small compared to K.N.
            %
            % rows is a column vector of indices into 1:obj.N
            %
            % dimFilter is a cell array of size obj.D where each cell
            % contains the pertinent indices into obj.Ks{d} for that
            % dimension.
            %
            % coveredRows is a vector of rows (in the space of the full K) 
            % covered if kron_1^D(Ks(dimFilter{d})) is expanded with the
            % generated dimension filter. It is a superset of rows.
            %
            % rowMapping (optional) is a list of all indices in coveredRows that ware
            % also part of rows
            
            indices = num2cell(obj.row2idx(rows), 1); % cell array with the referenced values per dimension
            dimFilter = cellfun(@(col) unique(col), indices, 'UniformOutput', false); % Unique, sorted
            
            coveredRows = dimFilter{obj.D};
            contrib = 1;
            for d = obj.D-1:-1:1         
                contrib = contrib * obj.Ns(d+1);
                coveredRows = reshape( ...
                    repmat(((dimFilter{d}-1)*contrib)', numel(coveredRows), 1), [], 1) + ... % Contribution from dimension d
                    repmat(coveredRows, numel(dimFilter{d}), 1); % Contribution from dimension d + 1
            end
            
            if(nargout > 2)
                rowMapping = find(ismember(coveredRows, rows));            
            end
        end
                
        function [dimFilter, coveredRows, rowMapping] = createGroupedDimensionFilter(obj, rows)
            % Given a set of rows (i.e., indices into 1:obj.N), compute a
            % minimal set of indices into each dimension of the grouped
            % covariance matrices KsGrouped required to cover all rows.
            %
            % see createDimensionFilter() for further details.
            
            indices = num2cell(obj.row2groupedidx(rows), 1); % cell array with the referenced values per dimension
            dimFilter = cellfun(@(col) unique(col), indices, 'UniformOutput', false); % Unique, sorted
            
            gD = numel(obj.KGrouping);
            
            coveredRows = dimFilter{1};
            contrib = 1;
            for d = 2:1:gD
                contrib = contrib * size(obj.KsGrouped{d-1},1);
                coveredRows = reshape( ...
                    repmat(((dimFilter{d}-1)*contrib)', numel(coveredRows), 1), [], 1) + ... % Contribution from dimension d
                    repmat(coveredRows, numel(dimFilter{d}), 1); % Contribution from dimension d + 1
            end
            
            if(nargout > 2)
                rowMapping = find(ismember(coveredRows, rows));            
            end
        end
                
    end
    
    methods(Access = 'protected')
        
        function prepareCov(obj)    
            
            Dchange = find(~obj.covPrepared);
            
            if(~isempty(Dchange))                            
                if(isempty(obj.Ks))
                    obj.Ks = cell(1, obj.D);
                end
                
                obj.Ks(Dchange) = obj.evaluateCovAxes(Dchange);               
                obj.diagK = 1;
                
                for i = 1:obj.D
                    obj.diagK = kron(obj.diagK, diag(obj.Ks{i}));                     
                end
                
                if(isempty(obj.kronDiagKsToIdx))
                    obj.kronDiagKsToIdx = cell(1, obj.D);
                    obj.kronDiagKsFromIdx = cell(1, obj.D);
                end
                               
                obj.kronDiagKsToIdx{1} = 1;    
                for split = (min(Dchange)+1):obj.D                                    
                    obj.kronDiagKsToIdx{split} = kron(obj.kronDiagKsToIdx{split-1}, diag(obj.Ks{split-1}));
                end
                    
                obj.kronDiagKsFromIdx{obj.D} = 1;                    
                for split = (max(Dchange)-1):-1:1
                    obj.kronDiagKsFromIdx{split} = kron(diag(obj.Ks{split+1}), obj.kronDiagKsFromIdx{split+1});
                end                    
                
                obj.covPrepared = ones(1, obj.D);
            end
            
        end
        
        function prepareEigen(obj)                                     
            % Make sure the component covariance matrices obj.Ks are evaluated
            if(~min(obj.covPrepared))
                obj.prepareCov(); 
            end
            
            Dchange = find(~obj.eigenPrepared);
            
            if(~isempty(Dchange))
                if(isempty(obj.Qs))                               
                    obj.Qs = cell(1, obj.D);
                    obj.QsT = cell(1, obj.D);                
                    obj.LambdaKs = cell(1, obj.D);                                      
                end
                                
                for i = Dchange
                    [obj.Qs{i},Lambda] = eig(obj.Ks{i});  
                    obj.QsT{i} = obj.Qs{i}';
                    obj.LambdaKs{i} = diag(Lambda);  
                end      
                
                obj.diagLambdaK = 1;
                obj.diagKinv = 1;
                
                for i = 1:obj.D
                    obj.diagLambdaK = kron(obj.diagLambdaK, obj.LambdaKs{i});               
                    obj.diagKinv = kron(obj.diagKinv, diag(obj.Qs{i} * diag(1./obj.LambdaKs{i}) * obj.QsT{i})); 
                end
                
                if(isempty(obj.kronLambdaKsToIdx))
                    obj.kronLambdaKsToIdx = cell(1, obj.D);
                    obj.kronLambdaKsFromIdx = cell(1, obj.D);
                end
                
                obj.kronLambdaKsToIdx{1} = 1;                     
                for split = (min(Dchange)+1):obj.D                
                    obj.kronLambdaKsToIdx{split} = kron(obj.kronLambdaKsToIdx{split-1}, ...
                        obj.LambdaKs{split-1});
                end
                
                obj.kronLambdaKsFromIdx{obj.D} = 1;                    
                for split = (max(Dchange)-1):-1:1
                    obj.kronLambdaKsFromIdx{split} = kron(obj.LambdaKs{split+1}, ...
                        obj.kronLambdaKsFromIdx{split+1});
                end  

                obj.eigenPrepared = ones(1, obj.D);
                
            end            
        end          
        
        function prepareGrouping(obj)
            
            if(~min(obj.covPrepared))
                obj.prepareCov(); 
            end
                     
            Ngroup = numel(obj.KGrouping);
            obj.KsGrouped = cell(1, Ngroup);
            obj.Ngs = zeros(1, Ngroup);
            
            for g = 1:Ngroup
                obj.KsGrouped{g} = obj.Ks{obj.KGrouping{g}(1)};
                for i = 2:numel(obj.KGrouping{g})
                    obj.KsGrouped{g} = kron(obj.Ks{obj.KGrouping{g}(i)}, obj.KsGrouped{g});
                end                
                obj.Ngs(g) = size(obj.KsGrouped{g}, 1);
            end
            
            obj.groupingPrepared = 1;
            
        end
    end
    
    methods(Abstract)
        
        Ks = evaluateCovAxes(obj);
        
        axis = paramToAxis(obj, idx);
        
        param = axisToParam(obj, axis);
        
    end
  
end

