classdef CacheCollaborative < handle
    
    properties(GetAccess = 'public')
        
        % Number of processes to estimante
        Nproc;
        
        % Lower Cholesky factor of the negative Hessian of the log
        % likelihood
        L;
                
        % Non-zero rows indices into L and the first task in L,
        % respectively
        Lrows;        
        LrowsS;
        
        % The S in the low-rank approximation of K: QSQ'+Lambda (only for
        % a single task)
        LRS;
        
        % The Q in the low-rank approximation of K: QSQ'+Lambda (only for
        % a single task)
        LRQ;
        
        % Lower Cholesky factor of P = S^-1 + Q' * Lambda3 * Q
        LRC;
                
        LRLambda3;
        
        % Number of observed grid locations, for all tasks and one task, 
        % respectively    
        Nobs;
        NobsS;
        
        % Number of columns in the low-rank approximation (i.e. the size of
        % LRS), for all and for a single tasks, respectively.
        NLR;
        NLRS;
        
        % Output of KroneckerCov.createGroupedDimensionFilter for the
        % current observation locations on a single task
        ObsFilterS;        
        
        ObsCoveredRows;
        ObsCoveredRowsS;
      
        NObsCoveredRows;
        NObsCoveredRowsS;
              
        ObsRowMapping;
        ObsRowMappingS;
                
    end
    
    properties(Access = 'private')
                
        % Reference to the KroneckerCov that goes with this Cache
        K;
                
        % Linear indices of observed grid locations
        Iobs;
        
        % Column indices of vectors used for low-rank approximations
        ILR;
                
        % Index matrix (as returned by KroneckerCov.row2idx) for rows in K
        % corresponding to observed indices
        KOindices;
        
        % Index matrix (as returned by KroneckerCov.row2idx) for columns in 
        % K's summetric factors Q used in the reduced rank approximation
        KLRindices;
        
        jitter = 1e-10;
        jitterMat;
        
    end    
    
    methods(Access = 'public')
        
        function val = needsInitialization(obj)
            val = isempty(obj.K);
        end
        
        function initialize(obj, K, indObs, NPROC)
            
            obj.K = K;
            obj.Nproc = NPROC;
            
            obj.Iobs = indObs;
            obj.NobsS = numel(obj.Iobs);
            obj.Nobs = NPROC * numel(obj.Iobs);
            
            obj.LrowsS = obj.Iobs;
            obj.Lrows = bsxfun(@plus, (0:NPROC-1)*K.N, repmat(obj.LrowsS, 1, NPROC));
            obj.Lrows = obj.Lrows(:);
                        
            obj.KOindices = obj.K.row2idx(obj.Iobs);
            
            [obj.ObsFilterS, obj.ObsCoveredRowsS, obj.ObsRowMappingS] = ...
                obj.K.createGroupedDimensionFilter(obj.Iobs);
            obj.NObsCoveredRowsS = numel(obj.ObsCoveredRowsS);
            
            obj.ObsCoveredRows = bsxfun(@plus, (0:NPROC-1)*K.N, repmat(obj.ObsCoveredRowsS, 1, NPROC));
            obj.ObsCoveredRows = obj.ObsCoveredRows(:);
            obj.NObsCoveredRows = numel(obj.ObsCoveredRows);
            
            obj.ObsRowMapping = bsxfun(@plus, (0:NPROC-1)*obj.NObsCoveredRowsS, repmat(obj.ObsRowMappingS, 1, NPROC));
            obj.ObsRowMapping = obj.ObsRowMapping(:);
            
        end
        
        function updateW(obj, W)
            if(isempty(obj.jitterMat) || any(size(obj.jitterMat) ~= size(W)))
                obj.jitterMat = speye(size(W,1)) * obj.jitter;
            end
                        
            obj.L = chol(W + obj.jitterMat, 'lower');
            % obj.L = ichol(W + obj.jitterMat);       
        end
                    
        function updateILR(obj, ILR)
            
            obj.ILR = ILR;
            obj.NLRS = numel(ILR);
            obj.NLR = obj.Nproc * obj.NLRS;
            
            
            obj.KLRindices = obj.K.row2idx(ILR);            
            obj.LRS = obj.K.diagLambdaK(ILR);        
            
            obj.LRQ = obj.K.eigenLR(obj.KOindices, obj.KLRindices);
            
            
            Lambda1 = sparse(1:obj.Nobs, 1:obj.Nobs, ...
                repmat(obj.K.diagK(obj.LrowsS) - sum(bsxfun(@times, obj.LRQ.^2, obj.LRS'), 2), obj.Nproc, 1)) ;              
            
            Lambda2 = speye(obj.Nobs) + obj.L(obj.Lrows,obj.Lrows)' *  Lambda1 * obj.L(obj.Lrows,obj.Lrows);                                                                        
            
            obj.LRLambda3 = obj.L(obj.Lrows, obj.Lrows) * (Lambda2 \ (obj.L(obj.Lrows,obj.Lrows)'));    
                        
            % Computer LRC using an LRQ for only a single GP
            P = cell(obj.Nproc, obj.Nproc);
            
            % In a square block matrix with blocks of size sz, return the
            % indices of the ith block
            diagidx = @(i, sz) (((i-1)*sz + 1):(i*sz))';
                        
            % Compute P = S^-1 + Q' * Lambda3 * Q
            for col = 1:obj.Nproc
                for row = col:obj.Nproc
                    % The diagonal of the Lambda3 block at (row, col)
                    L3 = obj.LRLambda3(sub2ind(size(obj.LRLambda3), ...
                        diagidx(row, obj.NobsS), diagidx(col, obj.NobsS)));
                        
                    P{row,col} = obj.LRQ' * bsxfun(@times, L3, obj.LRQ);
                    
                    if(row == col)
                        P{row,col} = P{row,col} + diag(1 ./ obj.LRS);
                    else
                        P{col,row} = P{row,col};
                    end
                end
            end
            
            obj.LRC = chol(cell2mat(P), 'lower');
                                 
        end      
                
    end
   
end

