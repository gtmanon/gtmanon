function Ax = pcgwrapper(x, gp)

    LxAlt = gp.C.L(gp.C.Lrows,gp.C.Lrows) * x;       
    sKLxAlt = reshape(sparse(gp.C.ObsRowMapping,1,LxAlt, gp.C.NObsCoveredRows,1), [], gp.NPROC);    
    KLxAlt = gp.KroneckerCov.KMultGroupFiltered(sKLxAlt, gp.C.ObsFilterS);    
    KLxAlt = KLxAlt(gp.C.ObsRowMappingS,:);
    Ax = x + gp.C.L(gp.C.Lrows,gp.C.Lrows)'*KLxAlt(:);

