

prepare_data;

BCD = DATASETS{1};
% BCD = BCD.selectUsers(1);

CLD = BCD.toClassificationDataset([0 0 0 0], true); % Sushi
% CLD = BCD.toClassificationDataset([0 0 0 0 0], true); % Tariffs

CLD.discretize([-1 -1 8 8], 'equalspace'); % Sushi
% CLD.discretize([ 8 8 8 -1 8 ], 'equalspace');

CLD.normalize();
% CLD = DATASETS_CLD{1};
[~, idx] = sort(CLD.uniques(), 'descend');
CLD.switchColumns(idx);

[ GDS, rowGridIndices ] = CLD.toGrid(false, []);
AX = GDS.getAxes()';
Ytrain = GDS.YBinary([], true);

lengthScale = 1 * ones(1, CLD.DX());
sigma = 0.5;

clear gp gpreg;



%{
E = GPPGridLaplaceExperiment(CLDdisc);
E.setInput('EIGENLIMIT', 0.01);
E.setInput('EIGENCOUNTLIMIT', 300);
E.setInput('EIGENSAFE', 0.5);
E.setInput('NWMAX', 100);
E.setInput('FORCEAPPROX', true);
E.run();
%}

%for ALLG = 2:20
%for LASTG = 2:20

K = KroneckerSEXPCov(AX);
K.updateParameters(log([sigma lengthScale]));
K.setGroupingParameters(7, 10);

NPROC = 3;

Gamma = abs(randn(BCD.NU(), NPROC));
Gamma = Gamma ./ repmat(sum(Gamma, 2), 1, NPROC);

gpcfkron = gpcf_kronsexp('lengthScale', lengthScale, 'magnSigma2', sigma);      
gpcfkron = gpcf_kronsexp(gpcfkron, 'lengthScale_prior', prior_logunif(), ...
                                   'magnSigma2_prior', prior_sqrtunif());

gp = gp_set('lik', lik_probit_weighted(), 'cf', gpcfkron, 'jitterSigma2', 1e-9, 'KroneckerCov', K, ...
    'EXMAX', 150, 'FORCEAPPROX', false, 'DEBUG', false, 'SKIPIMPLICIT', false, ...
    'EIGENLIMIT', 0.01, 'EIGENCOUNTLIMIT', 100, 'EIGENSAFE', 999, 'CGACCURACY', 1E-6, ...
     'GAMMA', Gamma);


opt=optimset('TolFun', 1e-3, 'TolX', 1e-3, 'DerivativeCheck', 'off', 'Display', 'iter');
opt.lambdalim = 1E6;

tic;
% gp=gp_optim(gp, AX, { Ytrain }, 'optimf', @fminscg, 'opt', opt);
[ E, Var ] = gp_pred(gp, AX, { Ytrain }, AX);
rt = toc;

accuracy_kron = sum(sign(Ytrain.values) == sign(sum(E .* Gamma(Ytrain.tasks,:), 2))) / numel(Ytrain.indices);
        

% gpcf_reg = gpcf_sexp('lengthScale', lengthScale, ...
%                  'magnSigma2', sigma, ...
%                  'lengthScale_prior', prior_logunif(), ...
%                  'magnSigma2_prior', prior_sqrtunif());
% 
% gpreg = gp_set('lik', lik_probit(), 'cf', gpcf_reg, ...
%             'jitterSigma2', 1E-9, 'latent_method', 'Laplace');
% 
%         
% Yreg = CLD.YBinary([], true);
%         
% gpreg = gp_optim(gpreg, CLD.X(), Yreg, 'opt', opt);
% [ E_reg, Var_reg ] = gp_pred(gpreg, CLD.X(), Yreg);
% 
% accuracy_reg = sum(sign(Ytrain.values) == sign(E_reg)) / numel(Ytrain.indices);


%fprintf('ALLG = %d, LASTG = %d, RT=%f\n', ALLG, LASTG, rt);
%end
%end