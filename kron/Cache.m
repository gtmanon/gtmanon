classdef Cache < handle
    
    properties(GetAccess = 'public')
        
        % W^{1/2} in matrix and vector representation
        L;
        Lvec;
        W;
        
        % Number of observed grid locations
        Nobs; 
        
        % Linear indices of observed grid locations
        Iobs;
        
        % The S in the low-rank approximation of K: QSQ'+Lambda
        LRS;  
        
        % The Q in the low-rank approximation of K: QSQ'+Lambda
        LRQ; 
        
        % Lower Cholesky factor of P = S^-1 + Q' * Lambda3 * Q
        LRC;
                
        LRLambda2;
        
        LRLambda3;
        
        % Lambda3 * Q * P^-1 * Q' * Lambda3
        L3QPinvQtL3;       
        
        % Index matrix (as returned by KroneckerCov.row2idx) for rows in K
        % corresponding to observed indices
        KOindices;
        
        % Dimension filter covering all observed grid locations
        ObsFilter;                
        ObsCoveredRows;      
        ObsRowMapping;
        
        % Sparse matrix indices for a matrix with entries at all pairs of
        % observed locations (Iobs, Iobs)
        OOridx;
        OOcidx;
        
        % Predictive variance. Might be computed and stored by the
        % gradients computation here
        predVar;
        
    end
    
    properties(Access = 'private')   
                 
        % Reference to the KroneckerCov that goes with this Cache
        K;
        
        % Column indices of vectors used for low-rank approximations
        ILR;
        
        % Index matrix (as returned by KroneckerCov.row2idx) for columns in 
        % K's summetric factors Q used in the reduced rank approximation
        KLRindices;
        
        % Index matrix (as returned by KroneckerCov.row2idx) for all rows
        % in K
        KAindices;
        
        % Sparse matrix indices for a matrix with entries at pairs of
        % observed locations (rows) and 1:Nobs (columns)
        OAridx;
        OAcidx;
        
        % Submatrix of K at all locations (rows) and only observed
        % locations (columns). Can be queried using getSubKAllObserved
        subKAO;
    end    
    
    methods(Access = 'public')
        
        function val = needsInitialization(obj)
            val = isempty(obj.K);
        end
        
        function initialize(obj, K, indObs, NPROC)
            
            obj.K = K;
            obj.Iobs = indObs;
            obj.Nobs = numel(obj.Iobs);
                        
            [ obj.OOridx, obj.OOcidx ] = ...
                meshgrid(obj.Iobs', obj.Iobs');
                        
            obj.OOridx = obj.OOridx(:); 
            obj.OOcidx = obj.OOcidx(:);
            
            [obj.OAcidx, obj.OAridx ] = ...
                meshgrid((1:obj.Nobs)', obj.Iobs');
            
            obj.OAridx = obj.OAridx(:); 
            obj.OAcidx = obj.OAcidx(:);
                    
            obj.KOindices = obj.K.row2idx(obj.Iobs);
            obj.KAindices = obj.K.row2idx((1:obj.K.N)');   
            
            [obj.ObsFilter, obj.ObsCoveredRows, obj.ObsRowMapping] = ...
                obj.K.createGroupedDimensionFilter(obj.Iobs);

        end
        
        function updateW(obj, W)
            obj.W = W;
            obj.Lvec = sqrt(W);
            obj.L = diag(obj.Lvec);            
        end
                    
        function updateILR(obj, ILR)            
            obj.ILR = ILR;

            obj.KLRindices = obj.K.row2idx(ILR);
            obj.LRS = obj.K.diagLambdaK(ILR);              
                      
            obj.LRQ = obj.K.eigenLR(obj.KOindices, obj.KLRindices);
            
            Lambda1 = obj.K.diagK(obj.Iobs) - sum(bsxfun(@times, obj.LRQ.^2, obj.LRS'), 2);    
            Lambda2 = (1 + obj.W(obj.Iobs) .* Lambda1);
            Lambda3 = obj.W(obj.Iobs) ./ Lambda2;
            
            obj.LRLambda2 = sparse(obj.Iobs, 1, Lambda2, obj.K.N, 1);
            obj.LRLambda3 = sparse(obj.Iobs, 1, Lambda3, obj.K.N, 1);
            
            LRL3Q = bsxfun(@times, Lambda3, obj.LRQ);
            obj.LRC = chol(diag(1./obj.LRS) + obj.LRQ' * LRL3Q, 'lower');
            
            L3QPinvQtL3local = LRL3Q * (obj.LRC' \ (obj.LRC \ LRL3Q'));
            obj.L3QPinvQtL3 = sparse(obj.OAridx, obj.OAcidx, L3QPinvQtL3local(:), obj.K.N, obj.Nobs); 
                                    
            obj.subKAO = obj.K.subK(obj.KAindices, obj.KOindices);   
            
        end
        
        function setPredictiveVariance(obj, predVar)
            obj.predVar = predVar;
        end
        
        function KAO = getSubKAO(obj)
            if(isempty(obj.subKAO))
                obj.subKAO = obj.K.subK(obj.KAindices, obj.KOindices);   
            end
            
            KAO = obj.subKAO;
        end
    end
        
end

