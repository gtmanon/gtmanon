function x = kron_mv(Ks, b)
    %Evaluate kron(Ks(1), ..., Ks(m))*b efficiently
    %
    %b can either be a column vector of a size compatible to the
    %kron(.) evaluation, or a matrix with multiple such vectors in
    %the columns. In the latter case, x will also be a matrix
    %containing the desired products in its columns.
    %
    %Adapted from Y. Saatchi, see https://github.com/ysaatchi/gp-grid

    [brows, bcols] = size(b);
    btotal = brows * bcols;            
    x = b;                        

    for d = numel(Ks):-1:1
        N = size(Ks{d}, 1);
        x = reshape(x, N, btotal / N);
        x = x'*Ks{d};                    
    end                 
    x = reshape(x, bcols, brows);
    x = x';  
end 