% clear all;
clear;

jitter = 1e-06;
noise = 0.001;

%g =@(x) sqrt(3*x(:,1).^2 + (2*x(:,2)).^2) + eps;
g =@(x) sqrt(x(:,1).^2 + (x(:,2)).^2) + eps;
f =@(x) sin(g(x))./g(x);

n1 = 100;
n2 = 100;

x1 = linspace(-8,8,n1)';
x2 = linspace(-8,8,n2)';
x = allcomb(x1,x2);
y = f(x) + normrnd(0,sqrt(noise),size(x,1),1);






% construct Kron GP
lik = lik_gaussian();
gpcf = gpcf_kronsexp('lengthScale', [1,1], 'magnSigma2', 1);
pl = prior_logunif();
pm = prior_sqrtunif();
gpcf = gpcf_kronsexp(gpcf, 'lengthScale_prior', pl, 'magnSigma2_prior', pm);
gp = gp_set('lik', lik, 'cf', gpcf, 'jitterSigma2', jitter);


% construct standard GP
I = randperm(n1*n2); %I = I(1:200);
X = x(I,:);
Y = y(I,:);

gpcf = gpcf_sexp('lengthScale', [1,1]);
gpcf = gpcf_sexp(gpcf, 'lengthScale_prior', pl, 'magnSigma2_prior', pm);
gp2 = gp_set('lik', lik, 'cf', gpcf, 'jitterSigma2', jitter);

% optimize standard GP
opt=optimset('TolFun',1e-3,'TolX',1e-3);
%gp2=gp_optim(gp2,X,Y,'opt',opt);


% restrict Kron GP
%gp.cf{1}.p.magnSigma2 = prior_fixed();
%gp.cf{1}.magnSigma2 = gp2.cf{1}.magnSigma2;
%gp.lik.p.sigma2 = prior_fixed();
%gp.lik.sigma2 = gp2.lik.sigma2;


% --- MAP estimate  ---
disp(' MAP estimate for the parameters')
% Set the options for the optimization
opt=optimset('TolFun',1e-3,'TolX',1e-3);
opt=optimset('TolFun',1e-3,'TolX',1e-3,'DerivativeCheck','on');
% Optimize with the scaled conjugate gradient method
gp=gp_optim(gp,{x1,x2},y,'opt',opt);
% 



% 
% 
figure;
[X,Y] = meshgrid(x1,x2);
Z = f(allcomb(x1,x2));
Z = gp_pred(gp, {x1,x2}, y, {x1,x2});
Z = reshape(Z,size(X));
mesh(Z);


% 
% 
% 
% gpcf = gpcf_kronsexp('lengthScale', 1, 'magnSigma2', 1);
% pl = prior_logunif();
% pm = prior_fixed();
% gpcf1 = gpcf_kronsexp(gpcf, 'lengthScale_prior', pl, 'magnSigma2_prior', pm);
% gpcf2 = gpcf_kronsexp(gpcf, 'lengthScale', 0.5, 'lengthScale_prior', pl, 'magnSigma2_prior', pm);
% gpcf = gpcf_kronsexp(gpcf, 'lengthScale', [1,0.5], 'lengthScale_prior', pl, 'magnSigma2_prior', pm);
% 
% b = rand(n1*n2,1);
% y=b;
% 
% K1 = gpcf1.fh.trcov(gpcf1, x1) + jitter*eye(n1);
% K2 = gpcf2.fh.trcov(gpcf2, x2) + jitter*eye(n2);
% 
% Ki = {K1,K2};
% 
% r = cellfun(@rows, Ki)';
% c = cellfun(@cols, Ki)';
% 
% repeat = 10;
% tic;
% for i=1:repeat
%   K1 = gpcf1.fh.trcov(gpcf1, x1);
%   K2 = gpcf2.fh.trcov(gpcf2, x2);
%   kpi({K1,K2},[2,1],r,c,2,b);
% end
% toc;
% 
% tic;
% for i=1:repeat
%   K = gpcf.fh.trcov(gpcf, X) + jitter*eye(size(X,1));
%   K*b;
% end
% toc;
% Kinv = inv(K+0.01*eye(size(K)));
% 
% nt1 = 4;
% nt2 = 2;
% xt1 = linspace(1,5,nt1)';
% xt2 = linspace(1,5,nt2)';
% Xt = [];
% for i=1:length(xt1)
%   for j=1:length(xt2)
%     Xt = [Xt;xt1(i),xt2(j)];   
%   end
% end
% 
% Ks1 = gpcf1.fh.cov(gpcf1, xt1, x1);
% Ks2 = gpcf2.fh.cov(gpcf2, xt2, x2);
% Ks = gpcf.fh.cov(gpcf, Xt, X);
% 
% 
% 
% 
% Ksi = {Ks1,Ks2};
% rt = cellfun(@rows, Ksi)';
% ct = cellfun(@cols, Ksi)';
% 
% 
% [u1,z1,v1] = svd(K1);
% [u2,z2,v2] = svd(K2);
% [u,z,v] = svd(K);
% 
% kron(u1,u2)*kron(z1,z2)*(kron(v1,v2)');
% 
% R = y;
% R = kpi({v1',v2'},[],r,c,2,R);
% R = (1./(kron(diag(z1),diag(z2)) + noise)).*R;
% R = kpi({u1,u2},[],r,c,2,R);