classdef KroneckerSEXPCov < KroneckerCov
    %KRONECKER Encapsulates a squared exponantial covariance matrix on a
    %grid
    
    properties
        
    end
    
    methods(Access = 'public')
       
        function obj = KroneckerSEXPCov(newAxes, initialParameters)
            
            % Set the grid axes for this instance and (optionally) initial
            % parameters
            %
            % initialParameters is a row vector of length numel(newAxes) +
            % 1 containing the log-transformed parameters values for sigma2
            % (first element) and the lengthscales (remaining elements)
            
            obj = obj@KroneckerCov(newAxes);
            
            if(nargin > 1)                
                obj.updateParameters(initialParameters);
            else
                obj.updateParameters(zeros(1, numel(newAxes) + 1));
            end
                    
        end
        
        function Ks = evaluateCovAxes(obj, dims)
                        
            if(nargin == 1 || isempty(dims))
                dims = 1:obj.D;               
            end            
            
            Ndims = numel(dims);            
            Ks = cell(1, Ndims);
            
            s2 = 1./obj.currentParams(1+dims).^2;
            if size(s2) == 1
                s2 = repmat(s2, 1, Ndims);
            end
            
            Kidx = 1;
            for d = dims                
                Ks{Kidx} = zeros(obj.Ns(d));
                
                for ii1=1:obj.Ns(d)-1
                    dist = zeros(obj.Ns(d)-ii1,1);
                    col_ind = ii1+1:obj.Ns(d);
                    dist = dist+s2(Kidx).*(obj.axes{d}(col_ind,1)-obj.axes{d}(ii1,1)).^2;
                    Ks{Kidx}(col_ind,ii1) = dist./2;
                end
                
                Ks{Kidx}(Ks{Kidx} < eps) = 0;                
                Ks{Kidx} = Ks{Kidx}+Ks{Kidx}';
                if d==1
                    Ks{Kidx} = (obj.currentParams(1)).*exp(-Ks{Kidx});
                else
                    Ks{Kidx} = exp(-Ks{Kidx});
                end
                
                Kidx = Kidx + 1;
            end            
        end               
    end 
    
    methods(Static)
        
         function axis = paramToAxis(i)
            % Return the axis into which the derivative w.r.t. the i-th
            % hyperparameter is folded.
            
            % For the SEXP, the sigma derivative is folded into the first
            % axis, whereas all lengthscale derivatives just affect their
            % own respective axes.
                                         
            % Equivalent: axis = arrayfun(@(idx) KroneckerSEXPCov.ifthenelse(idx == 1, 1, idx-1), i);
            idx1 = (i == 1);
            axis = i - 1;
            axis(idx1) = 1;            
         end
        
         function param = axisToParam(axis)
            % Return the parameters that are folded into the derivative w.r.t. 
            % the i-th hyperparameter. Note that multiple parameters can
            % all be folded into the same axis
            
            % For the SEXP, the sigma derivative is folded into the first
            % axis, whereas all lengthscale derivatives just affect their
            % own respective axes.
                                    
            % param = cellfun(@(a) KroneckerSEXPCov.ifthenelse(a == 1, [1,2], a+1), axis, 'UniformOutput', false);                       
            
            idx1 = (axis == 1);
            param = cellfun(@(a) a+1, axis, 'UniformOutput', false);   
            param{idx1} = [1,2];
                        
        end
        
        function val = ifthenelse(cond, thenval, elseval)
            
            if(cond)
                val = thenval;
            else
                val = elseval;
            end
            
        end
        
    end
    
end

