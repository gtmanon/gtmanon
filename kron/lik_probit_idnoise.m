function lik = lik_probit_idnoise(varargin)
%LIK_PROBIT  Create a Probit likelihood structure 
%
%  Description
%    LIK = LIK_PROBIT creates Probit likelihood for classification
%    problem with class labels {-1,1}.
%
%    Extension: Input dependent noise: Allow 0 labels whre observations do 
%               not enter the likelihood
%  
%    The likelihood is defined as follows:
%                  __ n
%      p(y|f, z) = || i=1 normcdf(y_i * f_i / Var_u(i))
%
%      or 1 if y_i = 0
%    
%      where f is the latent value vector.
%
%  See also
%    GP_SET, LIK_*
%

% Copyright (c) 2007      Jaakko Riihim�ki
% Copyright (c) 2007-2010 Jarno Vanhatalo
% Copyright (c) 2010 Aki Vehtari

% This software is distributed under the GNU General Public
% License (version 3 or later); please refer to the file
% License.txt, included with the software, for details.

  ip=inputParser;
  ip.FunctionName = 'LIK_PROBIT_IDNOISE';
  ip.addOptional('lik', [], @isstruct);
  ip.parse(varargin{:});
  lik=ip.Results.lik;

  if isempty(lik)
    init=true;
    lik.type = 'Probit';
  else
    if ~isfield(lik,'type') || ~isequal(lik.type,'Probit')
      error('First argument does not seem to be a valid likelihood function structure')
    end
    init=false;
  end

  if init
    % Set the function handles to the subfunctions
    lik.fh.pak = @lik_probit_pak;
    lik.fh.unpak = @lik_probit_unpak;
    lik.fh.ll = @lik_probit_ll;
    lik.fh.llg = @lik_probit_llg;    
    lik.fh.llg2 = @lik_probit_llg2;
    lik.fh.llg3 = @lik_probit_llg3;
    
    % Get frequently needed ll, llg and llg2 together
    lik.fh.llext = @lik_probit_llext;
    
    % Noise variance for task t
    lik.SigmaT = [];
  end

end

function [w,s] = lik_probit_pak(lik)
%LIK_PROBIT_PAK  Combine likelihood parameters into one vector.
%
%  Description 
%    W = LIK_PROBIT_PAK(LIK) takes a likelihood structure LIK and
%    returns an empty verctor W. If Probit likelihood had
%    parameters this would combine them into a single row vector
%    W (see e.g. lik_negbin). This is a mandatory subfunction used 
%    for example in energy and gradient computations.
%       
%     See also
%     LIK_NEGBIN_UNPAK, GP_PAK

  w = []; s = {};
end


function [lik, w] = lik_probit_unpak(lik, w)
%LIK_PROBIT_UNPAK  Extract likelihood parameters from the vector.
%
%  Description
%    W = LIK_PROBIT_UNPAK(W, LIK) Doesn't do anything.
% 
%    If Probit likelihood had parameters this would extracts them
%    parameters from the vector W to the LIK structure. This is a
%    mandatory subfunction used for example in energy and gradient 
%    computations.
%       
%  See also
%    LIK_PROBIT_PAK, GP_UNPAK

  lik=lik;
  w=w;
end

function lognormcdfz = lik_probit_ll(lik, y, f, z)
%LIK_PROBIT_LL  Log likelihood
%
%  Description
%    E = LIK_PROBIT_LL(LIK, Y, F) takes a likelihood structure
%    LIK, class labels Y, and latent values F. Returns the log
%    likelihood, log p(y|f,z). This subfunction is needed when 
%    using Laplace approximation or MCMC for inference with 
%    non-Gaussian likelihoods. This subfunction is also used 
%    in information criteria (DIC, WAIC) computations.
%
%  See also
%    LIK_PROBIT_LLG, LIK_PROBIT_LLG3, LIK_PROBIT_LLG2, GPLA_E

if(isstruct(y))
    tmp = y.values ~= 0;
    
    yInd = y.indices(tmp);
    yVal = y.values(tmp);
    
    if(~isempty(lik.SigmaT))
        yTask = y.tasks(tmp);
    end  
else
    yInd = find(y ~= 0);
    yVal = y(yInd);
end

unobserved = numel(f) - numel(unique(yInd));

if any(abs(yVal) ~=  1)
    error('lik_probit: The class labels have to be {-1,0,1}')
end

if(isempty(lik.SigmaT))
    z = yVal.*f(yInd);
else
    z = yVal.*f(yInd)./lik.SigmaT(yTask,:);  
end

% Avoids one function call and a bit of error checking overhead in
% normcdf
lognormcdfz = log(0.5 * erfc(full(-z ./ sqrt(2))));

if any(z<-10)
    % Asymptotic expansion of norm_cdf
    i = find(z<-10);
    c = 1 - 1./z(i).^2.*(1-3./z(i).^2.*(1-5./z(i).^2.*(1-7./z(i).^2)));
    lognormcdfz(i) = -0.5*log(2*pi)-z(i).^2./2-log(-z(i))+log(c);
end

lognormcdfz = sum(lognormcdfz) + unobserved * log(0.5); % Unobserved values should be undecided, but that does not affect the gradients 

end


function llg = lik_probit_llg(lik, y, f, param, z)
%LIK_PROBIT_LLG  Gradient of the log likelihood
%
%  Description
%    LLG = LIK_PROBIT_LLG(LIK, Y, F, PARAM) takes a likelihood
%    structure LIK, class labels Y, and latent values F. 
%    Returns the gradient of the log likelihood with respect to
%    PARAM. At the moment PARAM can be 'param' or 'latent'.
%    This subfunction is needed when using Laplace approximation 
%    or MCMC for inference with non-Gaussian likelihoods.
%
%   See also
%   LIK_PROBIT_LL, LIK_PROBIT_LLG2, LIK_PROBIT_LLG3, GPLA_E

if(isstruct(y))
    tmp = y.values ~= 0;
    
    yInd = y.indices(tmp);
    yVal = y.values(tmp);
    
    if(~isempty(lik.SigmaT))
        yTask = y.tasks(tmp);
    end
else
    yInd = find(y ~= 0);
    yVal = y(yInd);
end

if any(abs(yVal) ~=  1)
    error('lik_probit: The class labels have to be {-1,0,1}')
end

switch param
    case 'latent'
        if(isempty(lik.SigmaT))
            z = yVal.*f(yInd);
        else
            z = yVal.*f(yInd)./lik.SigmaT(yTask,:);
        end
        
        % ncdf=normcdf(p);
        % Avoids one function call and a bit of error checking overhead in
        % normcdf
        ncdfz = 0.5 * erfc(full(-z ./ sqrt(2)));
        if any(z<-10)
            % Asymptotic expansion of norm_cdf
            i = find(z<-10);
            c = 1 - 1./z(i).^2.*(1-3./z(i).^2.*(1-5./z(i).^2.*(1-7./z(i).^2)));
            ncdfz(i) = -0.5*log(2*pi)-z(i).^2./2-log(-z(i))+log(c);
        end
        
        npdfcdfz = norm_pdf(z)./ncdfz;
        if(isempty(lik.SigmaT))
            llg = sparse(yInd,1,yVal.*npdfcdfz,numel(f),1);
        else
            llg = sparse(yInd,1,yVal.*npdfcdfz./lik.SigmaT(yTask,:),numel(f),1);
        end
end
end


function llg2 = lik_probit_llg2(lik, y, f, param, z)
%LIK_PROBIT_LLG2  Second gradients of the log likelihood
%
%  Description        
%    LLG2 = LIK_PROBIT_LLG2(LIK, Y, F, PARAM) takes a likelihood
%    structure LIK, class labels Y, and latent values F. 
%    Returns the Hessian of the log likelihood with respect to
%    PARAM. At the moment PARAM can be only 'latent'. LLG2 is a
%    vector with diagonal elements of the Hessian matrix (off
%    diagonals are zero). This subfunction is needed when using 
%    Laplace approximation or EP for inference with non-Gaussian 
%    likelihoods.
%
%  See also
%    LIK_PROBIT_LL, LIK_PROBIT_LLG, LIK_PROBIT_LLG3, GPLA_E

  
if(isstruct(y))
    tmp = y.values ~= 0;
    
    yInd = y.indices(tmp);
    yVal = y.values(tmp);
    
    if(~isempty(lik.SigmaT))
        yTask = y.tasks(tmp);
    end
else
    yInd = find(y ~= 0);
    yVal = y(yInd);
end

if any(abs(yVal) ~=  1)
    error('lik_probit: The class labels have to be {-1,0,1}')
end

switch param
    case 'latent'
        if(isempty(lik.SigmaT))
            z = yVal.*f(yInd);
        else
            z = yVal.*f(yInd)./lik.SigmaT(yTask,:);
        end
                
        % ncdf=norm_cdf(z);
        % Avoids one function call and a bit of error checking overhead in
        % normcdf
        ncdfz = 0.5 * erfc(full(-z ./ sqrt(2)));
        if any(z<-10)
            % Asymptotic expansion of norm_cdf
            i = find(z<-10);
            c = 1 - 1./z(i).^2.*(1-3./z(i).^2.*(1-5./z(i).^2.*(1-7./z(i).^2)));
            ncdfz(i) = -0.5*log(2*pi)-z(i).^2./2-log(-z(i))+log(c);
        end
        
        npdfcdfz = norm_pdf(z)./ncdfz;
        if(isempty(lik.SigmaT))            
            llg2 = sparse(yInd,1,-npdfcdfz.^2 - z.*npdfcdfz,numel(f),1);
        else                        
            llg2 = sparse(yInd,1,npdfcdfz.*(-z - npdfcdfz)./lik.SigmaT(yTask,:).^2,numel(f),1);           
        end
end
end

function [ll, llg, llg2] = lik_probit_llext(lik, y, f)
%LIK_PROBIT_LLEXT Get log likelihood and its first two derivatives in one go

if(isstruct(y))
    tmp = y.values ~= 0;
    
    yInd = y.indices(tmp);
    yVal = y.values(tmp);
    
    if(~isempty(lik.SigmaT))
        yTask = y.tasks(tmp);
    end
else
    yInd = find(y ~= 0);
    yVal = y(yInd);
end

if any(abs(yVal) ~=  1)
    error('lik_probit: The class labels have to be {-1,0,1}')
end

unobserved = numel(f) - numel(unique(yInd));

if(isempty(lik.SigmaT))
    z = yVal.*f(yInd);
else
    z = yVal.*f(yInd)./lik.SigmaT(yTask,:);
end

ncdfz = normcdf(z); % 0.5 * erfc(full(-p ./ sqrt(2))); % normcdf(p)
npdfz = normpdf(z);

if any(z<-10)
    % Asymptotic expansion of norm_cdf
    i = find(z<-10);
    c = 1 - 1./z(i).^2.*(1-3./z(i).^2.*(1-5./z(i).^2.*(1-7./z(i).^2)));
    ncdfz(i) = -0.5*log(2*pi)-z(i).^2./2-log(-z(i))+log(c);
end

npdfcdfz = npdfz./ncdfz;

ll = sum(log(ncdfz)) + unobserved * log(0.5);

if(isempty(lik.SigmaT))
    llg = sparse(yInd,1,yVal.*npdfcdfz,numel(f),1);
    llg2 = sparse(yInd,1,-npdfcdfz.^2 - z.*npdfcdfz,numel(f),1);
else   
    llg = sparse(yInd,1,yVal.*npdfcdfz./lik.SigmaT(yTask,:),numel(f),1);
    llg2 = sparse(yInd,1,npdfcdfz.*(-z - npdfcdfz)./lik.SigmaT(yTask,:).^2,numel(f),1);      
end

end

function llg3 = lik_probit_llg3(lik, y, f, param, z)
%LIK_PROBIT_LLG3  Third gradients of the log likelihood
%
%  Description
%    LLG3 = LIK_PROBIT_LLG3(LIK, Y, F, PARAM) takes a likelihood
%    structure LIK, class labels Y, and latent values F and
%    returns the third gradients of the log likelihood with
%    respect to PARAM. At the moment PARAM can be only 'latent'. 
%    LLG3 is a vector with third gradients. This subfunction is 
%    needed when using Laplace approximation for inference with 
%    non-Gaussian likelihoods.
%
%  See also
%    LIK_PROBIT_LL, LIK_PROBIT_LLG, LIK_PROBIT_LLG2, GPLA_E, GPLA_G

if(isstruct(y))
    tmp = y.values ~= 0;
    
    yInd = y.indices(tmp);
    yVal = y.values(tmp);
    
    if(~isempty(lik.SigmaT))
        yTask = y.tasks(tmp);
    end
else
    yInd = find(y ~= 0);
    yVal = y(yInd);
end

if any(abs(yVal) ~=  1)
    error('lik_probit: The class labels have to be {-1,0,1}')
end

switch param
    case 'latent'
        if(isempty(lik.SigmaT))
            z = yVal.*f(yInd);
        else
            z = yVal.*f(yInd)./lik.SigmaT(yTask);
        end
        
        % ncdf=norm_cdf(z);
        % Avoids one function call and a bit of error checking overhead in
        % normcdf
        ncdf = 0.5 * erfc(full(-z ./ sqrt(2)));
        if any(z<-10)
            % Asymptotic expansion of norm_cdf
            i = find(z<-10);
            c = 1 - 1./z(i).^2.*(1-3./z(i).^2.*(1-5./z(i).^2.*(1-7./z(i).^2)));
            ncdf(i) = -0.5*log(2*pi)-z(i).^2./2-log(-z(i))+log(c);
        end
        
        npdfcdfz = norm_pdf(z)./ncdf;
        
        if(isempty(lik.SigmaT))            
            llg3 = sparse(yInd,1,2.*yVal.*npdfcdfz.^3 + 3.*f(yInd).*npdfcdfz.^2 - npdfcdfz.*(yVal-yVal.*f(yInd).^2),numel(f),1);
        else        
            llg3 = sparse(yInd,1,yVal.*((z.^2 - 1).*npdfcdfz + 3.*z.*npdfcdfz.^2 + 2.*npdfcdfz.^3)./lik.SigmaT(yTask,:).^3., numel(f),1);
        end
end
end
