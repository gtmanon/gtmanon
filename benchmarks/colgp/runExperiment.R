args <- commandArgs(trailingOnly = TRUE);

print(getwd())

if(length(args) == 0){
  METHOD <- 'MPLFITC';
  DATAPATH <- '.';
#  DATAPATH <- "~/git/preferences/benchmarks/colgp/data/10032014_155131_959"
  RESULT <- "result.txt";
  SEED <- 1;

  METHOD <- 'MPLFITC_NU';
  DATAPATH <- 'data/05062014_201418_328';
  SEED <- 1;
  RESULT <- 'RES_27042014_112705_138.txt';

} else {
  METHOD <- args[1];  
  DATAPATH  <- args[2];
  SEED <- as.integer(args[3]);  
  RESULT <- args[4];
}

print(sprintf("Running Experiment METHOD = '%s', DATAPATH = '%s', SEED=%d, RESULT = '%s'", METHOD, DATAPATH, SEED, RESULT));

set.seed(SEED);

itemFeatures <- as.matrix(read.table(paste(DATAPATH, "/itemFeatures.txt", sep="")))
nItems <- nrow(itemFeatures)

userFeatures <- as.matrix(read.table(paste(DATAPATH, "/userFeatures.txt", sep="")))

train <- as.matrix(read.table(paste(DATAPATH, "/train.txt", sep = "")))
test <- as.matrix(read.table(paste(DATAPATH, "/test.txt", sep = "")))

userIdsTrain <- train[ , 3 ]
ratingsTrain <- train[ , 4 ]
userIdsTest <- test[ , 3 ]
ratingsTest <- test[ , 4 ]

# We standardize the item features so that they have zero mean and unit standard deviation in the training set
# meanItemFeatures <- apply(itemFeatures[ unique(c(train[ , 1 ], train[ , 2 ])), ], 2, mean)
# sdItemFeatures <- apply(itemFeatures[ unique(c(train[ , 1 ], train[ , 2 ])), ], 2, sd)
# itemFeaturesStandardized <- (itemFeatures - matrix(meanItemFeatures, nrow(itemFeatures), ncol(itemFeatures), byrow = T)) / 
#                              matrix(sdItemFeatures, nrow(itemFeatures), ncol(itemFeatures), byrow = T)

meanItemFeatures <- apply(as.matrix(itemFeatures[ unique(c(train[ , 1 ], train[ , 2 ])), ]), 2, mean)
nonZeros <- which(meanItemFeatures != 0, arr.ind = T)

itemFeaturesStandardized <- as.matrix(itemFeatures)
itemFeaturesStandardized[ , nonZeros]<- scale(itemFeatures[ , nonZeros], center = TRUE, scale = TRUE);

itemFeaturesAtrain <- as.matrix(itemFeaturesStandardized[ train[ , 1 ], ])
itemFeaturesBtrain <-as.matrix(itemFeaturesStandardized[ train[ , 2 ], ])
itemFeaturesAtest <- as.matrix(itemFeaturesStandardized[ test[ , 1 ], ])
itemFeaturesBtest <- as.matrix(itemFeaturesStandardized[ test[ , 2 ], ])

meanUserFeatures <- apply(userFeatures, 2, mean)
sdUserFeatures <- apply(userFeatures, 2, sd)
sdUserFeatures[ sdUserFeatures == 0 ] <- 1
userFeatures <- (userFeatures - matrix(meanUserFeatures, nrow(userFeatures), ncol(userFeatures), byrow = T)) /
  matrix(sdUserFeatures, nrow(userFeatures), ncol(userFeatures), byrow = T)

oldwd <- getwd();

switch(METHOD,
  Birlutiu = {
    
    setwd('pref_learning/code/learning/epMPLbaldBirlutiu/code/')
    source('epGPC.R');
    
    timeTrain <- proc.time()
    gpc <- emBirlutiu(itemFeaturesStandardized, train)
    timeTrain <- proc.time() - timeTrain
    
    timeTest <- proc.time()
    pred <- rep(0, nrow(test))
    for (k in unique(userIdsTest)) {
      index <- which(userIdsTest == k)
      indexRatings <- mapRatingToIndex(test[ index, 1 ], test[ index, 2 ], nrow(itemFeatures))
      pred[ index ] <- sign(predictGPCbirlutiu(gpc[[ k ]], indexRatings)$m)
    }
    timeTest <- proc.time() - timeTest
    
  },
  Bonilla = {
    
    setwd('pref_learning/code/learning/epBonilla/noKernelTuning/code/')
    source('epBonilla.R')
    
    problemInfo <- list(itemFeaturesA = itemFeaturesAtrain, itemFeaturesB = itemFeaturesBtrain,
                        ratings = ratingsTrain, userFeatures = userFeatures[ userIdsTrain, ])
    
    timeTrain <- proc.time()
    ret <- epBonilla(problemInfo)
    timeTrain <- proc.time() - timeTrain
    
    timeTest <- proc.time()
    pred <- sign(predictBonilla(ret, userFeatures[ userIdsTest, ], itemFeaturesAtest, itemFeaturesBtest)$m)
    timeTest <- proc.time() - timeTest
    
  },
  GPC = {
    
    setwd('pref_learning/code/learning/epGPC/code/')
    source('epGPC.R')
    
    itemFeaturesAux <- itemFeaturesStandardized[ unique(c(train[ , 1 ], train[ , 2 ])), ]
    n <- nrow(itemFeaturesAux)
    Q <- matrix(apply(itemFeaturesAux^2, 1, sum), n, n)
    distance <- Q + t(Q) - 2 * itemFeaturesAux %*% t(itemFeaturesAux)
    lengthScale <- sqrt(0.5 * (median(distance[ upper.tri(distance) ])))
    
    timeTrain <- proc.time()        
    gpc <- list()
    for (k in unique(userIdsTrain)) {
      
      index <- which(userIdsTrain == k)
      
      itemsAlocal <- matrix(itemFeaturesAtrain[ index, ], length(index), ncol(itemFeatures))
      itemsBlocal <- matrix(itemFeaturesBtrain[ index, ], length(index), ncol(itemFeatures))
      userIdsLocal <- userIdsTrain[ index ]
      ratingsLocal <- ratingsTrain[ index ]
      
      gpc[[ k ]] <- epGPCinternal(itemsAlocal, itemsBlocal, userIdsLocal, ratingsLocal, lengthScale)      
    }    
    timeTrain <- proc.time() - timeTrain
    
    timeTest <- proc.time()
    pred <- rep(0, nrow(test))
    for (k in unique(userIdsTest)) {
      index <- which(userIdsTest == k)
      pred[ index ] <- sign(predictGPCvectorized(gpc[[ k ]], itemFeaturesAtest[ index, ], itemFeaturesBtest[ index, ])$m)
    }  
    timeTest <- proc.time() - timeTest
    
  },
  MPLFITC = {
    setwd('pref_learning/code/learning/epMPLFITC/code/')
    source("epMPL.R")
    
    itemFeaturesAux <- as.matrix(itemFeaturesStandardized[ unique(c(train[ , 1 ], train[ , 2 ])), ])
    n <- nrow(itemFeaturesAux)
    Q <- matrix(apply(itemFeaturesAux^2, 1, sum), n, n)
    distance <- Q + t(Q) - 2 * itemFeaturesAux %*% t(itemFeaturesAux)
    lengthScaleItems <- sqrt(0.5 * (median(distance[ upper.tri(distance) ])))
    
    userFeaturesAux <- userFeatures
    n <- nrow(userFeaturesAux)
    Q <- matrix(apply(userFeaturesAux^2, 1, sum), n, n)
    distance <- Q + t(Q) - 2 * userFeaturesAux %*% t(userFeaturesAux)
    lengthScaleUsers <- sqrt(0.5 * (median(distance[ upper.tri(distance) ])))
    
    problemInfo <- list(itemFeaturesA = itemFeaturesAtrain, itemFeaturesB = itemFeaturesBtrain,
                        itemIdsA = train[ , 1 ], itemIdsB = train[ , 2 ],
                        userIds = userIdsTrain, ratings = ratingsTrain, d = 20,
                        nTotalItems = nItems, userFeatures = userFeatures, nPseudoInputUsers = 25, nPseudoInputItems = 25)
        
    timeTrain <- proc.time()
    ret <- epMPL(problemInfo, lengthScaleItems, lengthScaleUsers)
    timeTrain <- proc.time() - timeTrain
        
    timeTest <- proc.time()
    pred <- sign(predictMPL(ret, userIdsTest, itemFeaturesAtest, itemFeaturesBtest)$m)
    timeTest <- proc.time() - timeTest
    
  },
  MPLFITC_NU = {
    setwd('pref_learning/code/learning/epMPLFITCnoUserFeatures/code/')
    source("epMPL.R")
    
    itemFeaturesAux <- as.matrix(itemFeaturesStandardized[ unique(c(train[ , 1 ], train[ , 2 ])), ])
    n <- nrow(itemFeaturesAux)
    Q <- matrix(apply(itemFeaturesAux^2, 1, sum), n, n)
    distance <- Q + t(Q) - 2 * itemFeaturesAux %*% t(itemFeaturesAux)
    lengthScaleItems <- sqrt(0.5 * (median(distance[ upper.tri(distance) ])))
    
    userFeaturesAux <- userFeatures
    n <- nrow(userFeaturesAux)
    Q <- matrix(apply(userFeaturesAux^2, 1, sum), n, n)
    distance <- Q + t(Q) - 2 * userFeaturesAux %*% t(userFeaturesAux)
    lengthScaleUsers <- sqrt(0.5 * (median(distance[ upper.tri(distance) ])))
    
    problemInfo <- list(itemFeaturesA = itemFeaturesAtrain, itemFeaturesB = itemFeaturesBtrain,
                        itemIdsA = train[ , 1 ], itemIdsB = train[ , 2 ],
                        userIds = userIdsTrain, ratings = ratingsTrain, d = 20,
                        nTotalItems = nItems, userFeatures = userFeatures, nPseudoInputUsers = 25, nPseudoInputItems = 25)
    
    timeTrain <- proc.time()
    ret <- epMPL(problemInfo, lengthScaleItems, lengthScaleUsers)
    timeTrain <- proc.time() - timeTrain

    timeTest <- proc.time()
    pred <- sign(predictMPL(ret, userIdsTest, itemFeaturesAtest, itemFeaturesBtest)$m)
    timeTest <- proc.time() - timeTest    
  },
  {
    print('Unknown inference method.')
  }
)

error <- mean(pred != ratingsTest)

setwd(oldwd);

result <- c(error, timeTrain[[1]], timeTrain[[2]], timeTest[[1]], timeTest[[2]])
write(result, file=RESULT, append=FALSE, sep=",")

