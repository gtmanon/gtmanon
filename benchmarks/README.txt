This directory should contain the following reference implementations:

(A) Subdirectory "colgp": Houlsby et al., NIPS 2012.
====================================================

(1) cd into the colgp subsirectory and get their code from github using a 
    command like

    git clone https://github.com/neilhoulsby/pref_learning.git

This should give you a directory called 'pref_learning' parallel to the file
runExperiment.R that already resides in that directory.

(2) You may have to re-compile the R extensions in

    pref_learning/code/learning/epMPLFITC/code

    using a sequence of commands like this (assuming R_HOME is the location 
    of your R installation)

        gcc -c -fPIC vbpcad.c -o vbpcad.o; gcc -shared -L$R_HOME/lib -lR -o vbpcad.so vbpcad.o

    or this

        gcc -m32 -c -fPIC vbpcad.c -o vbpcad.o; gcc -m32 -shared -L$R_HOME/lib -lR -o vbpcad.so vbpcad.o

(3) There is an error in their code that needs to be patched.

    The lines starting at 404 in 
    pref_learning/code/learning/{epMPLFITC|epMPLFITCnoUserFeatures}/code/epMPL 
    should read:

	index <- which(itemIdsA > itemIdsB)
	if (length(index) != 0) {
		swap <- itemIdsA[ index ]
		itemIdsA[ index ] <- itemIdsB[ index ]
		itemIdsB[ index ] <- swap

		swap <- itemFeaturesA[ index, ]
		itemFeaturesA[ index, ] <- itemFeaturesB[ index, ]
		itemFeaturesB[ index, ] <- swap

		ratings[ index ] <- -ratings[ index ]
	}

(B) Subdirectory "mixedlogit": Train, 2006
==========================================

(1) Get their code for "Mixed logit estimation by Bayesian methods" from
    http://elsa.berkeley.edu/~train/software.html.

(2) Unzip and copy the contents of their directory train_mxlhb_06 into the
    mixelogit directory, so that the file "mxlhb.m" is directly underneath
    mixedlogit directory.

(C) Subdirectory "mogp": Abbasnejad et al., 2013
================================================
