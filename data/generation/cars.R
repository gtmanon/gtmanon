#
# Routines for preparing the cars datasets.
#
# The data was originally assembled by the authors of the paper:
#
#   Abbasnejad et al., Learning Community-based Preferences via Dirichlet Process Mixtures of Gaussian Processes, IJCAI 2013 
#

library(plyr)

cars_prepare <- function(sourceDirectory, name) {
  
  dataDir <- paste("cars", name, sep="_")
  participantsFilename <- paste(sourceDirectory, dataDir, "users.csv", sep="/")
  instancesFilename <- paste(sourceDirectory, dataDir, "items.csv", sep="/")
  choicesFilename <- paste(sourceDirectory, dataDir, "prefs.csv", sep="/")
 
  userFeatures <- as.matrix(read.table(participantsFilename, sep = ",", header=TRUE))
  # Only users who answered more than 3 control questions correctly
  validUsers<-userFeatures[userFeatures[,6] > 3,1]
  validUsers<-cbind(validUsers,seq(1,length(validUsers)))
  colnames(validUsers) <- c("User.ID","New.ID")
  
  userFeatures<-userFeatures[validUsers[,1],c(2,3,4,5)]
  class(userFeatures)<-"numeric"
  
  itemFeatures <- as.matrix(read.table(instancesFilename, sep = ",", header=TRUE))
  itemFeatures <- itemFeatures[,c(2:5)]
  
  choicesRaw <- as.matrix(read.table(choicesFilename, sep = ",", header=TRUE))
  choicesRaw <- choicesRaw[choicesRaw[,1] %in% validUsers,]  # Only choices from valid users
  choicesRaw <- choicesRaw[choicesRaw[,4] == 0,] # No control items
  choicesRaw<-join(as.data.frame(choicesRaw), as.data.frame(validUsers), type="inner")
  
  choiceData <- matrix(data = 0, nrow=nrow(choicesRaw), ncol=7);
  choiceData[,c(1,6,7)] <- as.matrix(choicesRaw)[,c(5,2,3)];
  choiceData[,2] <- rep(1,nrow(choicesRaw))
 
  # Flip a random subset of choice situations to avoid having all targets == 1
  flip <- which(round(runif(nrow(choicesRaw))) == 1);
  swap <- choiceData[flip, 6];
  choiceData[flip, 6] <- choiceData[flip, 7];
  choiceData[flip, 7] <- swap;
  choiceData[flip,2] <- 2;
  
  storage.mode(choiceData) <- "integer"
  storage.mode(userFeatures) <- "integer"
  list(instances = itemFeatures, individuals = userFeatures, choices = choiceData, name = name)  
}



