classdef SyntheticBinaryChoiceDataset < BinaryChoiceDataset
%SYNTHETICBINARYCHOICEDATASET Creates a synthetic binary choice dataset.
%
%The general idea behind this dataset is to "reverse-engineer" binary 
%choices from noisy, user-specific utility functions that form clusters as 
%follows:
%
%First, draw InstancesPerRelation instances of dimensionality
%InstanceDimensionality. These are the "objects of interest" over which
%preferences are formed.
%
%Next, draw the orientation of NumberOfClusters hyperplanes in 
%(InstanceDimensionality + 1) dimensional space. These are the "general 
%directions" of utility functions for users belonging to that cluster. 
%
%Finally, the utility function (also called "relation") of each individual 
%user is formed by adding two levels of noise to this "general direction":
%(1) The hyperplane for the utility function is randomly tilted relative to 
%    the "general direction" 
%(2) Some random noise is sprinkled onto the relation for the user. 
%
%Considering  the so-constructed hyperplane a utility function in instance 
%space, one can now construct binary choices by simply evaluating the utility 
%function at each instance position and stating that the instance with the 
%higher utility is preferred to the instance with the lower utility.

    properties(GetAccess = 'public')        
        %Number of instances (i.e., "items" or "independent values"). 
        %All relations are evaluated over these same items
        numinstances = 10;
        
        %Dimensionality of items
        instancedim = 1;
        
        %Number of clusters with similar preference relations
        numcluster = 1;
        
        %Number of relations within each cluster
        relpercluster = 1;
        
        %Standard deviation of the relation gradients around the cluster gradient
        sigmarel = 0.0;   
        
        %Stadard deviation of the utility values aroud the relation gradient
        sigmapref = 0.0;
    end

    
    properties(Access = 'private')         
        %numinstanceXinstancedim matrix of actual instance values
        %
        %Note that these will be repeated in the actual dataset for each
        %user
        instances;
        
        %numclusterXinstancedim matrix of cluster directions
        clusterdir;
        
        %(numcluster*relpercluster)Xinstancedim matrix of individual relation directions
        reldir; 
        
        %(numcluster*relpercluster*numinstances)X1 vector of utility values for each
        %user
        utilities;
    end

    methods
        
        function obj = SyntheticBinaryChoiceDataset(varargin)     
            %Construct a new SyntheticBinaryChoiceDataset
            %
            %Arguments:
            %   InstancesPerRelation    - Number of instances, or "objects 
            %                             of interest" over which preferences 
            %                             are formulated (default: 10).
            %   InstanceDimensionality  - Dimensionality of instances (default: 1).
            %   NumberOfClusters        - Number of "general directions" or
            %                             "classes of users" in the
            %                             dataset (default: 1).
            %   RelationsPerCluster     - Number of individual users within
            %                             each cluster (default: 1).
            %   SigmaRelation           - Standard deviation of Gaussian
            %                             noise with which the relations
            %                             are tilted relative to their
            %                             cluster (default: 0.0).
            %   SigmaPreference         - Standard deviation with which the
            %                             utility values for each user are
            %                             distorted relative to the
            %                             idealized value inferred from the
            %                             relation (default: 0.0).
            [obj.numinstances, obj.instancedim, obj.numcluster, ...
             obj.relpercluster, obj.sigmarel, obj.sigmapref  ] = ...
            process_options(varargin, ...
            'InstancesPerRelation', 10, 'InstanceDimensionality', 1, 'NumberOfClusters', 1, ...
            'RelationsPerCluster', 1, 'SigmaRelation', 0.0, 'SigmaPreference', 0.0);
        end
        
        function load(obj)            
            % Determine the instances. We want a total of numinstances
            % instances on the interval [-1.0,1.0].
            obj.instances=(rand(obj.numinstances,obj.instancedim) - 0.5) * 2;
            
            if(obj.instancedim == 1)
                obj.instances = sort(obj.instances);
            end
            
            % Determine the general direction of each cluster. Each
            % cluster is arranged around a central plane in instancedim
            % dimensions, and hence represented by a gradient
            % in each dimension which we draw from a uniform distribution.
            % We draw the gradients in each dimension from the interval
            % [-1.0, 1.0]
            obj.clusterdir=(rand(obj.numcluster,obj.instancedim) - 0.5) * 2;
            
            % Determine, for each individual relation, the direction of
            % that relation. Relation gradients are normally distributed
            % around the cluster directions in each dimension.
            obj.reldir = zeros(obj.numcluster,obj.relpercluster,obj.instancedim);
            for cIdx = 1:obj.numcluster
                obj.reldir(cIdx,:,:) = repmat(obj.clusterdir(cIdx,:),obj.relpercluster,1) + ...
                    randn(obj.relpercluster,obj.instancedim)*obj.sigmarel;
            end
            
            obj.utilities = zeros(obj.numcluster*obj.relpercluster*obj.numinstances, 1);
                        
            for cIdx = 1:obj.numcluster
                blockstart = (cIdx - 1) * (obj.relpercluster*obj.numinstances) + 1;
                blockend = (cIdx * obj.relpercluster * obj.numinstances);
                
                
                % instances is a numinstancesXinstancedim matrix; the
                % second factor is, after projecting on one cluster and 
                % transposing, an instancedimXrelpercluster matrix. The product
                % between the two is a numinstancesXrelpercluster matrix which
                % gives, for each instance in each relation of the current
                % cluster, the utility value. Reshape this to bring it into
                % the linear form of the dataset, where relpercluster*numinstance
                % values are stored linearly for each cluster. 
                obj.utilities(blockstart:blockend,1) = ...
                    reshape(obj.instances*reshape(obj.reldir(cIdx,:,:),obj.relpercluster,obj.instancedim)', ...
                            obj.relpercluster*obj.numinstances,1);
            end    
                        
            if(obj.sigmapref > 0.0)
                % Add noise to the utilities if a positive noise stddev is given
                obj.utilities = obj.utilities + ...
                    randn(obj.numcluster*obj.relpercluster*obj.numinstances,1)*obj.sigmapref;
            end
            
            choices = obj.choicesForRelations();
            
            Nchoices = size(choices,1);
            
            % The choices are always arranges with the preferred
            % alternative first. Let's flip some of them randomly to make
            % things interesting in learning.
            unorderedChoice = randi(2, Nchoices, 1);
            flip = (unorderedChoice == 2);
            
            % Swap choices to be flipped
            choices(flip,4) = choices(flip,2);
            choices(flip,2) = choices(flip,3);
            choices(flip,3) = choices(flip,4);
            choices(:,4) = [];
            
            obj.set(obj.instances, randn(numel(unique(choices(:,1))),2), choices(:,1), choices(:,2:3), unorderedChoice);
        end
                     
        
        function choices = choicesForRelations(obj, relations)
            % Create a derived choice dataset for the given relations
            %
            % The relations parameters can be ommitted to retrieve choices 
            % for all relations            
            %
            % The return object is a Dataset with three columns containing:
            % - the unique identifier of the relation from which the
            %   preference statement is derived
            % - indices into the rows of the PrefLearnSyntheticDataset from
            %   which it is derived. 
            %
            % Each row (i_k, v_k, u_k) stands for: within relation i_k, the 
            % instance in row v_k is preferred to the instance in row u_k
            
            if(nargin < 2 || isempty(relations))
                relations = 1:obj.numberOfRelations();            
            end          
            
            if(max(relations) > obj.numberOfRelations() || min(relations) < 1)
                error('Given relation numbers are out of range.');
            end
                                    
            % Upper bound on the number of preference statements that can
            % come from one relation
            prefs_per_rel = obj.numinstances * (obj.numinstances - 1) / 2;
            choices = zeros(numel(relations) * prefs_per_rel, 3);
            idx = 1;
            
            for rIdx = relations                        
                % Index of the first datapoint for the given relation in the
                % given cluster
                blockstart = (rIdx - 1) * obj.numinstances + 1;
                
                % The y values for the given relation in the given cluster.
                % These correspond to utility values          
                u = obj.utilities(blockstart:(blockstart + obj.numinstances - 1),1);

                for upper = 2:obj.numinstances
                    for lower = 1:upper-1
                        if(u(upper) > u(lower))
                            choices(idx,:) = [rIdx,upper,lower];
                            idx = idx + 1;
                        elseif(u(upper) < u(lower))
                            choices(idx,:) = [rIdx,lower,upper];
                            idx = idx + 1;
                        else
                            % Nothing in case of equality                                
                        end
                    end
                end    
            end
            
            % Delete rows that were not needed due to equality in utility
            % values
            choices(idx:end,:)=[];
        end      
        
        function numrel = numberOfRelations(obj)
            % Retrieve the number of relations in this dataset
            numrel = obj.numcluster * obj.relpercluster;
        end
        
        function clusters = clustersForRelations(obj, relations)
            % Determine the cluster memberships for the given relation
            % indices. Relation indices must be in the range 1:numberOfRelations
            
            if(isempty(relations) || min(relations) < 1 || ...
               max(relations) > obj.numcluster * obj.relpercluster)
                error('Relation indices must be in the range 1:numberOfRelations');
            end
            
            % Relations are arranged per cluster, i.e. the first relpercluster
            % relations belong to cluster 1, the next relpercluster
            % relations belong to cluster 2, and so on.
            clusters = ceil(relations / obj.relpercluster);            
        end
        
        function indices = indicesForRelations(obj, relations)
            % Retrieve the indices into the dataset rows that pertain to
            % the given relation indices.
            if(nargin < 2 || isempty(relations))
                relations = 1:obj.numberOfRelations();            
            end          
            
            if(max(relations) > obj.numberOfRelations() || min(relations) < 1)
                error('Given relation numbers are out of range.');
            end
            
            NR = numel(relations);
            NI = obj.numinstances;
            
            baseindices = (reshape(relations,NR,1) - 1) * NI;            
            indices = reshape(repmat(baseindices, 1, NI) + (ones(NR,1) * (1:NI)), 1, NI * NR);            
        end
        
    end
end
