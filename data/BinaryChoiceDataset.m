classdef BinaryChoiceDataset < Dataset
    %Dataset representing instances and multi.user choice situations between these instances
    %
    
    properties(Access='private')
        
        % Dataset of users
        UD;
        
        % NAx2 matrix of indices into the rows of Dataset.X()
        %
        % Each row refers to two instances that were compared as part of a
        % binary choice. The values of Amat must all be integers in the
        % range 1:Dataset.NX().
        Amat;
        
        % NAx1 matrix of choices
        %
        % Each row contains the choice made when presented with alternatives
        % given by the corresponding row of Amat. The values of Cmat must
        % all be either 1 (first alternative selected) or 2 (second
        % alternative selected).
        Cmat;       
        
        % NAx1 vector of indices into the rows of UD.X()
        %
        % Each row contains the index of the user that made the choice in
        % the corresponding row of Cmat, when presented with the
        % alternatives in the corresponding row of Amat. The values of
        % CUmat must all be integers in the range 1:UD.NX()
        CUmat;
        
    end
    
    methods(Access='public')  
        
        function copy(obj, other)
            
            obj.copy@Dataset(other);
            
            obj.UD = Dataset;
            obj.UD.copy(other.getUsers());
            
            obj.Amat = other.getAlternatives();
            obj.Cmat = other.getChoices();
            obj.CUmat = other.getChoiceUsers();
            
        end
        
        function load(obj, filename, dataset)
            %Load a BinaryChoiceDataset from an ECHP HDF file
            
            load@Dataset(obj, filename, dataset);    
            
            obj.UD = Dataset;
            obj.UD.load(filename, dataset, 'INDIVIDUALS_INSTANCES');
                        
            tmp = double(hdf5read(filename, ['/ECHP_DATASETS/' dataset '/CHOICES'])');              
            if(size(tmp, 2) ~= 7)
                error([ filename '/' dataset ' does not appear to be a binary choice dataset.']);
            end
            
            obj.Amat = tmp(:,6:7);  
            obj.CUmat = tmp(:,1);            
            obj.Cmat = tmp(:,2);            
             
            obj.validate();      
            
        end  
        
        function set(obj, instances, users, choiceUsers, alternatives, choices)
            %Set the values of the BinaryChoiceDataset
            %
            %instances      NXxDX matrix of instance data. These are the
            %               objects that all choices are made between. Each
            %               instance is represented by a row of DX
            %               attributes.
            %
            %users          NUxDU matrix of user data. These are the users
            %               that make all choices. Each user is represented
            %               by a row of DU attributes.
            %
            %choiceUsers    NCx1 matrix of choosing users. Row i says that
            %               user choiceUsers(i) made the respective choice.
            %               That is, each value in choiceUsers is an index
            %               into users and must therefore be in the range
            %               (1:NU).
            %
            %alternatives   NCx2 matrix of alternatives. Each row i
            %               represents the two alternatives that user
            %               choiceUsers(i) was confronted with. Each value
            %               in alternatives is an index into instances and 
            %               must therefore be in the range (1:NU).
            %
            %choices        NCx1 matrix of choices. Each row i indicates
            %               that user choiceUsers(i) chose the first (value
            %               1) or the second (value 2) of the alternatives
            %               in alternatives(i)
            
            obj.setX(instances);                 
            
            obj.UD = Dataset;
            obj.UD.setX(users);
            
            obj.Amat = alternatives;
            obj.CUmat = choiceUsers;
            obj.Cmat = choices;
            
            obj.validate();            
        end        
        
        function setSingleUser(obj, instances, alternatives, choices)            
            %Convenience method performing set for a single user
            %
            %Calls set(...) assuming that all choices have been made by a
            %single user without distinctive user attributes.
           obj.set(instances, 1, ones(size(choices, 1), 1), alternatives, choices)
        end
        
        function [ preferred, dispreferred ] = getChoiceIndices(obj) 
            %Returns two vectors of size NC indicating the (dis-)preferred instance in each choice situation
            %
            %Each value in each of the two vectors is an index into the
            %rows of instances and is therefor in the range 1:NX
            preferred = obj.Amat(sub2ind(size(obj.Amat), int32(1:size(obj.Amat, 1))', int32(obj.Cmat)));
            dispreferred = obj.Amat(sub2ind(size(obj.Amat), int32(1:size(obj.Amat, 1))', int32(3 - obj.Cmat)));
        end  
        
        function UD = getUsers(obj)
            %Returns a Dataset of user instances
            UD = obj.UD;
        end
        
        function nu = NU(obj)
            %Returns the number of user instance, i.e., UD.NX()
            nu = obj.UD.NX();
        end
        
        function choiceUsers = getChoiceUsers(obj)
            %Returns a vector of size NC with indices into users, indicating the choosing user for each choice situation
            choiceUsers = obj.CUmat;
        end
        
        function alternatives = getAlternatives(obj)
            %Returns a vector of size NCx2 with indices into instances, indicating the two alternatives for each choice situation
            alternatives = obj.Amat;
        end
        
        function nc = NC(obj)
            % Returns the number of choice situations in this dataset, i.e., size(getChoices(), 1)            
            nc = size(obj.Cmat, 1);
        end
        
        function choices = getChoices(obj)
            %Returns a vector of size NC vith values in {1,2}, indicating which of the alternatives was chosen in the choice situation.
            choices = obj.Cmat;
        end
         
        function [ BCD, instanceIndices, userIndices ] = selectUsers(obj, userSet)
            %Create a new BinaryChoiceDataset which is consistently reduced to the given users
            %
            %The new dataset BCD has numel(userSet) users where userSet is
            %the given vector of indices in the range 1:NU pointing to
            %users in the full, old dataset.
            %
            %Instances, alternatives, and choiceSituations in the new
            %Dataset are pruned such that only those reference by users in
            %newUsers are retained.
            %
            %instanceIndices contains a mapping between instances in the old 
            %and new dataset. That is instanceIndices(i) refers to the
            %index that was used in the old dataset to refer to the same
            %instance that is now indexed i.
            %
            %userIndices similarly contains a mapping between users in the
            %old and new dataset.
                     
            userChoiceIndices = find(ismember(obj.CUmat, userSet));
                                
            instanceIndices = sort(unique(obj.Amat(userChoiceIndices,:)));            
            userIndices = sort(unique(obj.CUmat(userChoiceIndices)));
            
            mappedChoices = cell2mat(arrayfun(@(u, p1, p2) ...
                [find(userIndices == u), ...
                 find(instanceIndices == p1), ...
                 find(instanceIndices == p2)], ...
                obj.CUmat(userChoiceIndices), ...
                obj.Amat(userChoiceIndices,1), ...
                obj.Amat(userChoiceIndices,2), 'UniformOutput', false));
            
            
            BCD = BinaryChoiceDataset;
            BCD.set(obj.X(instanceIndices), obj.UD.X(userIndices), ...
                mappedChoices(:,1), mappedChoices(:,2:3), obj.Cmat(userChoiceIndices));
            
        end
        
        function [BCD, instanceIndices, userIndices] = reduce(obj, ...
                N, random, samplePerUser)            
            % Consistently reduce the dataset to N choices overall, or per user.
            %
            % This method reduces the dataset to N choices from the set of 
            % all choices. It also reduces the instances and users to only 
            % those referred to by the sampled choices and adjusts the 
            % indexing of the choices in a consistent way.
            %
            % If (the optional) samplePerUser is set to true, the routine 
            % will try to find N choices per user, i.e. it will reduce the 
            % dataset to a maximum of numberOfUsers*N choices. If there are 
            % less than N choices for a user, all choices for that user will 
            % be included in the result and the overall number of choices 
            % on the reduced dataset will be less than numberOfUsers*N.
            %
            % If random is set to true, the routine will select the N 
            % choices at random from all choices (or at random from the 
            % choices of a user, if samplePerUser is set to true).
            %
            % instanceIndices and userIndices contain the mapping from
            % reduced instances/users to the respective indices on the
            % original dataset. That is, if user u states p1 > p2 on the
            % reduced dataset, this corresponds to saying that
            % userIndices(u) stated instanceIndices(p1) >
            % instanceIndices(p2) on the original dataset.
            
            Nchoice = size(obj.Cmat,1);

            if(nargin < 4 || ~samplePerUser)
                % Get N random indices into the choices

                if(N >= size(obj.Cmat,1))
                    cidx = 1:Nchoice;
                else            
                    if(nargin >= 3 && random)
                        indices = randperm(Nchoice);                
                    else
                        indices = 1:Nchoice;
                    end
                    
                    cidx = indices(1:N);   
                end
            else                
                users = unique(obj.CUmat);
                % This is potentially bigger than need be; we'll filter out
                % the superfluous zeros lateron.
                cidx = zeros(1,N*numel(users));
                
                userCount = 1;
                for user = users'                    
                    userChoiceIndices = find(obj.CUmat == user);
                    if(numel(userChoiceIndices) <= N)                            
                        % Use all choices from this user
                        startIndex = (userCount-1)*N + 1;
                        endIndex = (userCount-1)*N + numel(userChoiceIndices);
                        cidx(1,startIndex:endIndex) = userChoiceIndices;
                    else
                        % Find N random indices into the user's choices
                        if(random)
                            indices = randperm(numel(userChoiceIndices));
                        else
                            indices = 1:N;
                        end
                        startIndex = (userCount-1)*N + 1;
                        endIndex = userCount*N;
                        cidx(1,startIndex:endIndex) = ...
                            userChoiceIndices(indices(1:N));
                    end                    
                    userCount = userCount + 1;
                end
                
                cidx = cidx(cidx ~= 0);
            end
            
            
            % The collection of instances that the reduced choices refer to
            instanceIndices = sort(unique(obj.Amat(cidx,:)));            
            userIndices = sort(unique(obj.CUmat(cidx)));
            
            mappedChoices = cell2mat(arrayfun(@(u, p1, p2) ...
                [find(userIndices == u), ...
                 find(instanceIndices == p1), ...
                 find(instanceIndices == p2)], ...
                obj.CUmat(cidx), ...
                obj.Amat(cidx,1), ...
                obj.Amat(cidx,2), 'UniformOutput', false));
                        
            
            
            BCD = BinaryChoiceDataset;
            BCD.set(obj.X(instanceIndices), obj.UD.X(userIndices), ...
                mappedChoices(:,1), mappedChoices(:,2:3), obj.Cmat(cidx));
            
        end
        
        function [trainingIndices, testIndices] = splitChoiceIndices(obj, ...
                trainingFraction, random, perUser)            
            % Compute training and test indices into all choice situations
            %
            % Return trainingIndices and testIndices, the row indices of 
            % trainingFraction*N training and (NC-trainingFraction*N) test 
            % instances, respectively.   
            %
            % If random is set to true, a random split is used instead of just
            % splitting the Dataset after the first N instances.  
            %
            % If N is greater than or equal to NC, the training set will be 
            % the full choices and the test set will be empty.
                        
            if(nargin < 4)
                perUser = false;
            end
            
            trainingIndices = [];
            testIndices = [];
            
            if(perUser)
                for u=1:obj.NU()                    
                    uIdx = find(obj.CUmat == u);
                    N = numel(uIdx);
                    
                    if(isempty(uIdx))
                        warning('No choices for user %d', u);
                    	continue
                    end
                    
                    if(nargin >= 3 && random)
                        indices = uIdx(randperm(numel(uIdx)));                
                    else
                        indices = uIdx;
                    end

                    trainingIndices = [trainingIndices; indices(1:ceil(N*trainingFraction))];
                    testIndices = [testIndices; indices(ceil(N*trainingFraction)+1:end)];                               
                end
            else
                if(nargin >= 3 && random)
                    indices = randperm(obj.NC());                
                else
                    indices = 1:obj.NC();
                end                
                N = numel(indices);

                trainingIndices = indices(1:round(N*trainingFraction));
                testIndices = indices(round(N*trainingFraction)+1:end);
            end
        end
        
        function CDC = toClassificationDataset(obj, relative, mirror)
            %Derive a binary ClassificationDataset from this BinaryChoiceDataset
            %
            %The resulting ClassificationDataset has one instance for each
            %choice situation with the same DX dimensions as the
            %BinaryChoiceDataset. The instance values are X(a1,:)-X(a2,:),
            %that is, the attribute differences between the alternatives
            %presented in each choice situation.
            %
            %The corresponding labels are 1 if the first alternative was
            %preferred, and 2 otherwise.
            %
            %If the optional relative vector is set, it must have DX
            %elements from {0,1,2,3}, which specify the conversion mode per 
            %dimension.
            %
            %0:  Differences, X(a1,d) - X(a2,d)
            %1:  Ratio, (X(a1,d) / X(a2,d)) - 1
            %2:  Log of the ratio, log(X(a1,d) / X(a2,d))
            %3:  Log of the modified ratio, log((X(a1,d) + 1) / (X(a2,d) + 1))
            
            CDC = ClassificationDataset();            
            D = obj.DX();
                        
            if(nargin < 2 || isempty(relative))
                relative = zeros(1, D);
            end
                                
            absIdx = find(relative == 0);
            relIdx = find(relative == 1);
            relLogIdx = find(relative == 2);
            relPlusIdx = find(relative == 3);

            Xnew = zeros(size(obj.X(obj.Amat(:,1))));
            Xnew(:,absIdx) = obj.Xmat(obj.Amat(:,1),absIdx) - obj.Xmat(obj.Amat(:,2),absIdx);
            Xnew(:,relIdx) = (obj.Xmat(obj.Amat(:,1),relIdx) ./ obj.Xmat(obj.Amat(:,2),relIdx)) - 1;
            Xnew(:,relLogIdx) = log(obj.Xmat(obj.Amat(:,1),relLogIdx) ./ obj.Xmat(obj.Amat(:,2),relLogIdx));
            Xnew(:,relPlusIdx) = log((obj.Xmat(obj.Amat(:,1),relPlusIdx) + 1) ./ (obj.Xmat(obj.Amat(:,2),relPlusIdx) + 1));
            
            Ynew = obj.Cmat;
            Cnew = obj.CUmat;
            
            if(nargin > 2)
                if(mirror)                    
                    Xnew_mir = zeros(size(obj.X(obj.Amat(:,1))));
                    Xnew_mir(:,absIdx) = obj.Xmat(obj.Amat(:,2),absIdx) - obj.Xmat(obj.Amat(:,1),absIdx);
                    Xnew_mir(:,relIdx) = (obj.Xmat(obj.Amat(:,2),relIdx) ./ obj.Xmat(obj.Amat(:,1),relIdx)) - 1;
                    Xnew_mir(:,relLogIdx) = log(obj.Xmat(obj.Amat(:,2),relLogIdx) ./ obj.Xmat(obj.Amat(:,1),relLogIdx));
                    Xnew_mir(:,relPlusIdx) = log((obj.Xmat(obj.Amat(:,2),relPlusIdx) + 1) ./ (obj.Xmat(obj.Amat(:,1),relPlusIdx) + 1));
                                        
                    Xnew = [Xnew; Xnew_mir];                    
                    Ynew = [Ynew; Ynew * -1 + 3]; % Reverse ones and twos                      
                    Cnew = [Cnew; Cnew];
                end
            end
            
            CDC.setX(Xnew);            
            CDC.setY(Ynew);  
            CDC.setTasks(Cnew);
        end
        
    end
    
    methods(Access = 'private')
        function validate(obj)
            
            if size(obj.CUmat, 1) ~= size(obj.Cmat,1) || ...
               size(obj.CUmat, 1) ~= size(obj.Amat,1)           
                error('Number of alternative pairs, choices, and choice users must all equal each other');           
            end
            
            if(size(obj.Amat, 2) ~= 2)
                error('Alternatives must be a matrix with two columns.');
            end
            
            if any(obj.CUmat > obj.UD.NX() | obj.CUmat < 1)
                error('BinaryChoiceDataset validation error: User index out of range.');                
            end
            
            if any(obj.Cmat > 2 | obj.Cmat < 1)
                error('BinaryChoiceDataset validation error: Choice index out of range.');                
            end
            
            if any(obj.Amat > obj.NX() | obj.Amat < 1)
                error('BinaryChoiceDataset validation error: Alternative index out of range.');                
            end                            
            
        end
    end
end

