classdef ClassificationDataset < LabeledDataset
    %LabeledDataset where each label is a positive, integer class number   
    
    methods(Access = 'public')

        
    end
    
    methods(Access = 'private')
        
        function validate(obj)            
            validate@LabeledDataset(obj);            
            if(any(~isinteger(obj.Ymat) || obj.Ymat < 1))
                error('All labels must be integers greater or equal 1 in classification datasets.');
            end            
        end
        
    end
    
end

