BCD = BinaryChoiceDataset;
% BCD.load('data/GPPREF.he5', 'gppref'); % Chu and Ghahramani, ICML 2005
% 
% BCD.load('data/CGPPREF.he5', 'election'); % Houlsby et al., NIPS 2012
% BCD.load('data/CGPPREF.he5', 'jura');
% BCD.load('data/CGPPREF.he5', 'movielens');
% BCD.load('data/CGPPREF.he5', 'sushi');
% 
% BCD.load('data/CARS.he5', 'exp1'); % Abbasnejad, IJCAI 2013
% BCD.load('data/CARS.he5', 'exp2');
% 
% BCD.load('data/TARIFFS.he5', 'full2013'); % Peters & Ketter, TR 2013
BCD.load('data/TARIFFS.he5', 'reduced2013');

instances = BCD.X(); % Get the instance attributes
users = BCD.getUsers().X(); % Get the user attributes;
alternatives = BCD.getAlternatives(); % Get the alternatives presented in each choice situation
choices = BCD.getChoices(); % Get the choice made in each situation
choiceUsers = BCD.getChoiceUsers(); % Get the index of the user making the choice

BCD.dropColumns(9:10); % Drop two columns of the instances 

% Consistently reduce the dataset to only the first 10 users. Note, that
% this reduces the choices and might also reduce the instances if there are
% instances that are not referred to by the selected user subset. The
% mapping to the old instance and user indices is returned by the method.
[ BCDred, instanceIndices, userIndices ] = BCD.selectUsers(1:10); 


% Derive a classification dataset where each choice situation is converted
% into an instance with attribute values (x_1 - x_2), where x_i are the
% alternatives presented in that choice situation, and labels {1,2}. 
% In other words, each instance is a collection of attribute differences,
% and the label designates which alternative was ultimately chosen.
CLD = BCDred.toClassificationDataset();

% Discretize the instances of the new dataset. "0" indicates that the
% dimension is kept "as is", other values indicate the desired number of
% bins.
CLD.discretize([2, 2, 0, 1, 2, 2, 4, 4]);

% Normalize each column to zero mean, unit variance. The returned values
% show the relationship to the original data
[means, stddevs] = CLD.normalize();

% Convert to GridDataset
GDS = CLD.toGrid();