classdef GridDataset < Dataset
    
    properties
        
        % Cell array, one cell per dimension, holding a matrix of axis
        % values in that dimension
        Dcell;
        
        % Column vector of grid locations
        Yind;
        
        % Column vector of observation values at the grid locations in
        % Yind
        Yval;
        
        % Column vector of optional task information at the grid
        % locations in Yind
        Tmat;
        
    end
    
    methods        
        function obj = GridDataset()
            obj.Dcell = {};
        end
        
        function createGrid(obj, dimCellArray, yValues, gridIndices)
            %Initialize the grid dataset
            %
            %dimCellArray must be a cell array with one cell per dimension.
            %Each cell must hold a vector of axis values in that dimension.
            %
            %yValues must be a column vector of observation values. It must
            %either be a vector of size prod(D), i.e. one observation for 
            %each location on the grid. Or, if the optional gridIndices is
            %given, it must be a vector of the same length holding
            %observations for the specified gridIndices. Note, that in this
            %latter case it is legal to have multiple observations per grid
            %location.
            obj.Dcell = dimCellArray;
            
            if(nargin > 3)
                obj.setY(yValues, gridIndices);
            else
                obj.setY(yValues);
            end
            
        end
        
        function D = getAxes(obj)
            D = obj.Dcell();            
        end
        
        function setY(obj, yValues, gridIndices)
            %Set the observation values on some or all grid locations
            %
            %yValues must be a column vector of observation values. It must
            %either be a vector of size prod(|D_i|), i.e. one observation for 
            %each location on the grid. Or, if the optional gridIndices is
            %given, it must be a vector of the same length holding
            %observations for the specified gridIndices. Note, that in this
            %latter case it is legal to have multiple observations per grid
            %location.
                        
            % Number of points on the grid
            gridVals = prod(cellfun(@numel, obj.Dcell));  
           
            if(nargin > 2)                
                targetVals = numel(gridIndices);
                indices = gridIndices;
            else
                targetVals = gridVals;    
                indices = (1:gridVals)';
            end
            
            if(size(yValues, 2) ~= 1 || (nargin > 2 && size(gridIndices, 2) ~= 1))
                error('Both, y values and grid indices (if given) must be column vectors.');
            end
                
            if(any(size(yValues) ~= [targetVals, 1]))
                error('Faulty number of values specified (you supplied %d y values).', size(yValues,1));
            end
       
            obj.Yval = yValues;
            obj.Yind = indices;
        end
        
        function setTasks(obj, tValues)
            %Set optional task information on some or all grid locations
            %
            %You must have called createGrid or setY before calling this
            %function, and tValues must specify task information at
            %exactly the same grid locations for which you specified
            %observations
           
            if(isempty(obj.Yind))
                error('You must set the observations before specifying task information.');
            end
           
            if(any(size(tValues) ~= [size(obj.Yind, 1), 1]))
                error('Faulty number of values specified (you supplied %d task values).', size(tValues,1));
            end
       
            obj.Tmat = tValues;            
        end
                        
        function YY = Y(obj, indices) 
            %Return the observation structure for this GridDataset
            %
            %If the optional indices is given, the observations only at the
            %specified grid locations are returned.
            %
            %The return values is a structure consisting of:
            %  - YY.indices: Linear grid locations of all reported observations
            %  - YY.values: Observed values at all locations in YY.indices
            %  - YY.tasks: If task information has been specified
            %  through setTasks(.), also report the task at all 
            %  locations in YY.indices.
            %
            if(nargin == 1)
                YY.values = obj.Yval;
                YY.indices = obj.Yind;
                
                if(~isempty(obj.Tmat))
                    YY.tasks = obj.Tmat;
                end
            else
                selInd = ismember(obj.Yind, indices);
                                
                YY.values = obj.Yval(selInd);
                YY.indices = obj.Yind(selInd);
                                
                if(~isempty(obj.Tmat))
                    YY.tasks = obj.Tmat(selInd);
                end
            end            
        end
                
        function YYbin = YBinary(obj, indices, allowZero)  
            %Return observations, converted to +/-1 labels           
            %
            %The output of this routine is the same as for obj.Y(...),
            %except that all labels are converted to +1 (label +1) or -1
            %(label +2). All observations must have value 1 or 2 or the
            %routine returns an error. 
            %
            %If the optional allowZero is set to true, observations may
            %also have value 0 (unobserved) and will be included in the
            %return value as zeros.
            %
            %see obj.Y(...) for the structure of the output.
            
            if(nargin > 2 && allowZero)
                allowedValues = [0 1 2];
            else
                allowedValues = [1 2];
            end
            
            if any(find(~ismember(obj.Yval, allowedValues)))
                error('All Y values must be 1 or 2 to convert them to binary -1/+1 labels (or 0 if allowZero is true).')
            end
            
            if(nargin < 2 || isempty(indices))
                YYbin = obj.Y();
            else
                YYbin = obj.Y(indices);
            end
                        
            YYbin.values(YYbin.values > 1) = -1;    
        end
        
        function load(~, ~, ~, ~)
            error('load is not implemented for GridDataset.');
        end  
        
        function setX(~, ~)
            error('setX is not implemented for GridDataset. Use createGrid instead.');
        end
                
        function Xmat = X(obj, indices)            
            Xmat = allcomb(obj.Dcell{:});
            
            if(nargin > 1)
                Xmat = obj.Xmat(indices,:);
            end
        end
        
        function n = NX(obj)
            n = prod(cellfun(@numel, obj.Dcell));
        end
                
        function d = DX(obj)
            d = numel(obj.Dcell);
        end                 
             
        function normalize(~)
            error('normalize is not implemented for GridDataset. Pass normalized values to createGrid instead.');
        end
       
        function toGrid(~, ~)
            error('toGrid is not implemented for GridDataset.');
        end
    end
    
    methods(Static)
        
        function D = computeAxes(dataset, indices)
            % Compute the axes of a multidimensional grid that can hold
            % each instance of the Dataset. The result is a cell array of
            % size dataset.DX() where each cell contains the sorted, unique
            % attribute values assumed by instances in dataset.X() in that 
            % dimension. 
            %
            % If the optional indices is given, only those instances will
            % be considered in creating the grid axes.
            
            if(~isa(dataset, 'Dataset'))
                error('Axes can only be computed with a Dataset as input.');
            end
            
            if nargin < 2 || isempty(indices)
                indices = 1:dataset.NX();
            end
            
            D = cell(dataset.DX(),1);
            X = double(dataset.X(indices));
            
            for col = 1:dataset.DX()
                vals = sort(unique(X(:,col)));
                D{col} = vals;
            end            
        end
        
    end    
end

