classdef LabeledDataset < Dataset
    %Dataset with one piece of label data per instance and optional task information
    %
    %LabeledDatasets can be used for either regression or classification
    %tasks.
    %
    
    properties(GetAccess = 'protected', SetAccess = 'private')
        
        % Row vector containing one label per instance
        Ymat;
        
        % Optional column vector containing
        Tmat;
        
    end
    
    methods(Access = 'public')
               
        function copy(obj, other)
            
            obj.copy@Dataset(other);
            
            obj.Ymat = other.Y();
            obj.Tmat = other.getTasks();
            
        end
        
        function setY(obj, targets, tasks)
            %Set the labels corresponding to the instances in X()
            %
            %targets must be a column vector with NX() elements
            %
            %If the optional tasks is given, it must also be a column
            %vector with NX() elements
            obj.Ymat = targets;
            
            if(nargin > 2 && ~isempty(tasks))
                obj.Tmat = tasks;
            end
            
            obj.validate();
        end
        
        function Ymat = Y(obj, indices)
            %Retrieve NX labels (rows) of dimensionality 1 (columns)
            %
            %If the optional indices vector is given, the return value will
            %have numel(indices) rows. The values in indices must be in the
            %range 1:NX
            
            if(nargin == 1)
                Ymat = obj.Ymat;
            else
                Ymat = obj.Ymat(indices,:);
            end
        end
        
                
        function YBinMat = YBinary(obj, indices, allowZero)
            %Retrieve NX binary labels (rows) of dimensionality 1 (columns)
            %
            %YBinary returns an error if there are labels other than 1, 2,
            %and (optionally) 0 (if the optional allowZero is set to true).
            %
            %The result will be a processed version of the result of Y(),
            %where all "2" labels will have been converted to "-1", all "1"
            %labels will remain "1", and (optionally, if allowZero is true)
            %all "0" labels will remain "0"
                     
            if(nargin > 2 && allowZero)
                allowedValues = [0 1 2];
            else
                allowedValues = [1 2];
            end
            
            if any(find(~ismember(obj.Ymat, allowedValues)))
                error('All Y values must be 1 or 2 to convert them to binary -1/+1 labels (or 0 if allowZero is true).')
            end
            
            if(nargin == 1 || isempty(indices))
                YBinMat = obj.Ymat;
            else
                YBinMat = obj.Ymat(indices,:);
            end
            
            YBinMat(YBinMat > 1) = -1;                        
        end
        
        
        function setTasks(obj, tasks)
            %Set the task information corresponding to the instances in X()
            %
            %The task can be thought of as an additional piece of label
            %information per instance that will typically be used to
            %designate the generating process (i.e., different users) that
            %a particular piece of data originated from.
            %
            %tasks must be column vector with NX() elements
            obj.Tmat = tasks;
            
            obj.validate();
        end
        
        function Tmat = getTasks(obj, indices)
            %Retrieve NX pieces of task information (rows) of dimensionality 1 (columns)
            %
            %If the optional indices vector is given, the return value will
            %have numel(indices) rows. The values in indices must be in the
            %range 1:NX
            
            if(nargin == 1)
                Tmat = obj.Tmat;
            else
                Tmat = obj.Tmat(indices,:);
            end
        end
        
        function mediandisp = medianDispersionPerTask(obj)
            % Return a matrix with the median distance from the mean, per dimension and per task
            
            mediandisp = zeros(max(obj.Tmat), obj.DX());
            
            for d = 1:obj.DX()
                mediandisp(:,d) = accumarray(obj.Tmat, abs(obj.Xmat(:,d) - mean(obj.Xmat(:,d), 1)), [], @median);
            end
        end
        
        function mediandisp = medianLabelDispersionPerTask(obj)
            % Return a matrix with the median distance from the mean, per dimension and per task
            
            mediandisp = accumarray(obj.Tmat, (obj.Ymat - mean(obj.Ymat, 1)).^2, [], @median);            
        end
                
        function [ GDStrain, rowGridIndices ] = toGrid(obj, majorityVote, axes, indices)
            %Convert the LabeledDataset into a GridDataset
            %
            %The method creates a new GridDataset where the n-th of the ND 
            %axes consists of all unique values of column n of X().
            %
            %Each row of the LabeledDataset is located on the grid, and the
            %(linear) grid coordinates are returned in rowGridIndices. That
            %is, rowGridIndices is a vector of size NX where element n
            %gives the linear address of instance X(n) on the new grid.
            %
            %If the optional majorityVote parameter is set to true, a
            %majority vote will be held between labels of duplicate X-rows
            %and the most frequently occurring label will be given to that
            %grid point. Otherwise, a label selected at random from the
            %duplicate rows will be used.
            %
            %If the optional axes is given, it must be a cell array of size
            %DX() where each cell contains the axis values for the grid in
            %that dimension. See also GridDatatset.computeAxes()
            %
            %If the optional indices is given, it contains row indices into
            %X() and only those rows will be located on the grid and
            %included in the generated GridDataset.
            
            GDStrain = GridDataset;
            
            if nargin < 4 || isempty(indices)
                indices = 1:obj.NX();
            end
                        
            % These are the minimal grid axes required to cover this
            % dataset
            D = GridDataset.computeAxes(obj, indices);
            
            if nargin > 3 && ~isempty(axes)                
                % If external axes are given make sure they are valid and a
                % superset of the minimal grid axes computed above.
                if(numel(axes) ~= obj.DX())
                    error('The axes parameter must contain one axis per dimension.');
                end

                for col = 1:obj.DX()
                    if ~isempty(setdiff(D{col}, axes{col}))
                        error('The given axes do not cover all instances of this dataset.');
                    end
                end
                
                D = axes;                
            end
                                             
            if(nargin > 1 && majorityVote)
                % rowsIdxIntoX gives the mapping between 'rows' and obj.X().
                % Note: unique changes the order of rows, so we need to be
                % careful with indexing henceforth.
                [ rows, rowsIdxIntoX, XIdxIntoRows ] = unique(obj.X(indices), 'rows');
            
                if(any(size(rows) ~= size(obj.X(indices))))
                    warning('The dataset contains duplicate instances. Only one of each will be kept.');
                end
            else
                rows = obj.X(indices);
                XIdxIntoRows = (1:numel(indices))';
            end

            % Convert rows into a matrix of the same size, where the elements
            % are now indices into the value set D{col} for each column
            indexRows = zeros(size(rows));
            for d = 1:obj.DX()
                [~, indexRows(:,d)] = ismember(rows(:,d), D{d});
            end            
            % indexRows = num2cell(indexRows, 2);
            
            % Compute the contribution of each column to the overall index
            % on the grid, given that the last column changes quickest.           
            indexContributions = flipud(cumprod(flipud([ cellfun(@numel, D); 1])));
            indexContributions = indexContributions(2:end);
            
            % Calculate the position of each row of 'rows' on the grid
            id = ones(1, obj.DX());
            %rowGridIndices = cellfun(@(row) sum((row - id) .* indexContributions') + 1, ...
            %    indexRows);            
            rowGridIndices = sum(bsxfun(@times, bsxfun(@minus, indexRows, id), indexContributions'), 2) + 1;
   
            if(nargin > 1 && majorityVote)
                % Select the most frequently occuring label for each grid
                % location.
                
                % Calculate an array, one element for each occupied grid 
                % point (i.e., same number of elements as cells in 'rows'), 
                % with the most frequently assigned label of that grid
                % point
                GDStrain.createGrid(D, arrayfun(@(row) mode(obj.Ymat(XIdxIntoRows == row)), ...
                                  1:numel(rowGridIndices))', rowGridIndices);    
                                              
            else
                %Otherwise, create a dataset with possibly multiple 
                %observations per location
                GDStrain.createGrid(D, obj.Ymat(indices), rowGridIndices);
            end
            
            if(~isempty(obj.Tmat))
                GDStrain.setTasks(obj.Tmat(indices));
            end
            
            % Map row indices back to the row order of the dataset before
            % the 'unique' operation.
            rowGridIndices = rowGridIndices(XIdxIntoRows);
            
        end       
        
        function [trainingIndices, testIndices] = splitIndices(obj, N, random, perTask)
            % Compute training and test indices into the Dataset
            %
            % Return trainingIndices and testIndices, the row indices of 
            % N training and (NX-N) test instances, respectively.   
            %
            % If random is set to true, a random split is used instead of just
            % splitting the Dataset after the first N instances.  
            %
            % If perTask is set to true, then task information must have be
            % set on the Dataset through setTasks, and the splitting will
            % attempt to have at least one instance per task in the
            % training set
            %
            % If N is greater than or equal to NX, the training set will be 
            % the full Dataset and the test set will be empty.
            
            if(N >= obj.NX())
                trainingIndices = 1:obj.NX();
                testIndices = [];
                return;
            end
            
            if(nargin < 4 || ~perTask)
                if(nargin >= 3 && random)
                    indices = randperm(obj.NX());                
                else
                    indices = 1:obj.NX();
                end

                trainingIndices = indices(1:N);
                testIndices = indices(N+1:end);
            else
                trainingIndices = [];
                testIndices = [];
                
                for t = unique(obj.Tmat)'           
                    tIdx = find(obj.Tmat == t);                   
                    Nt = max(1, ceil(N / obj.NX() * numel(tIdx)));
                                        
                    if(nargin >= 3 && random)
                        indices = tIdx(randperm(numel(tIdx)));
                    else
                        indices = tIdx;
                    end
                    
                    trainingIndices = [ trainingIndices; indices(1:Nt+1) ];
                    testIndices = [ testIndices; indices(Nt:end) ];
                end
            end
        end
        
    end
    
    methods(Access = 'private')
        
        function validate(obj)
            
             if(any(size(obj.Ymat) ~= [obj.NX(), 1]))
                error('Labels must be a column vector with the same number of entries as X.');
             end
             
             if(~isempty(obj.Tmat) && any(size(obj.Tmat) ~= [obj.NX(), 1]))
                error('Tasks must be a column vector with the same number of entries as X.');
             end
            
        end
        
    end
    
end

