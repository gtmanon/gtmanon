classdef Dataset < handle
    %Unlabeled dataset with NX instances of dimensionality DX
    %

    properties(GetAccess = 'protected', SetAccess = 'private')

        % Instance data, NX rows, DX columns
        Xmat;
        
    end
    
    methods(Access='public')  
        
        function load(obj, filename, dataset, table)
            %Load dataset from an ECHP HDF file
            if nargin < 4
                table = 'INSTANCES';
            end
            
            obj.Xmat = double(hdf5read(filename, ['/ECHP_DATASETS/' dataset '/' table])');
        end  
        
        function copy(obj, other)
            obj.Xmat = other.X();
        end
        
        function setX(obj, X)
            %Set the instance data, overwriting all existing instance data
            obj.Xmat = X;
        end
                
        function Xmat = X(obj, indices)
            %Retrieve NX instances (rows) of dimensionality DX (columns)
            %
            %If the optional indices vector is given, the return value will
            %have numel(indices) rows. The values in indices must be in the
            %range 1:NX
            if(nargin == 1)
                Xmat = obj.Xmat;
            else
                Xmat = obj.Xmat(indices,:);
            end
        end
        
        function n = NX(obj)
            %Number of instances in the dataset
            n = size(obj.Xmat, 1);
        end
                
        function d = DX(obj)
            %Dimensionality of each instance in the dataset
            d = size(obj.Xmat, 2);
        end   
        
        function dropColumns(obj, colIndices)
            %Drop the columns specified in the vector colIndices.
            %
            %The new dataset will have dimensionality DX-numel(colIndices).
            
            retain = setdiff(1:obj.DX(), colIndices);
            obj.Xmat = obj.Xmat(:,retain);            
        end
               
        function switchColumns(obj, colIndices)
            %Switch the order of columns in the Dataset
            %
            %colIndices must be a permutaion of the numeber 1...obj.DX
            obj.Xmat = obj.Xmat(:,colIndices);
        end
        
        function uniquecount = uniques(obj)
            % Return a vector with the count of unique values per dimension
            
            uniquecount = arrayfun(@(d) numel(unique(obj.Xmat(:,d))), 1:obj.DX());            
        end
        
        function mediandisp = medianDispersion(obj)
            % Return a vector with the median distance from the mean per dimension
            
            mediandisp = median(abs(obj.Xmat - repmat(mean(obj.Xmat, 1), obj.NX(), 1)), 1);
        end
        
        function maxdisp = maxDispersion(obj)
            % Return a vector with the max distance from the mean per dimension
            
            maxdisp = max(abs(obj.Xmat - repmat(mean(obj.Xmat, 1), obj.NX(), 1)), [], 1);
        end
        
        function [Xtrain, Xtest, trainingIndices, testIndices] = split(obj, N, random)
            % Split the Dataset into training and test instances
            %
            % Return Xtrain, the NxDX matrix of the first N instances, and 
            % Xtest the (NX-N)xDX matrix of the last (NX-N) instances in 
            % the Dataset
            %
            % If random is set to true, a random split is used instead of 
            % splitting the Dataset after the first N instances.
            %
            % trainingIndices and testIndices contain the row indices of the
            % training and test instances, respectively.
            
            [trainingIndices, testIndices] = splitIndices(obj, N, random);           
            Xtrain = obj.X(trainingIndices);
            Xtest = obj.X(testIndices);            
        end
        
        function [trainingIndices, testIndices] = splitIndices(obj, N, random)
            % Compute training and test indices into the Dataset
            %
            % Return trainingIndices and testIndices, the row indices of 
            % N training and (NX-N) test instances, respectively.   
            %
            % If random is set to true, a random split is used instead of just
            % splitting the Dataset after the first N instances.  
            %
            % If N is greater than or equal to NX, the training set will be 
            % the full Dataset and the test set will be empty.
            
            if(N >= obj.NX())
                trainingIndices = 1:obj.NX();
                testIndices = [];
                return;
            end
            
            if(nargin >= 3 && random)
                indices = randperm(obj.NX());                
            else
                indices = 1:obj.NX();
            end
            
            trainingIndices = indices(1:N);
            testIndices = indices(N+1:end);
        end
        
        function [means, stddevs] = normalize(obj)
            %Normalize the dataset such that each column has zero mean and unit variance
            %
            %Returns the previous mans and standard deviations per column
            means = mean(double(obj.Xmat));
            stddevs = std(double(obj.Xmat));
            
            stddevscorr = stddevs;
            stddevscorr(stddevscorr == 0) = 1;
            
            obj.Xmat = (double(obj.Xmat) - repmat(means, size(obj.Xmat, 1), 1)) ./ ...
                                   repmat(stddevscorr,  size(obj.Xmat, 1), 1);
        end
        
        function discretize(obj, numbins, bintype)
            %Discretize the dataset using a given number of bins in each dimension
            %
            %Numbins must be a vector of size DX where each element
            %specifies the number of bins for the corresponding column.
            %Columns for which the numbins entry is zero or negative will
            %not be binned.
            %
            %If the (optional) bintype is given, it must take one of the
            %following values:
            % 'equalspace'    bins are equally spaced along the data's
            %                 range. This is the default.
            % 'equalcount'    bins are created to hold approximately the 
            %                 same number of elements
            % 'kmeans'        bins are cretaed through k-means clustering
            %                 per dimension where each value is replaced by
            %                 the cluster controid.
            %
            %The discretized value for each bin is the average value in it.
            
            if(numel(numbins) ~= obj.DX())
                error('The number of bins must be a vector of size DX().');                
            end
            
            equalSpace = false;
            equalCount = false;
            kmeansBins = false;
            jenksBins = false;
            
            if(nargin < 3)
                equalSpace = true;
            else
                if(strcmp(bintype, 'equalspace'))
                    equalSpace = true;
                elseif(strcmp(bintype, 'equalcount'))
                    equalCount = true;
                elseif(strcmp(bintype, 'kmeans'))
                    kmeansBins = true;
                elseif(strcmp(bintype, 'jenks'))
                    jenksBins = true;
                else
                    error('Unknown bin type %s', bintype);
                end
            end
            
            for d = 1:obj.DX()    
                if(numbins(d) <= 0)
                    % No discretization for this column
                    continue;
                end
                
                uniquevals = unique(obj.Xmat(:,d));
                
                if numbins(d) >= numel(uniquevals)
                        warning('Number of bins specified for column %d (%d) is greater or equal the number of unique values (%d). Keeping current values.', ...
                            d, numbins(d), numel(uniquevals));
                else                                
                    if(numbins(d) > 1)           
                        if(equalSpace)                        
                            %linspace will place the last element on the max(.), 
                            %so create one bin more than specified
                            edges = linspace(min(uniquevals), max(uniquevals), numbins(d) + 1)';  
                            obj.discretizeWithEdges(edges, d);                      
                            
                        elseif(jenksBins)
                            naturalBreaks = jenks(uniquevals, numbins(d));                            
                            % disp(naturalBreaks);                            
                            obj.discretizeWithEdges(naturalBreaks, d);
                            
                        elseif(equalCount)
                            [vals, map] = sort(obj.Xmat(:,d));
                            binsize = floor(numel(vals) / numbins(d));

                            for b = 1:numbins(d)
                                if(b < numbins(d))
                                    indices = map(((b-1)*binsize + 1):b*binsize);
                                else
                                    % Everything else goes into the
                                    % remaining bin
                                    indices = map(((b-1)*binsize + 1):end);
                                end
                                obj.Xmat(indices, d) = mean(obj.Xmat(indices, d));
                            end
                        elseif(kmeansBins)
                            [IDX,C] = kmeans(obj.Xmat(:,d), numbins(d), 'emptyaction', 'singleton');
                            obj.Xmat(:,d) = C(IDX);
                        end
                    else                        
                        obj.Xmat(:,d) = mean(obj.Xmat(:,d));
                    end
                end
            end            
        end   
        
        function discretizeWithEdges(obj, edges, d)            
            if(nargin < 3 && (~iscell(edges) || numel(edges) ~= obj.DX()))
                error('If d is not given, edges must be a cell array of size DX where DX is the dimensionality of the instances.');
            end
            
            if(nargin == 3 && size(edges,2) ~= 1)
                error('If d is given, edges must be a column vector.');
            end
            
            if(nargin == 2)
                for d=1:obj.DX()
                    binEdges = edges{d};
                    
                    if(~isempty(binEdges))
                        binEdges(1) = -Inf;
                        binEdges(end) = Inf;

                        [ ~, bins ] = histc(obj.Xmat(:,d), binEdges);
                        obj.Xmat(:,d) = ((edges{d}(bins + 1) - edges{d}(bins)) ./ 2) + edges{d}(bins);
                    else
                        % Keep values as is
                    end
                end
            else
                binEdges = edges;
                binEdges(1) = -Inf;
                binEdges(end) = Inf;
                
                [ ~, bins ] = histc(obj.Xmat(:,d), binEdges);
                obj.Xmat(:,d) = ((edges(bins + 1) - edges(bins)) ./ 2) + edges(bins);
            end
        end
    end
end

